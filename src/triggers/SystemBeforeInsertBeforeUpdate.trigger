/*This trigger is used for all Before events on System. 
      Last Updated: Brittany Tankhim - 10/24/13 */

trigger SystemBeforeInsertBeforeUpdate on System__c (before insert, before update) {
   if (trigger.isUpdate) {
        SystemBlobUtil.setLicenseFeaturesSettingsBlob(trigger.new, trigger.oldMap); 
        SystemBlobUtil.setLicenseCountsBlob(trigger.new, trigger.oldMap);
    }
}
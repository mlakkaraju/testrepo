trigger GenerateLicenseFeaturesSettingsBlob on License_Key__c (before insert, before update) {

for (Integer i=0; i<Trigger.new.size();i++)
{
    String VOD_VIDEOMARK = Trigger.new[i].VOD_Videomark__c;
    String BROADCAST_VIDEOMARK = Trigger.new[i].Broadcast_Videomark__c;
    String REMOTE_ENTITLEMENT_MANAGER = Trigger.new[i].Remote_Entitlement_Manager__c;
    Double ACTIVE_CERTIFICATES_COUNT = Trigger.new[i].License_Count_Legacy__c;
    Double MAX_CHANNEL_COUNT = Trigger.new[i].Max_Broadcast_Channels__c;
    Double INTERNET_STREAM_COUNT = Trigger.new[i].Max_Internet_Streams__c;
    
    Date LICENSE_END_DATE = Trigger.new[i].License_End_Date__c;
    String LICENSE_END_YEAR = string.valueof(LICENSE_END_DATE.year());
    String LICENSE_END_MONTH = string.valueof(LICENSE_END_DATE.month());
    String LICENSE_END_DAY = string.valueof(LICENSE_END_DATE.day());
    
    if ( LICENSE_END_MONTH.length()==1)
    {
        LICENSE_END_MONTH = '0'+LICENSE_END_MONTH;
    }
        if ( LICENSE_END_DAY.length()==1)
    {
        LICENSE_END_DAY = '0'+LICENSE_END_DAY;
    }
    String endDate = LICENSE_END_YEAR + LICENSE_END_MONTH + LICENSE_END_DAY;

    Date LICENSE_START_DATE = Trigger.new[i].License_Start_Date__c;
    String LICENSE_START_YEAR = string.valueof(LICENSE_START_DATE.year());
    String LICENSE_START_MONTH = string.valueof(LICENSE_START_DATE.month());
    String LICENSE_START_DAY = string.valueof(LICENSE_START_DATE.day());
    
    if ( LICENSE_START_MONTH.length()==1)
    {
        LICENSE_START_MONTH = '0'+LICENSE_START_MONTH;
    }
        if ( LICENSE_START_DAY.length()==1)
    {
        LICENSE_START_DAY = '0'+LICENSE_START_DAY;
    }
    String startDate = LICENSE_START_YEAR + LICENSE_START_MONTH + LICENSE_START_DAY;

    Double LICENSE_RECYCLE_PERIOD = Trigger.new[i].License_Recycle_Period__c;
    String CLONE_DETECTION = Trigger.new[i].Clone_Detection__c;
    String ON_SCREEN_DISPLAY = Trigger.new[i].On_Screen_Display__c;
    String COPY_CONTROL = Trigger.new[i].Copy_Control__c;
    String VMPLCONF_BASE1 = Trigger.new[i].VMPLCONF_BASE1__c;
    String VMPLCONF_RANGE1 = Trigger.new[i].VMPLCONF_RANGE1__c;
    String VMPL_TransactionID = Trigger.new[i].VMPL_TransactionID__c;
    String VMPLCONF_HASH = Trigger.new[i].VMPLCONF_HASH__c;
    String VOD_ENCRYPTION = Trigger.new[i].VOD_Encryption__c;
    String VCP_ENC_KEY = Trigger.new[i].VCP_ENC_Key__c;
    String SITE_ID = Trigger.new[i].Site_ID__c;
    String OTT_ENABLED = Trigger.new[i].OTT_Enabled__c;
    String MRPR_ENABLED = Trigger.new[i].MRPR_Enabled__c;
    String SOCKEM_ENABLED = Trigger.new[i].SOCKEM_Enabled__c;
        
    if (LICENSE_RECYCLE_PERIOD == null) LICENSE_RECYCLE_PERIOD =0;
    
    Trigger.new[i].License_Features_Settings_Blob__c =

            'VOD_VIDEOMARK = ' + VOD_VIDEOMARK + '%' +
            'BROADCAST_VIDEOMARK = ' + BROADCAST_VIDEOMARK + '%' +
            'REMOTE_ENTITLEMENT_MANAGER = ' + REMOTE_ENTITLEMENT_MANAGER + '%' +
            'ACTIVE_CERTIFICATES_COUNT = ' + (Integer)ACTIVE_CERTIFICATES_COUNT + '%' +
            'MAX_CHANNEL_COUNT = ' + (Integer)MAX_CHANNEL_COUNT + '%' +
            'INTERNET_STREAM_COUNT = ' + (Integer)INTERNET_STREAM_COUNT + '%' +
            'LICENSE_START_DATE = ' + startDate + '%' +
            'LICENSE_END_DATE = ' + endDate + '%' +
            'LICENSE_RECYCLE_PERIOD =' +(Integer)LICENSE_RECYCLE_PERIOD * 24 + '%' +
            'CLONE_DETECTION = ' + CLONE_DETECTION + '%' +
            'ON_SCREEN_DISPLAY = ' + ON_SCREEN_DISPLAY + '%' +
            'COPY_CONTROL = ' + COPY_CONTROL + '%' +
            'VMPLCONF_BASE1 = ' + VMPLCONF_BASE1 + '%' +
            'VMPLCONF_RANGE1 = ' + VMPLCONF_RANGE1 + '%' +
            'VMPL_TransactionID = ' + VMPL_TransactionID + '%' +
            'VMPLCONF_HASH = ' + VMPLCONF_HASH + '%' +
            'VOD_ENCRYPTION = ' + VOD_ENCRYPTION + '%' +
            'VCP_ENC_KEY = ' + VCP_ENC_KEY + '%' +
            'SITE_ID = ' + SITE_ID + '%' +
            'OTT_ENABLED = ' + OTT_ENABLED + '%' +
            'MRPR_ENABLED = ' + MRPR_ENABLED + '%' +
            'SOCKEM_ENABLED = ' + SOCKEM_ENABLED + '%';

}
}
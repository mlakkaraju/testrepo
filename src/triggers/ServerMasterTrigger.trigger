//See comments in TriggerHandler for more details on the pattern
trigger ServerMasterTrigger on Server__c (
	before insert 
	//before update, 
	//before delete, 
	//after insert, 
	//after update, 
	//after delete, 
	//after undelete
	) {

	//add action for project license date trigger handler
	TriggerHandler th = new TriggerHandler();
	th.bind(TriggerHandler.Evt.beforeinsert, new SLA_ServerAddedTriggerHandler());

	//execute bindings
	th.manage();
}
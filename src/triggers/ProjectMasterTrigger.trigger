//Master trigger for all events on project.
// Each individual action should implement TriggerHandler.HandlerInterface
// If binding a new event, make sure that it is uncommented from the trigger def

//See comments in TriggerHandler for more details on the pattern

trigger ProjectMasterTrigger on Tenrox__c (
	before update,
	after insert,
	after update,  
	before insert
	//before delete, 
	//after delete, 
	//after undelete
	) {
	
	//add action for project license date trigger handler
	TriggerHandler th = new TriggerHandler();
	th.bind(TriggerHandler.Evt.beforeupdate, new SLA_ProjectLicenseDateTriggerHandler());
	th.bind(TriggerHandler.Evt.afterinsert, new ProjectCreateDeliverablesTriggerHandler());
	th.bind(TriggerHandler.Evt.afterupdate, new ProjectUpdateReconLicenseCount_TH());
	th.bind(TriggerHandler.Evt.afterupdate, new ProjectHasOpenTimePhased_TH());
	th.bind(TriggerHandler.Evt.beforeinsert, new SLA_ProjectPermanentLicenseTriggerHelper());
	//execute bindings
	th.manage();

}
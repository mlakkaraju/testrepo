trigger RHX_Client_Device_Test on Client_Device_Test__c (after insert, after update, after delete, after undelete) {   
    if (trigger.isAfter) {            
        Type rollClass = System.Type.forName('rh2', 'ParentUtil');        
       
        if(rollClass != null) {            
            RH2.PS_Rollup_Engine pu = (RH2.PS_Rollup_Engine) rollClass.newInstance();           
            Database.update(pu.performTriggerRollups(trigger.oldMap, trigger.newMap, null), false);         
            }    
       }
}
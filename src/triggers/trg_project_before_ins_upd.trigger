/*
Trigger is updating fields we no longer use. Also causing issues with VCAS Version object dataload (Case:  00030591)
Marked inactive on 10/30/13 for Salesforce v2.0 release. Should delete with larger Project object clean-up initiative (Case 00033729).
-Brittany Tankhim (10/30/13)
*/
trigger trg_project_before_ins_upd on tenrox__c (before insert,before update) 
{
/*
    for (tenrox__c rec : System.Trigger.new) 
    {
    
        if (System.Trigger.isUpdate)
        {
            if (!rec.Do_Not_Trigger__c)
            {
                rec.Do_Not_Trigger__c = true;
            }
            else
            {
                rec.Do_Not_Trigger__c = false;
            }
        }
        
        String totalComment = '';
        String crlf = '\n';
        if (rec.Status_Comments__c != null)
            totalComment += 'Project Status Comments : ' + rec.Status_Comments__c + crlf;
        String oppId = rec.opportunity__c;
        if (oppId != null)
        {            
            Opportunity oppRec = [select description,Project_Manager_Comments__c,Legal_Comments__c from Opportunity where Id = :oppId];
            if (oppRec.Project_Manager_Comments__c != null ) 
                totalComment += 'Project Manager Comments : ' + oppRec.Project_Manager_Comments__c  + crlf  ;
            if (oppRec.Legal_Comments__c != null )
                totalComment += 'Legal Comments : ' + oppRec.Legal_Comments__c;
        }
        rec.Total_Comments__c = totalComment;  
*/

        if (System.Trigger.IsInsert)
        {
            for (tenrox__c rec : Trigger.new) 
            {
                String acctId = rec.Account__c;
                if (acctId != null)
                {          
                    Account acctRec = [select Estimated_Contract_Complete_Date__c from Account where Id = :acctId limit 1];
                    if (acctRec.Estimated_Contract_Complete_Date__c != null ) 
                        rec.Estimated_Contract_Complete_Date__c = acctRec.Estimated_Contract_Complete_Date__c;
                }
            }
        }        
    
}
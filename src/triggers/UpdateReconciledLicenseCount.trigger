trigger UpdateReconciledLicenseCount on tenrox__c (after update) {

Map<Id, String> accountMap = new Map<Id, String>();

for (Integer i=0; i<Trigger.new.size();i++)
{
    String project_substatus_new = Trigger.new[i].Project_Substatus__c;
    String project_substatus_old = Trigger.old[i].Project_Substatus__c;
    
    if (project_substatus_old == 'License Count Discrepancy' && project_substatus_new != project_substatus_old && Trigger.new[i].Account__c != null)
    {
        accountMap.put(Trigger.new[i].Account__c, '' );
    }  
}
    Boolean empty = accountMap.isEmpty();
    
    if (!empty)
    {
        Set <Id> accountIdSet = new Set<Id>(); 
        accountIdSet = accountMap.keySet();

        Account[] accounts = [Select Id, Reconciled_License_Count__c from Account where Id in: accountIdSet];
        
        for (Account acc : accounts)
        {
            acc.Reconciled_License_Count__c = true;
        }
        update accounts;
    }
}
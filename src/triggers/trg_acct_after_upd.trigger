trigger trg_acct_after_upd on Account (after Update) 
{
    for (Integer i = 0; i < Trigger.new.size(); i++)  
    { 
        
        if (Trigger.new[i].Estimated_Contract_Complete_Date__c != Trigger.old[i].Estimated_Contract_Complete_Date__c)
        {
            projectUpdate.updateEstContractComplete(Trigger.new[i].Id, Trigger.new[i].Estimated_Contract_Complete_Date__c);
        }        
    }
}
trigger viid_trg on Account (before update) {

for( Account a : Trigger.new)
{

Blob targetBlob = Blob.valueOf(a.Id+'0');

Blob hashMD5 = Crypto.generateDigest('MD5', targetBlob);

String hashBase64MD5 = EncodingUtil.convertToHex(hashMD5);

hashBase64MD5 = hashBase64MD5.subString(0,32);

if(a.VIID__c == NULL)
a.VIID__c = hashBase64MD5;

}

}
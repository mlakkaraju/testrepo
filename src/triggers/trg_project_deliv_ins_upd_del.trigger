trigger trg_project_deliv_ins_upd_del on Project_Deliverable__c (before insert, before update, after insert,after update,after delete) 
{

    if(trigger.isBefore) {
        List<Project_Deliverable__c> updateFieldList = new List<Project_Deliverable__c>();   
        
        for(Project_Deliverable__c loopPD: trigger.new) {
            if(loopPD.Product__c != null) {
                updateFieldList.add(loopPD);
            }
        }
        
        if(!updateFieldList.isEmpty()) {
            ProjectDeliveryHandler.setFieldValues(updateFieldList);
        }
    }
    if(trigger.isAfter) {
      Boolean runUpdateRev = false;
      String projId = '';
        if (System.Trigger.IsDelete)
        {
            for (Integer i = 0; i < Trigger.old.size(); i++)  
            {
              runUpdateRev = true;
              projId = Trigger.old[i].project__c;
            }
        if (runUpdateRev)
          projectUpdate.createOrUpdateRevenue('DELIVERABLE', projId);
        }
        else
        {
            for (Integer i = 0; i < Trigger.new.size(); i++)  
            { 
                if ((System.Trigger.IsInsert) || (System.Trigger.IsUpdate && ((Trigger.new[i].Amount__c != null && (Trigger.old[i].Amount__c == null)) || (Trigger.new[i].Amount__c != Trigger.old[i].Amount__c))))
                {
                runUpdateRev = true;
                projId = Trigger.new[i].project__c;
              }
            }
        if (runUpdateRev)
          projectUpdate.createOrUpdateRevenue('DELIVERABLE', projId);
        }
    }
}
/*
Trigger is updating fields we no longer use. Also causing issues with VCAS Version object dataload (Case:  00030591)
Marked inactive on 10/30/13 for Salesforce v2.0 release. Should delete with larger Project object clean-up initiative (Case 00033729).
-Brittany Tankhim (10/30/13)
*/
trigger trg_project_ins_upd_del on tenrox__c (after insert,after delete) 
{

    String[] accountIds;
    String[] oppIds;
    String[] projIds;
    String[] commUpdIds;
    Date[] fufillDates;
    Date[] fufillDatesNew;
    if (System.Trigger.IsDelete)
    {
        accountIds = new String[Trigger.old.size() ];
        for (Integer i = 0; i < Trigger.old.size(); i++)  
        {
            accountIds[i] = Trigger.old[i].Account__c;         
        }
    }
    else
    {
        accountIds = new String[Trigger.new.size()];
        oppIds = new String[Trigger.new.size()];
        projIds = new String[Trigger.new.size()];
        commUpdIds = new String[Trigger.new.size()];
        fufillDates = new Date[Trigger.new.size()];
        fufillDatesNew = new Date[Trigger.new.size()];
        for (Integer i = 0; i < Trigger.new.size(); i++)  
        { 
            //if (System.Trigger.IsInsert || Trigger.new[i].Account__c != Trigger.old[i].Account__c )
            if (System.Trigger.IsInsert || Trigger.new[i].Do_Not_Trigger__c)
                accountIds[i] = Trigger.new[i].Account__c;
            
            if (System.Trigger.IsInsert || (Trigger.new[i].Opportunity__c != null && (Trigger.old[i].Opportunity__c == null) ))
            {
                oppIds[i] = Trigger.new[i].Opportunity__c;
                projIds[i] = Trigger.new[i].Id;
            }
            /* Commented out - no longer in use - BT 11/6/13
			else if ( (Trigger.old[i].Forecast_Project_Close_Acceptance_Date__c == null && Trigger.new[i].Forecast_Project_Close_Acceptance_Date__c != null ) || (Trigger.old[i].Forecast_Project_Close_Acceptance_Date__c != Trigger.new[i].Forecast_Project_Close_Acceptance_Date__c) )
            {
                projIds[i] = Trigger.new[i].Id;
                fufillDates[i] = Trigger.old[i].Forecast_Project_Close_Acceptance_Date__c;
                fufillDatesNew[i] = Trigger.new[i].Forecast_Project_Close_Acceptance_Date__c;
                String prId = Trigger.new[i].Id;
                projectUpdate.createOrUpdateRevenue('PROJECT', prId); 
            }*/
            
            //if (System.Trigger.IsInsert || (Trigger.new[i].Status_Comments__c != Trigger.old[i].Status_Comments__c))
            //{
           //     commUpdIds[i] = Trigger.new[i].Id;
            //}
        }
    }
    
    for (Integer intPos = 0; intPos < accountIds.size(); intPos++)
    {
        if (accountIds[intPos] != null)
            projectUpdate.updateAccount(accountIds[intPos]);
    } 
    if (System.Trigger.IsInsert)
    {
        for (Integer intPos = 0; intPos < projIds.size(); intPos++)
        {
            if (System.Trigger.IsInsert && oppIds[intPos] != null)
            {
                projectUpdate.updateOpportunity(oppIds[intPos], projIds[intPos]);
            }   
           //if (commUpdIds[intPos] != null)
           //      projectUpdate.updateMetaComment('',commUpdIds[intPos]);
                
            if (fufillDates[intPos] != null)
            {
                String strProjId = projIds[intPos];
                Date oldDt = fufillDates[intPos];
                for (Project_Deliverable__c lineRec : [select Id, Fulfilment_Forecast_Date__c from Project_Deliverable__c where Project__c = :strProjId and (Fulfilment_Forecast_Date__c = :oldDt or Fulfilment_Forecast_Date__c = null) ] )
                {
                    lineRec.Fulfilment_Forecast_Date__c = fufillDatesNew[intPos];                
                    update(lineRec);
                }
            }                
        }         
    }
 
}
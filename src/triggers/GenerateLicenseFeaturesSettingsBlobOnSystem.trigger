trigger GenerateLicenseFeaturesSettingsBlobOnSystem on System__c (before insert, before update) {

for (Integer i=0; i<Trigger.new.size();i++)
{
    String VOD_VIDEOMARK = Trigger.new[i].VODVideomark__c;
    String BROADCAST_VIDEOMARK = Trigger.new[i].BroadcastVideomark__c;
    String REMOTE_ENTITLEMENT_MANAGER = Trigger.new[i].RemoteEntitlementManager__c;
    Double ACTIVE_CERTIFICATES_COUNT = Trigger.new[i].LicenseCountLegacy__c;
    Double MAX_CHANNEL_COUNT = Trigger.new[i].MaxBroadcastChannels__c;
    Double INTERNET_STREAM_COUNT = Trigger.new[i].MaxInternetStreams__c;
    
    Date LICENSE_END_DATE = Trigger.new[i].LicenseEndDate__c;
    String LICENSE_END_YEAR = string.valueof(LICENSE_END_DATE.year());
    String LICENSE_END_MONTH = string.valueof(LICENSE_END_DATE.month());
    String LICENSE_END_DAY = string.valueof(LICENSE_END_DATE.day());
    
    if ( LICENSE_END_MONTH.length()==1)
    {
        LICENSE_END_MONTH = '0'+LICENSE_END_MONTH;
    }
        if ( LICENSE_END_DAY.length()==1)
    {
        LICENSE_END_DAY = '0'+LICENSE_END_DAY;
    }
    String endDate = LICENSE_END_YEAR + LICENSE_END_MONTH + LICENSE_END_DAY;

    Date LICENSE_START_DATE = Trigger.new[i].LicenseStartDate__c;
    String LICENSE_START_YEAR = string.valueof(LICENSE_START_DATE.year());
    String LICENSE_START_MONTH = string.valueof(LICENSE_START_DATE.month());
    String LICENSE_START_DAY = string.valueof(LICENSE_START_DATE.day());
    
    if ( LICENSE_START_MONTH.length()==1)
    {
        LICENSE_START_MONTH = '0'+LICENSE_START_MONTH;
    }
        if ( LICENSE_START_DAY.length()==1)
    {
        LICENSE_START_DAY = '0'+LICENSE_START_DAY;
    }
    String startDate = LICENSE_START_YEAR + LICENSE_START_MONTH + LICENSE_START_DAY;

    Double LICENSE_RECYCLE_PERIOD = Trigger.new[i].LicenseRecyclePeriod__c;
    String CLONE_DETECTION = Trigger.new[i].CloneDetection__c;
    String ON_SCREEN_DISPLAY = Trigger.new[i].OnScreenDisplay__c;
    String COPY_CONTROL = Trigger.new[i].CopyControl__c;
    String VMPLCONF_BASE1 = Trigger.new[i].VMPLCONFBASE1__c;
    String VMPLCONF_RANGE1 = Trigger.new[i].VMPLCONFRANGE1__c;
    String VMPL_TransactionID = Trigger.new[i].VMPLTransactionID__c;
    String VMPLCONF_HASH = Trigger.new[i].VMPLCONFHASH__c;
    String VOD_ENCRYPTION = Trigger.new[i].IPTVVODEncryption__c;
    String VCP_ENC_KEY = Trigger.new[i].VCPENCKey__c;
    String SITE_ID = Trigger.new[i].Site_ID__c;
    String OTT_ENABLED = Trigger.new[i].OTTEnabled__c;
    String MRPR_ENABLED = Trigger.new[i].MRPREnabled__c;
    String SOCKEM_ENABLED = Trigger.new[i].SOCKEM_Enabled__c;
   /* String WHOLESALER = Trigger.new[i].Wholesaler__c;
    Double MAX_WHOLESALER_COUNT = Trigger.new[i].Max_Wholesalers__c;
    String RETAILER = Trigger.new[i].Retailer__c; 
    Double MAX_RETAILER_COUNT= Trigger.new[i].Max_Retailers__c;
 */
    Trigger.new[i].License_Features_Settings_Blob__c =

            'VOD_VIDEOMARK = ' + VOD_VIDEOMARK + '%' +
            'BROADCAST_VIDEOMARK = ' + BROADCAST_VIDEOMARK + '%' +
            'REMOTE_ENTITLEMENT_MANAGER = ' + REMOTE_ENTITLEMENT_MANAGER + '%' +
            'ACTIVE_CERTIFICATES_COUNT = ' + (Integer)ACTIVE_CERTIFICATES_COUNT + '%' +
            'MAX_CHANNEL_COUNT = ' + (Integer)MAX_CHANNEL_COUNT + '%' +
            'INTERNET_STREAM_COUNT = ' + (Integer)INTERNET_STREAM_COUNT + '%' +
            'LICENSE_START_DATE = ' + startDate + '%' +
            'LICENSE_END_DATE = ' + endDate + '%' +
            'LICENSE_RECYCLE_PERIOD =' +(Integer)LICENSE_RECYCLE_PERIOD*24 + '%' +
            'CLONE_DETECTION = ' + CLONE_DETECTION + '%' +
            'ON_SCREEN_DISPLAY = ' + ON_SCREEN_DISPLAY + '%' +
            'COPY_CONTROL = ' + COPY_CONTROL + '%' +
            'VMPLCONF_BASE1 = ' + VMPLCONF_BASE1 + '%' +
            'VMPLCONF_RANGE1 = ' + VMPLCONF_RANGE1 + '%' +
            'VMPL_TransactionID = ' + VMPL_TransactionID + '%' +
            'VMPLCONF_HASH = ' + VMPLCONF_HASH + '%' +
            'VOD_ENCRYPTION = ' + VOD_ENCRYPTION + '%' +
            'VCP_ENC_KEY = ' + VCP_ENC_KEY + '%' +
            'SITE_ID = ' + SITE_ID + '%' +
            'OTT_ENABLED = ' + OTT_ENABLED + '%' +
            'MRPR_ENABLED = ' + MRPR_ENABLED + '%' +
            'SOCKEM_ENABLED = ' + SOCKEM_ENABLED + '%' ;
           /* 'WHOLESALER = ' + WHOLESALER + '%' +
            'MAX_WHOLESALER_COUNT = ' + (Integer)MAX_WHOLESALER_COUNT + '%' +
            'RETAILER = ' + RETAILER + '%' +
            'MAX_RETAILER_COUNT = ' + (Integer)MAX_RETAILER_COUNT + '%' ;
 */

}
}
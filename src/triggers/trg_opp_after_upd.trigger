trigger trg_opp_after_upd on Opportunity (after Update) 
{
    for (Integer i = 0; i < Trigger.new.size(); i++)  
    { 
        if (!Trigger.new[i].Exclude_From_Trigger__c)
        {    
            if (Trigger.new[i].Project_Manager_Comments__c != Trigger.old[i].Project_Manager_Comments__c || Trigger.new[i].Legal_Comments__c != Trigger.old[i].Legal_Comments__c)
            {
                projectUpdate.updateMetaComment(Trigger.new[i].Id, '');
            }
            
            if ((Trigger.new[i].StageName != Trigger.old[i].StageName) && Trigger.new[i].StageName == 'Closed Won')
            {
                projectUpdate.createCommissionRecords(Trigger.new[i].Id);
                Opportunity oppRec = [select Id, Exclude_From_Trigger__c from Opportunity where Id = :Trigger.new[i].Id];
                oppRec.Exclude_From_Trigger__c = true;   
                update oppRec;         
            }        
        }
    }
}
trigger NewUserFeedbackOpportunity on Opportunity (before insert, before update) {

     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    
    // Begin mail message.

        List<String> sendTo = new List<String>();
        sendTo.Add(UserInfo.getUserEmail());
        mail.setToAddresses(sendTo);
        
        mail.setReplyTo('no_reply_SFCD@verimatrix.com');
        mail.setSenderDisplayName('Salesforce Support');
    
        mail.setSubject('Thank you for submitting your feedback!');

    
          List<String> ccTo = new List<String>();
          ccTo.add('ttrevisan@verimatrix.com');
          ccTo.add('jsemingson@verimatrix.com');
          ccTo.add('btankhim@verimatrix.com');
          mail.setCcAddresses(ccTo);
 
      
     // Begin Account feedback field and create a new User Feedback record.   
    for (Opportunity thisOppty : Trigger.new) {
          if (thisOppty.Feedback__c != null) {
            User_Feedback__c UserFeedback = new User_Feedback__c();
            
            UserFeedback.Feedback_Text__c = thisOppty.Feedback__c;
            UserFeedback.Object__c = 'Opportunity';
            UserFeedback.Name = UserInfo.getName();
            
                 String body = 'Dear ' + UserInfo.getFirstName() + ', <br><br>';
                body += 'Thank you for submitting feedback.';
                body += 'We appreciate you Beta testing. <br><br>';        
                body += 'Feedback: '+thisOppty.Feedback__c+' <br><br>';
                body += 'Salesforce Support Team';
                mail.setHtmlBody(body);
                
            thisOppty.Feedback__c = null;
          
            insert UserFeedback;
             
    
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }
  } 
  }
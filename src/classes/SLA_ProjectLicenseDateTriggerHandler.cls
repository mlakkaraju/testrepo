//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
// class is executed by trigger hander
// updates system license dates when Approved for Permanent Licenses (afpl) is checked or 
// Days Licenses Valid (dlv) changes.  If a license request exsists for the current configuration, 
// then we automaticly regenerate licenses.
public with sharing class SLA_ProjectLicenseDateTriggerHandler implements TriggerHandler.HandlerInterface{

	//interface method. Run by handler bindings
	public void handle(){
		Map<Id,Tenrox__c> sysUpdateMap = new Map<Id,Tenrox__c>();
		for(Tenrox__c proj : (List<Tenrox__c>) Trigger.new){
			Tenrox__c oldProj = (Tenrox__c) Trigger.oldMap.get(proj.Id);
			if(isEligible(oldProj, proj)){
				//doesn't handle conflicts...
				System.debug('eligible!');
				sysUpdateMap.put(proj.System__c, proj);
			}
		}

		if(sysUpdateMap.size()==0){
			return;
		}

		//update systems license dates
		List<System__c> systems = [SELECT Id FROM System__c WHERE Id IN : sysUpdateMap.keySet()];
		System.debug(systems);
		for(System__c sys : systems){
			Tenrox__c proj = sysUpdateMap.get(sys.Id);
			sys.LicenseStartDate__c = Date.today();
			Boolean afpl = proj.Approved_for_Permanent_Licenses__c;
			Integer dlv = (Integer) proj.Days_License_Valid__c;
			sys.LicenseEndDate__c = 
			SLA_SystemLicenseCount_Helper.calculateLicenseEndDate(afpl, dlv);
		}
		update systems;
		
		//create new request if one already exists for project system pair
		// this will no scale very well, but it is unlikely to ever have this trigger run for multiple projects
		for(System__c sys : systems){
			Tenrox__c proj = sysUpdateMap.get(sys.Id);
			
			//prevent this LR Generation from running multiple times for the same project
			if(!TriggerRunOnce.isAlreadyDone(proj.Id)){
				try{
					//get last request
					License_Request__c lr = [SELECT System__c, Project__c, Contact_1__c, Contact_2__c, Contact_3__c, Contact_4__c, Contact_5__c, License_End_Date__c 
												FROM License_Request__c
												WHERE Project__c = : proj.Id
												AND System__c = : sys.Id 
												ORDER BY CreatedDate DESC 
												LIMIT 1];

					License_Request__c lrNew = SLA_LicenseRequestHelpers.openNewRequest(lr.System__c, lr.project__c);
					//copy contacts from last request
					lrNew.Contact_1__c = lr.Contact_1__c;
					lrNew.Contact_2__c = lr.Contact_2__c;
					lrNew.Contact_3__c = lr.Contact_3__c;
					lrNew.Contact_2__c = lr.Contact_4__c;
					lrNew.Contact_3__c = lr.Contact_5__c;
					update lrNew;
					SLA_LicenseRequestHelpers.startLicenseGeneration(lrNew);

				}catch(Exception e){
					system.debug('No lr found. Skip Generation');
				}
				TriggerRunOnce.setAlreadyDone(proj.Id);
			}
		}
	}

	//Determines if the project is eligible for license date updates
	//run when:
	//* AFPL was checked or DLV has changed
	//* License_Counts_Set_on_System__c = true
	//* system is set
	private boolean isEligible(Tenrox__c oldProj, Tenrox__c proj){
		
		System.debug('OLD AFPL: ' + oldProj.Approved_for_Permanent_Licenses__c);
		System.debug('NEW AFPL: ' + proj.Approved_for_Permanent_Licenses__c);
		System.debug('OLD DLV: ' + oldProj.Days_License_Valid__c);
		System.debug('NEW DLV: ' + proj.Days_License_Valid__c);
		return(
			(
				(!oldProj.Approved_for_Permanent_Licenses__c && proj.Approved_for_Permanent_Licenses__c) ||
				(oldProj.Days_License_Valid__c != proj.Days_License_Valid__c && proj.Days_License_Valid__c != null)
				) 
			&& proj.License_Counts_Set_on_System__c 
			&& proj.System__c != null
			);
	}
}
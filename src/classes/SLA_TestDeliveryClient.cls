@isTest
public class SLA_TestDeliveryClient {
	
	public static final String RESP_REQUEST_FAILURE = '{"status": 400,"message": "Bad Request"}';
	public static final String RESP_REQUEST_SUCCESS = '{"status": 200,"message": "http://test.com"}'; 

	private static final String MOCK_ID = '0123456789abcdef';
	private static final String MOCK_FILENAME = 'VCAS_0.0.1.zip';

	@isTest static void test_software_request_success() {
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
														SLA_DeliveryClient.ENDPOINT,
														RESP_REQUEST_SUCCESS
													);
		mockResponse.contentType = 'application/json';

		Test.setMock(HttpCalloutMock.class,mockResponse);

		SLA_DeliveryClient.SoftwareRequest request = new SLA_DeliveryClient.SoftwareRequest(MOCK_ID, MOCK_FILENAME);
		SLA_DeliveryClient.APIResponse response = SLA_DeliveryClient.sendRequest(request);
		System.assertEquals(response.status, '200');
		System.assertEquals(response.message, 'http://test.com');
	}

	@isTest static void test_license_request_success() {
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
														SLA_DeliveryClient.ENDPOINT,
														RESP_REQUEST_SUCCESS
													);
		mockResponse.contentType = 'application/json';

		Test.setMock(HttpCalloutMock.class,mockResponse);

		SLA_DeliveryClient.LicenseRequest request = new SLA_DeliveryClient.LicenseRequest(MOCK_ID, 200);
		SLA_DeliveryClient.APIResponse response = SLA_DeliveryClient.sendRequest(request);
		System.assertEquals(response.status, '200');
		System.assertEquals(response.message, 'http://test.com');
	}

	@isTest static void test_software_request_failure() {
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
														SLA_DeliveryClient.ENDPOINT,
														RESP_REQUEST_FAILURE
													);
		mockResponse.contentType = 'application/json';

		Test.setMock(HttpCalloutMock.class,mockResponse);

		SLA_DeliveryClient.SoftwareRequest request = new SLA_DeliveryClient.SoftwareRequest(MOCK_ID, MOCK_FILENAME);
		SLA_DeliveryClient.APIResponse response = SLA_DeliveryClient.sendRequest(request);
		System.assertEquals(response.status, '400');
		System.assertEquals(response.message, 'Bad Request');
	}

	@isTest static void test_license_request_failure() {
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
														SLA_DeliveryClient.ENDPOINT,
														RESP_REQUEST_FAILURE
													);
		mockResponse.contentType = 'application/json';

		Test.setMock(HttpCalloutMock.class,mockResponse);

		SLA_DeliveryClient.LicenseRequest request = new SLA_DeliveryClient.LicenseRequest(MOCK_ID, 200);
		SLA_DeliveryClient.APIResponse response = SLA_DeliveryClient.sendRequest(request);
		System.assertEquals(response.status, '400');
		System.assertEquals(response.message, 'Bad Request');
	}
	
}
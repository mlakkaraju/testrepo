public with sharing class TopSolutions {

	public Integer solutionCount { get; set; }

	public List<Solution> topSolutions {
		get {
			if(topSolutions == null) {
				topSolutions = [select id, solutionName, timesUsed from Solution order by timesUsed desc limit :solutionCount];
			}
			return topSolutions;
		}
		private set;
	}

	@isTest
	private static void basicTopSolutionsTest() {
		TopSolutions controller = new TopSolutions();
		controller.solutionCount = 5;
		List<Solution> topSolutions = controller.topSolutions;
	}
}
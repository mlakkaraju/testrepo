// DeployTriggers.cls
// This class lists all the test methods

@isTest


public class DeployTriggers {


    //Test method to test the trigger UpdateReconciledLicenseCount on Project Object

    Public Static testmethod void testUpdateReconciledLicenseCount() 
    {    
         tenrox__c[] projectArray = [Select Id, Name, Project_Status_CC__c from tenrox__c where Name =: 'GVT M&S for Additional RTES Channels Ericsson PO 4507374174' limit 1 ];
     
         if (projectArray != null)
         
         {
            tenrox__c project =   projectArray [0];
             
            project.Project_Status_CC__c = 'On Hold - License Count Discrepancy';
            
            project.Last_License_Delivery_Count__c = 1000;
            
            project.Last_Channel_Delivery_Count__c = 1000;
            
            //project.Master_Service_Project__c = project.Id;
        
            update projectArray ;
        
            project.Project_Status_CC__c = 'YELLOW';
            
            project.Reconciled_License_Count__c = 10;
        
            update projectArray ; 
         }
     }
     
     
     
    //Test method to test the trigger UpdateHasOpenTimePhasedProject on Project Object 
     
    Public Static testmethod void testUpdateHasOpenTimePhasedProject () 
    {   
         tenrox__c[] projectArray = [Select Id, Name, Revenue_Delivery_Type__c from tenrox__c where Name =: 'GVT M&S for Additional RTES Channels Ericsson PO 4507374174' limit 1 ];
     
         if (projectArray != null)
         
         {
            tenrox__c project =   projectArray [0];
             
            project.Revenue_Delivery_Type__c = 'Regular';
            
            project.Last_Channel_Delivery_Count__c = 1000;
            
             project.Last_Channel_Delivery_Count__c = 1000;
            
            //project.Master_Service_Project__c = project.Id;
        
            update projectArray ;
        
            project.Revenue_Delivery_Type__c = 'Time-phased';
        
            update projectArray ; 
         }
     }
     
/* Original test method for GenerateLicenseFeaturesSettingsBlobOnSystem. Will remove once SystemBlobUtil is complete. -BT
     
        //Test method to test the trigger GenerateLicenseFeaturesSettingsBlobOnSystem on System Object   
   
   Public Static testmethod void testGenerateLicenseFeaturesSettingsBlobOnSystem() 
   {
       Account testAcc =[Select Id from Account limit 1];
        String test = 'test'; 
        System__c systemObj = new System__c();
        systemObj.Account__c = testAcc.Id;
        systemObj.Name = 'test';
        systemObj.VODVideomark__c = 'NO';
        systemObj.BroadcastVideomark__c = 'NO';
        systemObj.RemoteEntitlementManager__c = 'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.MaxBroadcastChannels__c = 100;
        systemObj.MaxInternetStreams__c = 100; 
        systemObj.LicenseEndDate__c = System.today()+ 366;
        systemObj.LicenseStartDate__c = System.today()+1;
        systemObj.LicenseRecyclePeriod__c = 30;
        systemObj.CloneDetection__c =  'YES';
        systemObj.OnScreenDisplay__c =  'YES';
        systemObj.CopyControl__c =  'YES';
        systemObj.VMPLCONFBASE1__c = '0';
        systemObj.VMPLCONFRANGE1__c = '6.8719476735E10';
        systemObj.VMPLTransactionID__c = '68719476735,1,VCAS_ServerTID';
        systemObj.VMPLCONFHASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
        systemObj.IPTVVODEncryption__c =  'YES';
        systemObj.VCPENCKey__c =  'YES';
        systemObj.OTTEnabled__c =  'YES';
        systemObj.MRPREnabled__c =  'YES';
        systemObj.SOCKEM_Enabled__c = 'YES';
        
        //Wholesaler and Retailer features for Case#00032005. 
        //Testing = Successful by Q&A, waiting on Engineering to release Decima Q1-2014. When Engineering ready, uncomment code below -BT (9/12/13) 
        systemObj.Wholesaler__c = 'YES';
        systemObj.Max_Wholesalers__c = 5;
        systemObj.Retailer__c = 'YES';
        systemObj.Max_Retailers__c = 100; 
        
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.LowerBoundforWarningMsg__c = 80;
        systemObj.UpperBoundforIssueCert__c = 120;   
        systemObj.LicenseCountUnknown__c = 100;
        systemObj.LicenseCountIPTVSTB__c = 100;
        systemObj.LicenseCountHybridSTB__c = 100;
        systemObj.LicenseCountDVBSTBwSC__c = 100;
        systemObj.LicenseCountDVBSTBwoSC__c = 100;
        systemObj.LicenseCountDesktopPC__c = 100;
        systemObj.LicenseCountDesktopMac__c = 100;
        systemObj.LicenseCountWebPC__c = 100;
        systemObj.LicenseCountWebMac__c = 100;
        systemObj.LicenseCountWebiPhone__c = 100;
        systemObj.LicenseCountWebAndroid__c = 100;
        systemObj.LicenseCountWebSTB__c = 100;
        systemObj.LicenseCountGroup1__c = 0;
        systemObj.LicenseCountGroup2__c = 0;
        systemObj.LicenseCountGroup3__c = 0;
        systemObj.Type__c = 'Production';
        systemObj.Location__c = '/home/ftp/btankhim1';
        systemObj.CompanyName__c = 'verimatrix';
        systemObj.LocationHostname__c = 'ftp.verimatrix.com';
        systemObj.New_License_Server__c = true; 
        
        insert systemObj;
    }    
*/
//Test method to test the trigger SystemBeforeInsertBeforeUpdate on System Object   
   
   Public Static testmethod void testSystemBeforeInsertBeforeUpdate() 
   {
       Account testAcc =[Select Id from Account limit 1];
        String test = 'test'; 
        System__c systemObj = new System__c();
        systemObj.Account__c = testAcc.Id;
        systemObj.Name = 'test';
        systemObj.VODVideomark__c = 'NO';
        systemObj.BroadcastVideomark__c = 'NO';
        systemObj.RemoteEntitlementManager__c = 'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.MaxBroadcastChannels__c = 100;
        systemObj.MaxInternetStreams__c = 100; 
        systemObj.LicenseEndDate__c = System.today()+ 366;
        systemObj.LicenseStartDate__c = System.today()+1;
        systemObj.LicenseRecyclePeriod__c = 30;
        systemObj.CloneDetection__c =  'YES';
        systemObj.OnScreenDisplay__c =  'YES';
        systemObj.CopyControl__c =  'YES';
        systemObj.VMPLCONFBASE1__c = '0';
        systemObj.VMPLCONFRANGE1__c = '68719476735';
        systemObj.VMPLTransactionID__c = '68719476735,1,VCAS_ServerTID';
        systemObj.VMPLCONFHASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
        systemObj.IPTVVODEncryption__c =  'YES';
        systemObj.VCPENCKey__c =  'YES';
        systemObj.OTTEnabled__c =  'YES';
        systemObj.MRPREnabled__c =  'YES';
        systemObj.SOCKEM_Enabled__c = 'YES';
        
        /*Wholesaler and Retailer features for Case#00032005. 
          Testing = Successful by Q&A, waiting on Engineering to release Decima Q1-2014. When Engineering ready, uncomment code below -BT (9/12/13) 
        systemObj.Wholesaler__c = 'YES';
        systemObj.Max_Wholesalers__c = 5;
        systemObj.Retailer__c = 'YES';
        systemObj.Max_Retailers__c = 100; */
        
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.LowerBoundforWarningMsg__c = 80;
        systemObj.UpperBoundforIssueCert__c = 120;   
        systemObj.LicenseCountUnknown__c = 100;
        systemObj.LicenseCountIPTVSTB__c = 100;
        systemObj.LicenseCountHybridSTB__c = 100;
        systemObj.LicenseCountDVBSTBwSC__c = 100;
        systemObj.LicenseCountDVBSTBwoSC__c = 100;
        systemObj.LicenseCountDesktopPC__c = 100;
        systemObj.LicenseCountDesktopMac__c = 100;
        systemObj.LicenseCountWebPC__c = 100;
        systemObj.LicenseCountWebMac__c = 100;
        systemObj.LicenseCountWebiPhone__c = 100;
        systemObj.LicenseCountWebAndroid__c = 100;
        systemObj.LicenseCountWebSTB__c = 100;
        systemObj.LicenseCountGroup1__c = 0;
        systemObj.LicenseCountGroup2__c = 0;
        //systemObj.LicenseCountGroup3__c = 0;
        systemObj.Type__c = 'Production';
        systemObj.Location__c = '/home/ftp/btankhim1';
        systemObj.CompanyName__c = 'verimatrix';
        systemObj.LocationHostname__c = 'ftp.verimatrix.com';
        systemObj.New_License_Server__c = true;
        systemObj.hid_License_Blobs_Changed__c = true; 
        systemObj.VCAS_Version__c= 'a13500000034gBn';
        systemObj.Certificate__c= 'a0Q5000000GunGu';
        insert systemObj;
    }    

   //Test method to test the trigger GenerateClientLicenseBlobOnSystem on System Object   

   Public Static testmethod void testGenerateClientLicenseBlobOnSystem() 
   {
       Account testAcc =[Select Id from Account limit 1];
        String test = 'test'; 
        System__c systemObj = new System__c();
        systemObj.Account__c = testAcc.Id;
        systemObj.Name = 'test';
        systemObj.VODVideomark__c = 'NO';
        systemObj.BroadcastVideomark__c = 'NO';
        systemObj.RemoteEntitlementManager__c = 'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.MaxBroadcastChannels__c = 100;
        systemObj.MaxInternetStreams__c = 100; 
        systemObj.LicenseEndDate__c = System.today()+ 366;
        systemObj.LicenseStartDate__c = System.today()+1;
        systemObj.LicenseRecyclePeriod__c = 30;
        systemObj.CloneDetection__c =  'YES';
        systemObj.OnScreenDisplay__c =  'YES';
        systemObj.CopyControl__c =  'YES';
        systemObj.VMPLCONFBASE1__c = '0';
        systemObj.VMPLCONFRANGE1__c = '68719476735';
        systemObj.VMPLTransactionID__c = '68719476735,1,VCAS_ServerTID';
        systemObj.VMPLCONFHASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
        systemObj.IPTVVODEncryption__c =  'YES';
        systemObj.VCPENCKey__c =  'YES';
        systemObj.OTTEnabled__c =  'YES';
        systemObj.MRPREnabled__c =  'YES';
        systemObj.SOCKEM_Enabled__c =  'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.LowerBoundforWarningMsg__c = 80;
        systemObj.UpperBoundforIssueCert__c = 120;   
        systemObj.LicenseCountUnknown__c = 100;
        systemObj.LicenseCountIPTVSTB__c = 100;
        systemObj.LicenseCountHybridSTB__c = 100;
        systemObj.LicenseCountDVBSTBwSC__c = 100;
        systemObj.LicenseCountDVBSTBwoSC__c = 100;
        systemObj.LicenseCountDesktopPC__c = 100;
        systemObj.LicenseCountDesktopMac__c = 100;
        systemObj.LicenseCountWebPC__c = 100;
        systemObj.LicenseCountWebMac__c = 100;
        systemObj.LicenseCountWebiPhone__c = 100;
        systemObj.LicenseCountWebAndroid__c = 100;
        systemObj.LicenseCountWebSTB__c = 100;
        systemObj.LicenseCountGroup1__c = 0;
        systemObj.LicenseCountGroup2__c = 0;
        //systemObj.LicenseCountGroup3__c = 0;
        systemObj.Type__c = 'Production';
        systemObj.Location__c = '/home/ftp/btankhim1';
        systemObj.CompanyName__c = 'verimatrix';
        systemObj.LocationHostname__c = 'ftp.verimatrix.com';
        systemObj.New_License_Server__c = true;
        systemObj.Last_New_License_s_Generated_Date_Time__c = System.now();
        systemObj.Last_New_License_s_Generated_Count__c = 1;  
        systemObj.VCAS_Version__c= 'a13500000034gBn';
        systemObj.Certificate__c= 'a0Q5000000GunGu';
        insert systemObj;
    }   
    
    
   //Test method to test LicenseControllerOnSystem class
   
   Public Static testmethod void testLicenseControllerOnSystem() 
   {
       Account testAcc =[Select Id from Account limit 1];
        String test = 'test'; 
        System__c systemObj = new System__c();
        systemObj.Account__c = testAcc.Id;
        systemObj.Name = 'test';
        systemObj.VODVideomark__c = 'NO';
        systemObj.BroadcastVideomark__c = 'NO';
        systemObj.RemoteEntitlementManager__c = 'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.MaxBroadcastChannels__c = 100;
        systemObj.MaxInternetStreams__c = 100; 
        systemObj.LicenseEndDate__c = System.today()+ 366;
        systemObj.LicenseStartDate__c = System.today()+1;
        systemObj.LicenseRecyclePeriod__c = 30;
        systemObj.CloneDetection__c =  'YES';
        systemObj.OnScreenDisplay__c =  'YES';
        systemObj.CopyControl__c =  'YES';
        systemObj.VMPLCONFBASE1__c = '0';
        systemObj.VMPLCONFRANGE1__c = '68719476735';
        systemObj.VMPLTransactionID__c = '68719476735,1,VCAS_ServerTID';
        systemObj.VMPLCONFHASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
        systemObj.IPTVVODEncryption__c =  'YES';
        systemObj.VCPENCKey__c =  'YES';
        systemObj.OTTEnabled__c =  'YES';
        systemObj.MRPREnabled__c =  'YES';
        systemObj.SOCKEM_Enabled__c =  'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.LowerBoundforWarningMsg__c = 80;
        systemObj.UpperBoundforIssueCert__c = 120;   
        systemObj.LicenseCountUnknown__c = 100;
        systemObj.LicenseCountIPTVSTB__c = 100;
        systemObj.LicenseCountHybridSTB__c = 100;
        systemObj.LicenseCountDVBSTBwSC__c = 100;
        systemObj.LicenseCountDVBSTBwoSC__c = 100;
        systemObj.LicenseCountDesktopPC__c = 100;
        systemObj.LicenseCountDesktopMac__c = 100;
        systemObj.LicenseCountWebPC__c = 100;
        systemObj.LicenseCountWebMac__c = 100;
        systemObj.LicenseCountWebiPhone__c = 100;
        systemObj.LicenseCountWebAndroid__c = 100;
        systemObj.LicenseCountWebSTB__c = 100;
        systemObj.LicenseCountGroup1__c = 0;
        systemObj.LicenseCountGroup2__c = 0;
        //systemObj.LicenseCountGroup3__c = 0;
        systemObj.Type__c = 'Production';
        systemObj.Location__c = '/home/ftp/btankhim1';
        systemObj.CompanyName__c = 'verimatrix';
        systemObj.LocationHostname__c = 'ftp.verimatrix.com';
        systemObj.New_License_Server__c = true; 
        systemObj.Last_New_License_s_Generated_Date_Time__c = System.now();
        systemObj.Last_New_License_s_Generated_Count__c = 1;     
        systemObj.VCAS_Version__c= 'a13500000034gBn';
                systemObj.Certificate__c= 'a0Q5000000GunGu';

        Database.SaveResult systemResult = Database.insert(systemObj);
        Id sysId = systemResult.getId();
        
        Server__c serverObj = new Server__c();
        serverObj.Name = 'acsm2';
        serverObj.Server_License_Key__c = 'Sjz7M5EBdhpyu6UTx5ahHf1w3gg=';
        serverObj.License_Start_Date__c = System.today()+2;
        serverObj.Model__c = 'xyz123455';
        serverObj.SerialNo__c = 'a1234567889';
        serverObj.System__c = sysId;
        serverObj.Server_Role__c = 'ACSM';
        serverObj.Server_Purchased_Through__c = 'Customer Sourced';
        serverObj.Inactive__c = false;
        
        Database.SaveResult serverResult = Database.insert(serverObj);
        
        LicenseControllerOnSystem licCont = new LicenseControllerOnSystem();
        LicenseControllerOnSystem.licenseGenerator((String)sysId);
    }
    
    Public Static testmethod void testGenerateLicenseFeaturesSettingsBlob()
{
   tenrox__c testTenrox =[Select Id from tenrox__c limit 1];
   Site_License__c testSiteLicense =[Select Id from Site_License__c limit 1];
   
   License_Key__c LicObj = new License_Key__c();
   
   LicObj.Lower_Bound_for_Warning_Msg_Android__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DVB_SC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DVB_nSC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Dsktp_PC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DsktpMac__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_1__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_2__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_3__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Hybrid__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_IPTV_STB__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Legacy__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_RSM__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Receiver__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_Mac__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Unknown__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_PC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_STB__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_iPhone__c = 80;
   LicObj.Upper_Bound_for_Issue_Cert_Android__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_DVB_nSC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_DVB_SC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Dsktp_Mac__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Dsktp_PC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_1__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_2__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_3__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Hybrid__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_IPTV_STB__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Legacy__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_RSM__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Receiver__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Unknown__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_Mac__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_PC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_STB__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_iPhone__c = 120;

    //Test Group fucntionliaty for OTT -BT 9/12/13
   LicObj.Client_Device_Group_Web_Android__c = 'Group 1';
   LicObj.Client_Device_Group_Web_Mac__c = 'Group 1';
   LicObj.Client_Device_Group_Web_PC__c = 'Group 1';
   LicObj.Client_Device_Group_Web_STB__c = 'Group 1';
   LicObj.Client_Device_Group_iPhone__c = 'Group 1';
   
   //Test Group Functionality for IPTV and Legacy -BT 9/12/13
   LicObj.Client_Device_Group_Legacy__c = 'Group 3';
   LicObj.Client_Device_Group_IPTV_STB__c = 'Group 3';

   LicObj.Broadcast_Videomark__c = 'NO';
   LicObj.Clone_Detection__c = 'YES';
   LicObj.Company_Name__c = 'test';
   LicObj.Copy_Control__c = 'YES';
   LicObj.VOD_Encryption__c = 'YES';
   
   //Test license counts -BT 9/12/13
   LicObj.License_Count_DVB_STB_w_SC__c = 1;
   LicObj.License_Count_DVB_STB_w_o_SC__c = 1;
   LicObj.License_Count_Desktop_Mac__c = 1;
   LicObj.License_Count_Desktop_PC__c = 1;
   LicObj.License_Count_Hybrid_STB__c = 1;
   LicObj.License_Count_Prof_Receiver__c = 1;
   LicObj.License_Count_RSM__c = 1;
   LicObj.License_Count_Unknown__c = 1;
   
   //Set to "0" to test Group 3 functionality -BT 9/12/13
   LicObj.License_Count_IPTV_STB__c = 0;
   LicObj.License_Count_Legacy__c = 0;
   
   //Set to "0" to test Group 1 functionality -BT 9/12/13
   LicObj.License_Count_Web_Android__c = 0;
   LicObj.License_Count_Web_Mac__c = 0;
   LicObj.License_Count_Web_PC__c = 0;
   LicObj.License_Count_Web_STB__c = 0;
   LicObj.License_Count_Web_iPhone__c = 0;
   
   //Test Groups with devices by setting Group 1 and Group 3 = to "1" -BT 9/12/13 
   LicObj.License_Count_Group_1__c = 1;
   LicObj.License_Count_Group_2__c = 0;
   LicObj.License_Count_Group_3__c = 1;
   
   LicObj.License_Start_Date__c =  System.today() + 1;
   LicObj.License_End_Date__c = System.today() + 366;
   LicObj.License_Recycle_Period__c = 7;
   LicObj.Location_Hostname__c = '/home/ftp/btankhim/licenses';
   LicObj.Location__c = 'ftp.verimatrix.com';
   LicObj.MRPR_Enabled__c = 'YES';
   LicObj.SOCKEM_Enabled__c = 'YES';
   
   /*Wholesaler and Retailer features for Case#00032005. 
       Testing = Successful by Q&A, waiting on Engineering to release Decima Q1-2014. When Engineering ready, uncomment code below -BT (9/12/13) 
   LicObj.Wholesaler__c = 'YES';
   LicObj.Max_Wholesalers__c = 5;
   LicObj.Retailer__c = 'YES';
   LicObj.Max_Retailers__c = 100; */
   
   LicObj.Max_Broadcast_Channels__c = 0;
   LicObj.Max_Internet_Streams__c = 10;
   LicObj.New_License_Server__c = true;
   LicObj.OTT_Enabled__c = 'YES';
   LicObj.On_Screen_Display__c = 'YES';
   LicObj.Project__c = testTenrox.Id; 
   LicObj.Remote_Entitlement_Manager__c = 'YES';
   LicObj.Server_License_Key__c = 'test';
   LicObj.Server_Name__c = 'test';
   LicObj.Site_License__c = testSiteLicense.Id;
   LicObj.VCP_ENC_Key__c = 'E985B4C2685FADE8';
   LicObj.VMPL_TransactionID__c = '68719476735,1,VCAS_ServerTID';
   LicObj.VMPLCONF_BASE1__c = '0';
   LicObj.VMPLCONF_HASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
   LicObj.VMPLCONF_RANGE1__c = '68719476735';
   LicObj.VOD_Videomark__c = 'YES';
   //LicObj.Retrieval_Location_Hostname__c = 
   //LicObj.Retrieval_Location_Path__c =
   //LicObj.Certificate__c = 
   //LicObj.Max_Certificates_for_Clients__c = 
   //LicObj.Max_VOD_Count__c = 
   
   insert LicObj;
}

Public Static testmethod void testGenerateClientLicenseBlob()
{
   tenrox__c testTenrox =[Select Id from tenrox__c limit 1];
   Site_License__c testSiteLicense =[Select Id from Site_License__c limit 1];
   
   License_Key__c LicObj = new License_Key__c();
   
   LicObj.Lower_Bound_for_Warning_Msg_Android__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DVB_SC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DVB_nSC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Dsktp_PC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_DsktpMac__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_1__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_2__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Group_3__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Hybrid__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_IPTV_STB__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Legacy__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_RSM__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Receiver__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_Mac__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Unknown__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_PC__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_Web_STB__c = 80;
   LicObj.Lower_Bound_for_Warning_Msg_iPhone__c = 80;
   LicObj.Upper_Bound_for_Issue_Cert_Android__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_DVB_nSC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_DVB_SC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Dsktp_Mac__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Dsktp_PC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_1__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_2__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Group_3__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Hybrid__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_IPTV_STB__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Legacy__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_RSM__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Receiver__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Unknown__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_Mac__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_PC__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_Web_STB__c = 120;
   LicObj.Upper_Bound_for_Issue_Cert_iPhone__c = 120;

    //Test Group fucntionliaty for OTT -BT 9/12/13
   LicObj.Client_Device_Group_Web_Android__c = 'Group 1';
   LicObj.Client_Device_Group_Web_Mac__c = 'Group 1';
   LicObj.Client_Device_Group_Web_PC__c = 'Group 1';
   LicObj.Client_Device_Group_Web_STB__c = 'Group 1';
   LicObj.Client_Device_Group_iPhone__c = 'Group 1';
   
   //Test Group Functionality for IPTV and Legacy -BT 9/12/13
   LicObj.Client_Device_Group_Legacy__c = 'Group 3';
   LicObj.Client_Device_Group_IPTV_STB__c = 'Group 3';

   LicObj.Broadcast_Videomark__c = 'NO';
   LicObj.Clone_Detection__c = 'YES';
   LicObj.Company_Name__c = 'test';
   LicObj.Copy_Control__c = 'YES';
   LicObj.VOD_Encryption__c = 'YES';
   
   //Test license counts -BT 9/12/13
   LicObj.License_Count_DVB_STB_w_SC__c = 1;
   LicObj.License_Count_DVB_STB_w_o_SC__c = 1;
   LicObj.License_Count_Desktop_Mac__c = 1;
   LicObj.License_Count_Desktop_PC__c = 1;
   LicObj.License_Count_Hybrid_STB__c = 1;
   LicObj.License_Count_Prof_Receiver__c = 1;
   LicObj.License_Count_RSM__c = 1;
   LicObj.License_Count_Unknown__c = 1;
   
   //Set to "0" to test Group 3 functionality -BT 9/12/13
   LicObj.License_Count_IPTV_STB__c = 0;
   LicObj.License_Count_Legacy__c = 0;
   
   //Set to "0" to test Group 1 functionality -BT 9/12/13
   LicObj.License_Count_Web_Android__c = 0;
   LicObj.License_Count_Web_Mac__c = 0;
   LicObj.License_Count_Web_PC__c = 0;
   LicObj.License_Count_Web_STB__c = 0;
   LicObj.License_Count_Web_iPhone__c = 0;
   
   //Test Groups with devices by setting Group 1 and Group 3 = to "1" -BT 9/12/13 
   LicObj.License_Count_Group_1__c = 1;
   LicObj.License_Count_Group_2__c = 0;
   LicObj.License_Count_Group_3__c = 1;
   
   LicObj.License_Start_Date__c =  System.today() + 1;
   LicObj.License_End_Date__c = System.today() + 366;
   LicObj.License_Recycle_Period__c = 7;
   LicObj.Location_Hostname__c = '/home/ftp/btankhim/licenses';
   LicObj.Location__c = 'ftp.verimatrix.com';
   LicObj.MRPR_Enabled__c = 'YES';
   LicObj.SOCKEM_Enabled__c = 'YES';
   
   /*Wholesaler and Retailer features for Case#00032005. 
       Testing = Successful by Q&A, waiting on Engineering to release Decima Q1-2014. When Engineering ready, uncomment code below -BT (9/12/13) 
   LicObj.Wholesaler__c = 'YES';
   LicObj.Max_Wholesalers__c = 5;
   LicObj.Retailer__c = 'YES';
   LicObj.Max_Retailers__c = 100; */
   
   LicObj.Max_Broadcast_Channels__c = 0;
   LicObj.Max_Internet_Streams__c = 10;
   LicObj.New_License_Server__c = true;
   LicObj.OTT_Enabled__c = 'YES';
   LicObj.On_Screen_Display__c = 'YES';
   LicObj.Project__c = testTenrox.Id; 
   LicObj.Remote_Entitlement_Manager__c = 'YES';
   LicObj.Server_License_Key__c = 'test';
   LicObj.Server_Name__c = 'test';
   LicObj.Site_License__c = testSiteLicense.Id;
   LicObj.VCP_ENC_Key__c = 'E985B4C2685FADE8';
   LicObj.VMPL_TransactionID__c = '68719476735,1,VCAS_ServerTID';
   LicObj.VMPLCONF_BASE1__c = '0';
   LicObj.VMPLCONF_HASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
   LicObj.VMPLCONF_RANGE1__c = '68719476735';
   LicObj.VOD_Videomark__c = 'YES';
   //LicObj.Retrieval_Location_Hostname__c = 
   //LicObj.Retrieval_Location_Path__c =
   //LicObj.Certificate__c = 
   //LicObj.Max_Certificates_for_Clients__c = 
   //LicObj.Max_VOD_Count__c = 
   
   insert LicObj;
}
 
 
 
    //Test method to test LicenseRegeneratorOnSystem class
   
   Public Static testmethod void testLicenseRegeneratorOnSystem() 
   {
        Account testAcc =[Select Id from Account limit 1];
        String test = 'test'; 
        System__c systemObj = new System__c();
        systemObj.Account__c = testAcc.Id;
        systemObj.Name = 'test';
        systemObj.VODVideomark__c = 'NO';
        systemObj.BroadcastVideomark__c = 'NO';
        systemObj.RemoteEntitlementManager__c = 'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.MaxBroadcastChannels__c = 100;
        systemObj.MaxInternetStreams__c = 100; 
        systemObj.LicenseEndDate__c = System.today()+ 366;
        systemObj.LicenseStartDate__c = System.today()+1;
        systemObj.LicenseRecyclePeriod__c = 30;
        systemObj.CloneDetection__c =  'YES';
        systemObj.OnScreenDisplay__c =  'YES';
        systemObj.CopyControl__c =  'YES';
        systemObj.VMPLCONFBASE1__c = '0';
        systemObj.VMPLCONFRANGE1__c = '68719476735';
        systemObj.VMPLTransactionID__c = '68719476735,1,VCAS_ServerTID';
        systemObj.VMPLCONFHASH__c = 'd842a049b065c3164b3f1ebfdffd085dda036064';
        systemObj.IPTVVODEncryption__c =  'YES';
        systemObj.VCPENCKey__c =  'YES';
        systemObj.OTTEnabled__c =  'YES';
        systemObj.MRPREnabled__c =  'YES';
        systemObj.SOCKEM_Enabled__c =  'YES';
        systemObj.LicenseCountLegacy__c = 100;
        systemObj.LowerBoundforWarningMsg__c = 80;
        systemObj.UpperBoundforIssueCert__c = 120;   
        systemObj.LicenseCountUnknown__c = 100;
        systemObj.LicenseCountIPTVSTB__c = 100;
        systemObj.LicenseCountHybridSTB__c = 100;
        systemObj.LicenseCountDVBSTBwSC__c = 100;
        systemObj.LicenseCountDVBSTBwoSC__c = 100;
        systemObj.LicenseCountDesktopPC__c = 100;
        systemObj.LicenseCountDesktopMac__c = 100;
        systemObj.LicenseCountWebPC__c = 100;
        systemObj.LicenseCountWebMac__c = 100;
        systemObj.LicenseCountWebiPhone__c = 100;
        systemObj.LicenseCountWebAndroid__c = 100;
        systemObj.LicenseCountWebSTB__c = 0;
        systemObj.LicenseCountGroup1__c = 0;
        systemObj.LicenseCountGroup2__c = 0;
        systemObj.Type__c = 'Production';
        systemObj.Location__c = '/home/ftp/btankhim1';
        systemObj.LocationHostname__c = 'ftp.verimatrix.com';
        systemObj.RegenerationLocationPath__c = '/home/ftp/btankhim1';    
        systemObj.RegenerationLocationHostname__c = 'ftp.verimatrix.com';  
        systemObj.CompanyName__c = 'verimatrix';
        systemObj.New_License_Server__c = true;
        systemObj.Last_New_License_s_Generated_Date_Time__c = System.now();
        systemObj.Last_New_License_s_Generated_Count__c = 1;     
        systemObj.VCAS_Version__c= 'a13500000034gBn';
        systemObj.Certificate__c= 'a0Q5000000GunGu';
        Database.SaveResult systemResult = Database.insert(systemObj);
        Id sysId = systemResult.getId();
        
        Server__c serverObj = new Server__c();
        serverObj.Name = 'acsm2';
        serverObj.Server_License_Key__c = 'Sjz7M5EBdhpyu6UTx5ahHf1w3gg=';
        serverObj.License_Start_Date__c = System.today()+2;
        serverObj.Model__c = 'xyz123455';
        serverObj.SerialNo__c = 'a1234567889';
        serverObj.System__c = sysId;
        serverObj.Server_Role__c = 'ACSM';
        serverObj.Server_Purchased_Through__c = 'Customer Sourced';
        serverObj.Inactive__c = false;
        
        Database.SaveResult serverResult = Database.insert(serverObj);
        
        LicenseRegeneratorOnSystem licCont = new LicenseRegeneratorOnSystem();
        LicenseRegeneratorOnSystem.LicenseGet((String)sysId);
    }  
    
      
        
}
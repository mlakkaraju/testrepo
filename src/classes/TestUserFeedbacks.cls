@isTest (seeAllData=true)
   
    public class TestUserFeedbacks { 
        static testMethod void insertNewAccount() {
            
            Account accountToCreate = new Account();
            
            accountToCreate.Name = 'This New Account';
            accountToCreate.Sales_Status__c = 'Prospect';
            accountToCreate.Type = 'Operator'; 
            accountToCreate.Region__c = 'Americas'; 
            accountToCreate.Feedback__c = 'Test feedback for test class'; 
      
            insert accountToCreate;        
         
           
            accountToCreate.Feedback__c = 'Test feedback for test class once again'; 
            
            update accountToCreate;
            
            // opportunity test
            Opportunity opportunityToCreate = new Opportunity();
            
            opportunityToCreate.Name = 'This New Opportunity';
            opportunityToCreate.AccountId = accountToCreate.Id;
            opportunityToCreate.Class__c = '1';
            opportunityToCreate.Sales_Region__c = 'Americas'; 
            opportunityToCreate.CloseDate = date.today(); 
            opportunityToCreate.Type = 'Demo/Lab System'; 
            opportunityToCreate.VCAS_Solution__c = 'IPTV'; 
            opportunityToCreate.Verimatrix_Entity__c = 'Verimatrix Inc'; 
            opportunityToCreate.PMO_Required__c = 'No'; 
            opportunityToCreate.Amount = Decimal.valueOf('100'); 
            opportunityToCreate.StageName = 'Prospecting'; 
            opportunityToCreate.Direct__c = TRUE;
            
            opportunityToCreate.Feedback__c = 'Test feedback for test class';      
          
            
            insert opportunityToCreate;
            
            opportunityToCreate.Feedback__c = 'Test feedback for test class one again'; 
            
            update opportunityToCreate;
            delete opportunityToCreate;
            delete accountToCreate;
            
        }
        
    }
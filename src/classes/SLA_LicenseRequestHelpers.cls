//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Helpers for the License_Request__c Object

public without sharing class SLA_LicenseRequestHelpers {
    public static final String STATUS_OPEN = 'Opened';
    public static final String STATUS_GENERATING = 'Generating';
    public static final String STATUS_GENERATION_COMPLETE = 'Generation Complete';
    public static final String STATUS_READY = 'Ready';
    public static final String STATUS_REPLACED = 'Replaced';
    public static final String STATUS_COMPLETE = 'Completed';
    public static final String STATUS_FAILED = 'Generation Failure';
    public static final String STATUS_FAILED_EC2 = 'License Retrieval Failed';


    public static License_Request__c openNewRequest(Id systemId){
        return openNewRequest(systemId, null);
    }


    public static License_Request__c openNewRequest(Id systemId, Id projectId){
        System__c sys = [SELECT License_Features_Settings_Blob__c,
        Client_Licenses_Blob__c,
        VCAS_version__c,
        (SELECT Name FROM Servers__r WHERE Inactive__c = false)
        FROM System__c
        WHERE Id = :systemId 
        LIMIT 1];

        List<String> hostNames = new List<String>();
        for(Server__c server : sys.Servers__r){
            hostNames.add(server.Name);
        }

        String hostNamesString = String.Join(hostNames, ',');

        License_Request__c lr = new License_Request__c(
            System__c=systemId, 
            VCAS_Version__c = sys.VCAS_version__c, 
            Status__c=SLA_LicenseRequestHelpers.STATUS_OPEN,
            License_Features_Settings_Blob__c = sys.License_Features_Settings_Blob__c,
            Client_Licenses_Blob__c = sys.Client_Licenses_Blob__c,
            ServerHostsCSV__c = hostNamesString,
            Number_of_Licenses__c = sys.Servers__r.size(),
            Generate_Certificate__c = false,
            Project__c = projectId
            );
        insert lr;
        return lr;
    }

    public static List<License_Request__c> findLicenseRequestBySystem(Id systemId){
        return [SELECT Name,
        Status__c,
        VCAS_Version__c,
        CreatedDate,
        CreatedById 
        FROM License_Request__c 
        WHERE System__c =:systemId
        ORDER BY CreatedDate DESC
        ];
    }


    public static void startLicenseGeneration(License_Request__c lr){
        System.debug(lr);
        lr.Status__c =  STATUS_GENERATING;
        lr.Generation_Request_Date__c = DateTime.now();
        update lr;

        //start L&C batch
        BatchLicenseGenerator batch = new BatchLicenseGenerator(lr);
        Database.executeBatch(batch,1);
    }

    public static void requestLicenses(License_Request__c requestObj){

        Integer licenseCount = (Integer) requestObj.Number_of_Licenses__c;
        if(requestObj.Generate_Certificate__c){
            licenseCount++;
        }

        SLA_DeliveryClient.LicenseRequest req = new SLA_DeliveryClient.LicenseRequest(
            requestObj.Id, licenseCount);
        
        SLA_DeliveryClient.APIResponse resp;
        if(!Test.isRunningtest()){
            resp = SLA_DeliveryClient.sendRequest(req);
        }else{
            resp = new SLA_DeliveryClient.APIResponse();
            resp.status = '200';
            resp.message = 'http://test.com';
        }
        
        if(resp.status == '200'){
            requestObj.Download_URL__c = resp.message;
        }else{
            System.Debug(resp.message);
            throw new LicenseRequestException('Failed to get download link from AWS! Message from EC2: ' + resp.message);
        }
        //email users1, 2 & 3

        List<Id> contactIds = new List<Id>();
        
        if(requestObj.Contact_1__c != null){
            contactIds.add(requestObj.Contact_1__c);
        }
        if(requestObj.Contact_2__c != null){
            contactIds.add(requestObj.Contact_2__c);
        }
        if(requestObj.Contact_3__c != null){
            contactIds.add(requestObj.Contact_3__c);
        }
        if(requestObj.Contact_4__c != null){
            contactIds.add(requestObj.Contact_4__c);
        }
        if(requestObj.Contact_5__c != null){
            contactIds.add(requestObj.Contact_5__c);
        }

        System.debug('Contact IDs'+ contactIds);
        if(contactIds.size() > 0){
            //[TODO: Use different templates depending on generating user (communitites vs internal)]
            //ALSO: Instead of modifying the activity in the function it should likely return the activity Id and do so here.
            SLA_Communities_Helpers.sendCommunityEmail2(contactIds, Label.SLA_LicenseDownloadReadyEmailTemplateInternal, requestObj.Project__c, requestObj);
           // SLA_Communities_Helpers.sendCommunityEmail(contactIds, Label.SLA_LicenseDownloadReadyEmailTemplate, requestObj.Id);
        }

        System.debug('Replacing licenses');
        requestObj.Status__c = STATUS_READY;
        
        List<License_Request__c> lrs = [SELECT ID FROM License_Request__c WHERE System__c = :requestObj.System__c AND Status__c = : SLA_LicenseRequestHelpers.STATUS_READY];
        System.debug(lrs);
        for(License_Request__c lr : lrs){
            lr.status__c = SLA_LicenseRequestHelpers.STATUS_REPLACED;
        }   
        update lrs;             
        
        update requestObj;

        if(requestObj.project__c != null){
            SLA_ProjectHelpers.completeProject(requestObj.project__c);
        }
        
    }

    public static Boolean isRecent(Id licenseRequestId){
        License_Request__c lr = [SELECT Client_Licenses_Blob__c, 
        License_Features_Settings_Blob__c,
        Number_of_Licenses__c,
        Generation_Complete_Date__c,
        System__r.Client_Licenses_Blob__c, 
        System__r.License_Features_Settings_Blob__c,
        System__r.Last_New_License_s_Generated_Count__c,
        System__r.Last_New_License_s_Generated_Date_Time__c
        FROM License_Request__c WHERE Id = :licenseRequestId];

        System.Debug(lr.License_Features_Settings_Blob__c + '|' + lr.System__r.License_Features_Settings_Blob__c);

        //don't let the check fail if null
        if(lr.System__r.Last_New_License_s_Generated_Count__c == null){
            lr.System__r.Last_New_License_s_Generated_Count__c = lr.Number_of_Licenses__c;
        }
        //check that values are equal
        if(lr.Client_Licenses_Blob__c != lr.System__r.Client_Licenses_Blob__c ||
            lr.License_Features_Settings_Blob__c != lr.System__r.License_Features_Settings_Blob__c ||
            lr.Number_of_Licenses__c != lr.System__r.Last_New_License_s_Generated_Count__c ||
            lr.Generation_Complete_Date__c != lr.System__r.Last_New_License_s_Generated_Date_Time__c){
            return false;
        }

        return true;
    }

    public class LicenseRequestException extends Exception{}
}
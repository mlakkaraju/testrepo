//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Controller for the communities system overview page
//      Shows Customers their systems with related license request 
//      and actions they can take on them

public without sharing class SLA_CustomerLicenseAccessController {

    private static final Integer MAX_LR = 10;

    public List<System__c> systems {get; private set;}
    public Map<Id,RequestAction> requestActions {get; set;}
    public Map<Id,List<License_Request__c>> systemLRMap {get; set;}

    public SLA_CustomerLicenseAccessController() {
        try{
            Id userId = UserInfo.getUserId(); 
            User u = [SELECT contactId FROM User WHERE id=:userId];
 
            SLA_EntitledAccessRetriever sr = new SLA_EntitledAccessRetriever(u);
            systems = sr.getSystems();

            requestActions = new Map<Id,RequestAction>();
            systemLRMap = new Map<Id,List<License_Request__c>>();
            for(System__c sys : systems){
                Boolean linkCreated = false;
                systemLRMap.put(sys.Id, filterLicenseRequest(sys.License_Requests__r));
                
                for(License_Request__c req : systemLRMap.get(sys.Id)){
                    linkCreated = addRequestAction(req, linkCreated);
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Line: ' + e.getLineNumber() + ' Message: ' + e.getMessage()));
        }
    }

    //function used to determine what actions can be made against a license request
    //Basicly there are too actions you can complete here:
    // 1: download the completed license
    // 2: request a new generation on a license download that has expired
    // Returns true if a link has been added
    private Boolean addRequestAction(License_Request__c request, Boolean skipLink){
        String text = null;
        String url = null;
        
        if(request.status__c == SLA_LicenseRequestHelpers.STATUS_OPEN || request.status__c == SLA_LicenseRequestHelpers.STATUS_GENERATING 
            || request.status__c == SLA_LicenseRequestHelpers.STATUS_GENERATING){
            text = 'Generation In Progress';
            url = null;
        }else if(request.status__c == SLA_LicenseRequestHelpers.STATUS_READY){
                text = 'Download License'; 
                url = request.Download_URL__c;
        }else if(request.status__c == SLA_LicenseRequestHelpers.STATUS_COMPLETE){ //could optionally check if its still up-to-date
            if(skipLink){
                text = 'Complete';
            }else{
                text = 'Re-Request Licenses';
                PageReference pageRef = Page.SLA_LicenseRequestWizard_System1;
                pageRef.getParameters().put('system_id',request.System__c);
                pageRef.setRedirect(true);
                url =  Site.getCurrentSiteUrl() + pageRef.getUrl().subString(1);
            }
        }else if(request.status__c == SLA_LicenseRequestHelpers.STATUS_FAILED){
            text = 'Contact Support';
            url = null;
        }else{
            text = '';
            url = null;
        }

        requestActions.put(request.id, new RequestAction(text,url));
        if(url == null && skipLink == false){
            return false;
        }else{
            return true;
        }
    }

    private List<License_Request__c> filterLicenseRequest(List<License_Request__c> lrs){        
        List<License_Request__c> retList = new List<License_Request__c>();
        for(License_Request__c lr : lrs){
            if(retList.size() < MAX_LR){
                    retList.add(lr);
            }  
        }

        return retList;
    }

    public class RequestAction{
        public String text {get; set;}
        public String url {get; set;}
        public RequestAction(String text, String url){
            this.text = text;
            this.url = url;
        }
    }
}
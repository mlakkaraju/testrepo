//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Class that handles creating community users

public with sharing class SLA_CommunityUserFactory {
	private static final String CUSTOMER_PROFILE = Label.SLA_CustomerProfile;
	private static final String PARTNER_PROFILE = Label.SLA_PartnerProfile;

	public static User createUserFromContact(Id contactId, Boolean partner){
		
		//check to see if user already exists
		try{
			User u = [SELECT id FROM User WHERE contactId =:contactId AND ContactId != null LIMIT 1];
			return u;
		}catch(Exception e){} //were good. no user found.

		Contact contact = [SELECT id, name, email, lastName, firstName FROM Contact WHERE id=:contactId  LIMIT 1];
		
		String profileName = CUSTOMER_PROFILE;
		if(partner){
			profileName = PARTNER_PROFILE;
		}
		

		Profile profile;
		try{
			profile = [SELECT id, name FROM Profile WHERE name =:profileName LIMIT 1];
			}catch(Exception e){
				throw new CommunityUserException('Could not create user; Failed to find Profile!');
			}

			String nickName = generateCommunityNickname(contact);

			User user = new User(
				contactId=contact.Id,
				username=contact.Email,
				firstName=contact.FirstName,
				lastName=contact.LastName,
				email=contact.Email,
				communityNickname = nickName,
				alias = string.valueof(contact.FirstName!=null?contact.FirstName.substring(0,1):'' + contact.LastName.substring(0,1)),
				profileId = profile.Id,
				emailEncodingKey='UTF-8',
				languageLocaleKey='en_US',
				localesIdKey='en_US',
			timezonesIdKey='America/Los_Angeles'); //not sure about some of the local stuff

			//insert the user, force email to be sent
			Database.DMLOptions dlo = new Database.DMLOptions();
			dlo.EmailHeader.triggerUserEmail = true;
			Database.SaveResult result = Database.insert(user,dlo);
			if(!result.success){
				String errString = '';
				for(Integer i = 0; i < result.errors.size(); i++){
					Database.Error err = result.errors[i];
					errString += 'Error #' + (i+1) +': ' + err.message + '.\n';
					errString += 'Related Fields: ' + String.join(err.fields, ', ') + '.\n';
					System.Debug(errString);
				}
				throw new CommunityUserException('Failed to create User! ' + errString);
			}
			return user;
		}

	//Generates a unique nickname
	private static String generateCommunityNickname(Contact contact){
		String nickName = string.valueof(contact.FirstName!=null? contact.FirstName.substring(0,1) + '_' : '') + contact.LastName;

		String dupNameQry = 'SELECT communityNickname FROM User WHERE communityNickname LIKE \'' + nickName + '%\'';
		List<User> userConflicts = Database.Query(dupNameQry);

		Set<String> dupNames = new Set<String>();
		for(User u : userConflicts){
			dupNames.add(u.communityNickname);
		}

		Integer conflictResolver = 0;

		while(true){
			String qualifiedName = nickName;
			if(conflictResolver > 0){
				qualifiedName += String.valueOf(conflictResolver);
			}
			if(dupNames.contains(qualifiedName)){
				conflictResolver ++;
				}else{
					nickName = qualifiedName;
					break;
				}
			}
			return nickName;
		}

		public class CommunityUserException extends Exception{}
	}
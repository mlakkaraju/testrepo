//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Controller for getting the customer to accept "Terms of Service" for software beta access downloads
public without sharing class SLA_CustomerBetaTOSController {

	private final VCAS_ViewRight_Version__c version;

    public Beta_Access_Association__c betaAccess {get; set;}

    private Id accountId;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SLA_CustomerBetaTOSController() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'By clicking Accept, I hereby agree, on behalf of and with authority of the entity listed below, to the Verimatrix Beta Program Terms and Conditions.'));

        this.version = [SELECT stage__c, 
        Delivery_File_Name__c 
        FROM VCAS_ViewRight_Version__c 
        WHERE Id =:ApexPages.currentPage().getParameters().get('id')
        LIMIT 1];

        User u = [SELECT contactId FROM User WHERE Id = :UserInfo.getUserId()];

        Contact c = [SELECT AccountId,
                    FirstName, 
                    LastName, 
                    Email,
                    Title, 
                    MailingStreet, 
                    MailingCity, 
                    MailingState, 
                    MailingPostalCode, 
                    MailingCountry,
                    Account.Name 
                    FROM Contact 
                    WHERE Id=:u.contactId 
                    LIMIT 1];

        this.accountId = c.accountId;
       
        betaAccess = [SELECT First_Name__c,
            Last_Name__c, 
            Email__c,
            Title__c, 
            Full_Company_Name__c, 
            Accepted_TOS__c, 
            Street__c, 
            City__c, 
            Postal_Code__c,
            State__c,
            Country__c
            FROM Beta_Access_Association__c 
            WHERE Account__c = :accountId 
            AND VCAS_Version__c = :version.Id LIMIT 1];

            betaAccess.First_Name__c = c.FirstName;
            betaAccess.Last_Name__c = c.LastName;
            betaAccess.Email__c = c.Email;
            betaAccess.Title__c = c.Title;
            betaAccess.Full_Company_Name__c = c.Account.Name;
            betaAccess.Street__c = c.MailingStreet;
            betaAccess.City__c = c.MailingCity;
            betaAccess.Postal_Code__c = c.MailingPostalCode;
            betaAccess.State__c = c.MailingState;
            betaAccess.Country__c = c.MailingCountry;
    }

    public PageReference acceptTOS(){
        betaAccess.Accepted_TOS__c = true;
        betaAccess.Accepted_By__c = UserInfo.getUserId();
        betaAccess.Accepted_On__c = Datetime.Now();
        update betaAccess;

        PageReference pageRef = Page.SLA_SoftwareDownload;
        pageRef.getParameters().put('id', version.Id);
        return pageRef;
    }

    public PageReference backToList(){
        return Page.SLA_CustomerSoftwareAccess;
    }

    public String getTOSUrl(){
        return Label.SLA_TOSUrl;
    }
}
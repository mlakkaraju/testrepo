//Updated by: Charlie@callawaycloudconsulting.com
//Refactor of UpdateReconciledLicenseCount.Trigger to use ProjectMasterTrigger instead.
public with sharing class ProjectUpdateReconLicenseCount_TH implements TriggerHandler.HandlerInterface {
	
	public void handle(){
		Map<Id, String> accountMap = new Map<Id, String>();

		for (Integer i=0; i<Trigger.new.size();i++)
		{
			Tenrox__c newProj = (Tenrox__c) Trigger.new[i];
			String project_substatus_new = newProj.Project_Substatus__c;
			String project_substatus_old = ((Tenrox__c) Trigger.old[i]).Project_Substatus__c;

			if (project_substatus_old == 'License Count Discrepancy' && project_substatus_new != project_substatus_old && newProj.Account__c != null)
			{
				accountMap.put(newProj.Account__c, '' );
			}  
		}
		Boolean empty = accountMap.isEmpty();

		if (!empty)
		{
			Set <Id> accountIdSet = new Set<Id>(); 
			accountIdSet = accountMap.keySet();

			Account[] accounts = [Select Id, Reconciled_License_Count__c from Account where Id in: accountIdSet];

			for (Account acc : accounts)
			{
				acc.Reconciled_License_Count__c = true;
			}
			update accounts;
		}
	}
}
Global class BatchLicenseRegeneratorOnSystem implements Database.Batchable<sObject>,Database.AllowsCallouts
{

    //Class Constructor
        public BatchLicenseRegeneratorOnSystem() 
        {
        }
        List<Server__c> li = new List<Server__c>();
        String id;  
        String query;  
        String serverLicenseKey;  
        String serverId; 
        String serverName; 
        Integer output;    
        User User;   
        System__c Lic; 
        String xsCompany;
        String xsType;
        Integer countLicensesLastGeneratedOnSystem=0;
        String xsServerData;  
        String xsClientData;  
        String xsUsername; 
        String xsPassword; 
        String xsHostname; 
        String xsPath; 
        Boolean xsNewLicenseServer; 
        Date LicenseStartDate; 
        LicenseCertificateMasterClass.clicgen stub; 
        Integer count=0; 
        public BatchLicenseRegeneratorOnSystem(String tid)
        {
        id = tid;
        query = 'Select Id, Name, Server_License_Key__c ,Success_Error_Code__c,License_Start_Date__c,Inactive__c  from Server__c where System__c =: id';
         stub = new LicenseCertificateMasterClass.clicgen();  
         stub.Timeout_x = 30000;    
         stub.clientCert_x = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA';
         stub.clientCertPasswd_x = 'verimatrix';
         Lic = Lic = [Select Id, 
                        License_Features_Settings_Blob__c, 
                        LicenseStartDate__c, 
                        Client_Licenses_Blob__c, 
                        Type__c, 
                        CompanyName__c,
                        RegenerationLocationHostname__c,
                        RegenerationLocationPath__c,          
                        New_License_Server__c from System__c where Id =:id];
         User = [Select Id, FTP_Username__c, FTP_Password__c from User where id = :UserInfo.getUserId()];                
      
        xsNewLicenseServer = Lic.New_License_Server__c;
        if (xsNewLicenseServer)
        {
            stub.endpoint_x = 'https://vgen2.verimatrix.com/wscpp/clicgen';
        }  
         xsCompany = Lic.CompanyName__c;        
         xsType = Lic.Type__c;        
         xsServerData = Lic.License_Features_Settings_Blob__c;        
         xsClientData = Lic.Client_Licenses_Blob__c;        
         xsUsername = User.FTP_Username__c;
         xsPassword = User.FTP_Password__c;
         xsHostname = Lic.RegenerationLocationHostname__c;
         xsPath = Lic.RegenerationLocationPath__c;
         LicenseStartDate = Lic.LicenseStartDate__c;
       }                
    //executes the query using Database.getQueryLocator(query) and calls
    // the execute method with the result(List of allServers in this case)
         global Database.QueryLocator start(Database.BatchableContext BC)
    {
         query = 'Select Id, Name, Server_License_Key__c ,Success_Error_Code__c,License_Start_Date__c,Inactive__c  from Server__c where System__c =: id';
         return Database.getQueryLocator(query);
    }
   //executes once for each batch
    global void execute(Database.BatchableContext BC, List<Server__c> allServers)
    {
     for(Server__c server : allServers)
     {
      if (server.Inactive__c != true)   //Make sure Server is active               
      {           
         serverLicenseKey = server.Server_License_Key__c;                        
         serverName = server.Name;                        
         serverId = server.Id;
        if(!Test.isRunningTest()) output = stub.getLicense(xsCompany,xsType,serverLicenseKey,serverName,xsServerData,xsClientData,xsUsername,xsPassword,xsHostname,xsPath);
        else output =0 ;
         if (output == 0)   // Check if license was successfully generated
         {
           server.License_Start_Date__c = LicenseStartDate;
           countLicensesLastGeneratedOnSystem++;   //Set License Start Date on server with Start Date from System
         }
         else
         {
         li.add(server);
         count++;         
         }
        server.Success_Error_Code__c = output;
      } 
         else //Otherwise, if server is inactive, clear Success/Error Code
         {
           server.Success_Error_Code__c = null;  
         }
      }
        if(!Test.isRunningTest()) update allServers;
    }   

    global void finish(Database.BatchableContext BC)
    {    
 
      if (countLicensesLastGeneratedOnSystem >= 1 || Test.isRunningTest())  //if licenses were generated
            {
                Lic.Last_New_License_s_Generated_Date_Time__c = datetime.now();  //Set the Last License(s) Generated Date/Time                               
                Lic.Last_New_License_s_Generated_Count__c  = countLicensesLastGeneratedOnSystem;  //Set the Number of Licenses Last Generated
                Lic.hid_License_Blobs_Changed__c = false;   //Unmark License Blobs Changed boolean
                Lic.hid_Generated_with_vgen3__c = false;   // license not generated with vgen3
                System.debug('Lic :' + Lic);
                if(!Test.isRunningTest()) update Lic;
            }

    if(count >0)
    {
     RegenUntilSuccess r= new RegenUntilSuccess(id,li,0,countLicensesLastGeneratedOnSystem);
     Database.executeBatch(r,1);
    }
    }
    
    // Class method for generating license
    webservice static void licenseGet(String id) 
           {
        String tid=id;
        BatchLicenseRegeneratorOnSystem b = new BatchLicenseRegeneratorOnSystem(tid); 
        //Parameters of ExecuteBatch(context,BatchSize)
        Database.executebatch(b,1);   
    }
}
//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Rest endpoint for AWS to call when Software & License download Urls expire
@RestResource(urlMapping='/SLA/ExpireLink/*')
global class SLA_ExpireLinkEndpoints {

    global final static Integer STATUS_OK = 200;
    global final static Integer STATUS_BAD = 400;
    global final static Integer STATUS_UNAUTHORIZED = 401;
    global final static Integer STATUS_NOTFOUND = 404;
    global final static Integer STATUS_EXCEPTION = 500;


    global final static String STATUS_OK_MESSAGE = 'The request was successfully processed';
    global final static String STATUS_BAD_TYPE_MESSAGE = 'type param must either be: license or software';
    global final static String STATUS_UNAUTHORIZED_MESSAGE = 'Authorization Failed';
    global final static String STATUS_NOTFOUND_MESSAGE = 'The object Id you requested does not exist';
    global final static String STATUS_EXCEPTION_MESSAGE = 'An Internal Exception occured typing to process this request';

    global final static String OPPERATION_LICENSE = 'license';
    global final static String OPPERATION_SOFTWARE = 'software';

    @TestVisible private final static String AUTH_KEY = Label.SLA_AWSkey;

    //Rest endpoint to mark download URLS on AWS as expired
    //Params:
    //  type (string): the type of object we are marking as expired
    //      license
    //      software
    //  objectId: the object id that we are going to expire the link for
    @HttpPost  
    global static ExpireLinkReturn expireLink(String authKey, String type, String objectId) {
        ExpireLinkReturn ret = new ExpireLinkReturn();

        if(authKey != AUTH_KEY){
            ret.code = STATUS_UNAUTHORIZED;
            ret.message = STATUS_UNAUTHORIZED_MESSAGE;
            return ret;
        }

        if(type == OPPERATION_LICENSE){
            try{
                //Expire License_Request__c link
                expireLicenseRequestLink(objectId);
            }catch(ExpireLink404Exception e){
                ret.code = STATUS_NOTFOUND;
                ret.message = STATUS_NOTFOUND_MESSAGE;
            }catch(Exception e){
                ret.code = STATUS_EXCEPTION;
                ret.message = STATUS_EXCEPTION_MESSAGE;
            }

        }else if(type == OPPERATION_SOFTWARE){
            try{
                //Expire Software_Request__c link
                expireSoftwareRequestLink(objectId);
            }catch(ExpireLink404Exception e){
                ret.code = STATUS_NOTFOUND;
                ret.message = STATUS_NOTFOUND_MESSAGE;
            }catch(Exception e){
                ret.code = STATUS_EXCEPTION;
                ret.message = STATUS_EXCEPTION_MESSAGE;
            }
        }else{
            ret.code = STATUS_BAD;
            ret.message = STATUS_BAD_TYPE_MESSAGE;
        }

        if(ret.code == null){
            ret.code = STATUS_OK;
            ret.message = STATUS_OK_MESSAGE;
        }
        return ret;
    }

    private static void expireLicenseRequestLink(Id requestId){
        License_Request__c lr;
        try{
            lr = [SELECT Status__c FROM License_Request__c WHERE Id=:requestId LIMIT 1];
        }catch(Exception e){
            throw new ExpireLink404Exception('Request Not found');
        }
        if(lr.Status__c != SLA_LicenseRequestHelpers.STATUS_REPLACED){
            lr.status__c = SLA_LicenseRequestHelpers.STATUS_COMPLETE;
        }
        lr.Expired_On_Date__c = Datetime.now();
        update lr;
    }

    private static void expireSoftwareRequestLink(String requestId){
        Software_Request__c sr;
        try{
            sr = [SELECT id FROM Software_Request__c WHERE Reference_Id__c= :requestId LIMIT 1];
        }catch(Exception e){
            throw new ExpireLink404Exception('Request Not found');
        }    
        sr.status__c = SLA_DownloadRequestHelpers.STATUS_EXPIRED;
        sr.Expired_On_Date__c = Datetime.now();
        update sr;
    }

    global class ExpireLinkReturn{
        Webservice Integer code;
        WebService String message;
    }

    private class ExpireLink404Exception extends Exception{}

}
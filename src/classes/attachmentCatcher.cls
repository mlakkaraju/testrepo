/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email..
 */
global class attachmentCatcher implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope env)
    {
//        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
//        return result;

        //Create an inboundEmailResult object for returning the result of the Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.binaryAttachments;
        Messaging.InboundEmail.TextAttachment[]   tAttachments = email.textAttachments;
        String strEmailBodyText = '';
        String accountId = '';
        strEmailBodyText = email.plainTextBody;
  
        if (strEmailBodyText  != null && strEmailBodyText != '')
        {
            Integer startPos = strEmailBodyText.indexOf('Account Reference [DO NOT MODIFY]: ');
            if (startPos >=0)
            {
                startPos = startPos + 35;
                accountId = strEmailBodyText.substring(startPos,startPos + 15); 
            }
        }

        if (bAttachments != null && accountId != '')
        {
            for (Messaging.InboundEmail.BinaryAttachment mailAttachment : bAttachments)
            {
                String attachmentName   = mailAttachment.fileName;
                Blob   body             = mailAttachment.body;
                Attachment attachment   = new Attachment ();
                if(attachmentName == null || attachmentName == '' )
                {
                    attachmentName = 'noname';              
                }
                attachment.name = attachmentName;
                attachment.body = body;
                attachment.parentId = accountId;

                insert attachment;
            }
        }

        if (tAttachments != null)
        {
            for (Messaging.InboundEmail.TextAttachment mailAttachment : tAttachments)
            {
                String attachmentName   = mailAttachment.fileName;
                String strBody             = mailAttachment.body;
                Attachment attachment   = new Attachment ();
                if(attachmentName == null || attachmentName == '' )
                {
                    attachmentName = 'noname';              
                }
                attachment.name = attachmentName;
                attachment.body = Blob.valueOf(strBody);
                attachment.parentId = accountId;

                insert attachment;
            }
        }

        return result;

    }
}
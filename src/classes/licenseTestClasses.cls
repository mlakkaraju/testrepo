/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class licenseTestClasses {

    static testMethod void licTest() {
        License_Key__c Lic = [select Id, 
                              Company_Name__c, 
                              Server_License_Key__c, 
                              Server_Name__c, 
                              License_Features_Settings_Blob__c,
                              Location__c,
                              Location_Hostname__c,
                              Type__c, 
                              Client_Licenses_Blob__c from License_Key__c where id = 'a0O50000002FsXh'];
        
        User User = [select Id, 
                    FTP_Username__c, 
                    FTP_Password__c from User where id = :UserInfo.getUserId()];

        LicenseCertificateMasterClass.getCertificateResponse_element certResp = new LicenseCertificateMasterClass.getCertificateResponse_element();
        certResp.xiResult = 1;
        LicenseCertificateMasterClass.getCertificate_element certElem = new LicenseCertificateMasterClass.getCertificate_element();
        certElem.xsDepartment = 'Test';
        certElem.xsEmail = 'test@gmail.com';
        certElem.xsCity = 'Dallas';
        certElem.xsState = 'TX';
        certElem.xsCountry = 'USA';
        certElem.xsContact = 'Test Cont';
        certElem.xbGenerateSubCA = true;
        certElem.xbTextMode = true;
        certElem.xiValidity = 12;
        certElem.xsComponents = 'Test';        

        LicenseCertificateMasterClass.generateLicenseResponse_element licResp = new LicenseCertificateMasterClass.generateLicenseResponse_element();
        licResp.xiResult = 1;
        
        LicenseCertificateMasterClass.getLicense_element licElem = new LicenseCertificateMasterClass.getLicense_element(); 
        licElem.xsCompany = Lic.Company_Name__c;
        licElem.xsType = Lic.Type__c;
        licElem.xsHostKey = Lic.Server_License_Key__c;
        licElem.xsHostName = Lic.Server_Name__c;
        licElem.xsServerData = Lic.License_Features_Settings_Blob__c;
        licElem.xsClientData = Lic.Client_Licenses_Blob__c;
        licElem.xsUsername = User.FTP_Username__c;
        licElem.xsPassword = User.FTP_Password__c;
        licElem.xsHostname = Lic.Location_Hostname__c;
        licElem.xsPath = Lic.Location__c;

        LicenseCertificateMasterClass.getLicenseResponse_element licRespElem = new LicenseCertificateMasterClass.getLicenseResponse_element();
        licRespelem.xiResult = 1;
        
        LicenseCertificateMasterClass.generateCertificateResponse_element  genCertRespElem = new LicenseCertificateMasterClass.generateCertificateResponse_element();
        genCertRespElem.xiResult = 1;
        
        LicenseCertificateMasterClass.clicgen stub = new LicenseCertificateMasterClass.clicgen();      
        stub.clientCert_x = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA';
        stub.clientCertPasswd_x = 'verimatrix';
                            
        String xsCompany = Lic.Company_Name__c;
        String xsType = Lic.Type__c;
        String xsHostKey = Lic.Server_License_Key__c;
        String xsServerName = Lic.Server_Name__c;
        String xsServerData = Lic.License_Features_Settings_Blob__c;
        String xsClientData = Lic.Client_Licenses_Blob__c;
        String xsUsername = User.FTP_Username__c;
        String xsPassword = User.FTP_Password__c;
        String xsHostname = Lic.Location_Hostname__c;
        String xsPath = Lic.Location__c;
       
        
        System.debug('In License Generator');
        //public Integer generateLicense(String xsCompany,String xsType,String xsHostKey,String xsHostName,String xsServerData,String xsClientData,String xsLocation)
        //Integer output1 = stub.generateLicense (xsCompany,xsType,xsHostKey,xsHostName,xsServerData,xsClientData,xsLocation);    
                                       
        //Integer output1a = stub.getLicense(xsCompany, xsType, xsHostKey, xsHostName, xsServerData, xsClientData, xsLocation);
                                               
        String xsDepartment = 'Test';
        String xsEmail = 'test@gmail.com';
        String xsCity = 'Dallas';
        String xsState = 'TX';
        String xsCountry = 'USA';
        String xsContact = 'Test Cont';
        Boolean xbGenerateSubCA = true;
        Boolean xbTextMode = true;
        Integer xiValidity = 12;
        String xsComponents = 'Test';        
        //public Integer generateCertificate(String xsCompany,String xsDepartment,String xsEmail,String xsCity,String xsState,String xsCountry,String xsContact,Boolean xbGenerateSubCA,Boolean xbTextMode,Integer xiValidity,String xsComponents,String xsLocation)
        //Integer output2 = stub.generateCertificate(xsCompany, xsDepartment, xsEmail, xsCity, xsState, xsCountry, xsContact, xbGenerateSubCA, xbTextMode, xiValidity, xsComponents, xsLocation);
        //Integer output2a = stub.getCertificate(xsCompany, xsDepartment, xsEmail, xsCity, xsState, xsCountry, xsContact, xbGenerateSubCA, xbTextMode, xiValidity, xsComponents, xsLocation);

        licenseController licCont = new licenseController();
        Integer out0 = licenseController.licenseGenerator('a0O50000002FsXh');


                                                                                          
    }
    
    static testMethod void licRetrievalTest() {
        License_Key__c Lic = [select Id, 
                              Company_Name__c, 
                              Server_License_Key__c, 
                              Server_Name__c, 
                              License_Features_Settings_Blob__c,
                              Retrieval_Location_Path__c,
                              Retrieval_Location_Hostname__c,
                              Type__c, 
                              Client_Licenses_Blob__c from License_Key__c where id = 'a0O50000002FsXh'];
        
        User User = [select Id, 
                    FTP_Username__c, 
                    FTP_Password__c from User where id = :UserInfo.getUserId()];

        LicenseCertificateMasterClass.getCertificateResponse_element certResp = new LicenseCertificateMasterClass.getCertificateResponse_element();
        certResp.xiResult = 1;
        LicenseCertificateMasterClass.getCertificate_element certElem = new LicenseCertificateMasterClass.getCertificate_element();
        certElem.xsDepartment = 'Test';
        certElem.xsEmail = 'test@gmail.com';
        certElem.xsCity = 'Dallas';
        certElem.xsState = 'TX';
        certElem.xsCountry = 'USA';
        certElem.xsContact = 'Test Cont';
        certElem.xbGenerateSubCA = true;
        certElem.xbTextMode = true;
        certElem.xiValidity = 12;
        certElem.xsComponents = 'Test';        

        LicenseCertificateMasterClass.generateLicenseResponse_element licResp = new LicenseCertificateMasterClass.generateLicenseResponse_element();
        licResp.xiResult = 1;
        
        LicenseCertificateMasterClass.getLicense_element licElem = new LicenseCertificateMasterClass.getLicense_element(); 
        licElem.xsCompany = Lic.Company_Name__c;
        licElem.xsType = Lic.Type__c;
        licElem.xsHostKey = Lic.Server_License_Key__c;
        licElem.xsHostName = Lic.Server_Name__c;
        licElem.xsServerData = Lic.License_Features_Settings_Blob__c;
        licElem.xsClientData = Lic.Client_Licenses_Blob__c;
        licElem.xsUsername = User.FTP_Username__c;
        licElem.xsPassword = User.FTP_Password__c;
        licElem.xsHostname = Lic.Retrieval_Location_Hostname__c;
        licElem.xsPath = Lic.Retrieval_Location_Path__c;

        LicenseCertificateMasterClass.getLicenseResponse_element licRespElem = new LicenseCertificateMasterClass.getLicenseResponse_element();
        licRespelem.xiResult = 1;
        
        LicenseCertificateMasterClass.generateCertificateResponse_element  genCertRespElem = new LicenseCertificateMasterClass.generateCertificateResponse_element();
        genCertRespElem.xiResult = 1;
        
        LicenseCertificateMasterClass.clicgen stub = new LicenseCertificateMasterClass.clicgen();      
        stub.clientCert_x = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA';
        stub.clientCertPasswd_x = 'verimatrix';
                            
        String xsCompany = Lic.Company_Name__c;
        String xsType = Lic.Type__c;
        String xsHostKey = Lic.Server_License_Key__c;
        String xsServerName = Lic.Server_Name__c;
        String xsServerData = Lic.License_Features_Settings_Blob__c;
        String xsClientData = Lic.Client_Licenses_Blob__c;
        String xsUsername = User.FTP_Username__c;
        String xsPassword = User.FTP_Password__c;
        String xsHostname = Lic.Retrieval_Location_Hostname__c;
        String xsPath = Lic.Retrieval_Location_Path__c;
       
        
        System.debug('In License Generator');
        //public Integer generateLicense(String xsCompany,String xsType,String xsHostKey,String xsHostName,String xsServerData,String xsClientData,String xsLocation)
        //Integer output1 = stub.generateLicense (xsCompany,xsType,xsHostKey,xsHostName,xsServerData,xsClientData,xsLocation);    
                                       
        //Integer output1a = stub.getLicense(xsCompany, xsType, xsHostKey, xsHostName, xsServerData, xsClientData, xsLocation);
                                               
        String xsDepartment = 'Test';
        String xsEmail = 'test@gmail.com';
        String xsCity = 'Dallas';
        String xsState = 'TX';
        String xsCountry = 'USA';
        String xsContact = 'Test Cont';
        Boolean xbGenerateSubCA = true;
        Boolean xbTextMode = true;
        Integer xiValidity = 12;
        String xsComponents = 'Test';        
        //public Integer generateCertificate(String xsCompany,String xsDepartment,String xsEmail,String xsCity,String xsState,String xsCountry,String xsContact,Boolean xbGenerateSubCA,Boolean xbTextMode,Integer xiValidity,String xsComponents,String xsLocation)
        //Integer output2 = stub.generateCertificate(xsCompany, xsDepartment, xsEmail, xsCity, xsState, xsCountry, xsContact, xbGenerateSubCA, xbTextMode, xiValidity, xsComponents, xsLocation);
        //Integer output2a = stub.getCertificate(xsCompany, xsDepartment, xsEmail, xsCity, xsState, xsCountry, xsContact, xbGenerateSubCA, xbTextMode, xiValidity, xsComponents, xsLocation);

        
        licenseRetriever licenseRet = new licenseRetriever();
        Integer out0c = licenseRetriever.licenseGet('a0O50000002FsXh');

                                                                                          
    }
    
    static testMethod void certTest() {
        Certificate__c Cert = [select Id, 
                              Company_Name__c, 
                              Contact_Department__c, 
                              Contact_Email__c, 
                              City__c, 
                              State__c, 
                              Country__c, 
                              Contact_Name_TEXT__c, 
                              Generate_Sub_CA__c, 
                              Keep_HumanReadable_Text__c,
                              Validity__c,
                              Server_Components_Blob__c,
                              Location__c,
                              Location_Hostname__c from Certificate__c where id = 'a0Q50000001SYaj'];
                              
        User User = [select Id, 
                    FTP_Username__c, 
                    FTP_Password__c from User where id = :UserInfo.getUserId()];

        LicenseCertificateMasterClass.getCertificateResponse_element certResp = new LicenseCertificateMasterClass.getCertificateResponse_element();
        certResp.xiResult = 1;
        LicenseCertificateMasterClass.getCertificate_element certElem = new LicenseCertificateMasterClass.getCertificate_element();
        certElem.xsDepartment = 'Test';
        certElem.xsEmail = 'test@gmail.com';
        certElem.xsCity = 'Dallas';
        certElem.xsState = 'TX';
        certElem.xsCountry = 'USA';
        certElem.xsContact = 'Test Cont';
        certElem.xbGenerateSubCA = true;
        certElem.xbTextMode = true;
        certElem.xiValidity = 12;
        certElem.xsComponents = 'Test';        

        LicenseCertificateMasterClass.generateLicenseResponse_element licResp = new LicenseCertificateMasterClass.generateLicenseResponse_element();
        licResp.xiResult = 1;
        
        //LicenseCertificateMasterClass.getLicense_element licElem = new LicenseCertificateMasterClass.getLicense_element(); 
        //licElem.xsCompany = Lic.Company_Name__c;
        //licElem.xsType = Lic.Type__c;
        //licElem.xsHostKey = Lic.Server_License_Key__c;
        //licElem.xsHostName = Lic.Server_Name__c;
        //licElem.xsServerData = Lic.License_Features_Settings_Blob__c;
        //licElem.xsClientData = Lic.Client_Licenses_Blob__c;
        //licElem.xsLocation = Lic.Location__c;

        LicenseCertificateMasterClass.getLicenseResponse_element licRespElem = new LicenseCertificateMasterClass.getLicenseResponse_element();
        licRespelem.xiResult = 1;
        
        LicenseCertificateMasterClass.generateCertificateResponse_element  genCertRespElem = new LicenseCertificateMasterClass.generateCertificateResponse_element();
        genCertRespElem.xiResult = 1;
        
        LicenseCertificateMasterClass.clicgen stub = new LicenseCertificateMasterClass.clicgen();      
        stub.clientCert_x = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA';
        stub.clientCertPasswd_x = 'verimatrix';
                            
        //String xsCompany = Lic.Company_Name__c;
        //String xsType = Lic.Type__c;
        //String xsHostKey = Lic.Server_License_Key__c;
        //String xsHostName = Lic.Server_Name__c;
        //String xsServerData = Lic.License_Features_Settings_Blob__c;
        //String xsClientData = Lic.Client_Licenses_Blob__c;
        //String xsLocation = Lic.Location__c;
       
        
        System.debug('In License Generator');
        //public Integer generateLicense(String xsCompany,String xsType,String xsHostKey,String xsHostName,String xsServerData,String xsClientData,String xsLocation)
        //Integer output1 = stub.generateLicense (xsCompany,xsType,xsHostKey,xsHostName,xsServerData,xsClientData,xsLocation);    
                                       
        //Integer output1a = stub.getLicense(xsCompany, xsType, xsHostKey, xsHostName, xsServerData, xsClientData, xsLocation);
                                               
        String xsDepartment = 'Test';
        String xsEmail = 'test@gmail.com';
        String xsCity = 'Dallas';
        String xsState = 'TX';
        String xsCountry = 'USA';
        String xsContact = 'Test Cont';
        Boolean xbGenerateSubCA = true;
        Boolean xbTextMode = true;
        Integer xiValidity = 12;
        String xsComponents = 'Test';
        
                
        certificateController certCont = new certificateController();
        Integer out0a = certificateController.certificateGenerator('a0Q50000001SYaj');

        
        }
        
        static testMethod void certRetrievalTest() {
        Certificate__c Cert = [select Id, 
                              Company_Name__c, 
                              Contact_Department__c, 
                              Contact_Email__c, 
                              City__c, 
                              State__c, 
                              Country__c, 
                              Contact_Name_TEXT__c, 
                              Generate_Sub_CA__c, 
                              Keep_HumanReadable_Text__c,
                              Validity__c,
                              Server_Components_Blob__c,
                              Retrieval_Location_Path__c,
                              Retrieval_Location_Hostname__c from Certificate__c where id = 'a0Q50000001SYaj'];
                              
        User User = [select Id, 
                    FTP_Username__c, 
                    FTP_Password__c from User where id = :UserInfo.getUserId()];

        LicenseCertificateMasterClass.getCertificateResponse_element certResp = new LicenseCertificateMasterClass.getCertificateResponse_element();
        certResp.xiResult = 1;
        LicenseCertificateMasterClass.getCertificate_element certElem = new LicenseCertificateMasterClass.getCertificate_element();
        certElem.xsDepartment = 'Test';
        certElem.xsEmail = 'test@gmail.com';
        certElem.xsCity = 'Dallas';
        certElem.xsState = 'TX';
        certElem.xsCountry = 'USA';
        certElem.xsContact = 'Test Cont';
        certElem.xbGenerateSubCA = true;
        certElem.xbTextMode = true;
        certElem.xiValidity = 12;
        certElem.xsComponents = 'Test';        

        LicenseCertificateMasterClass.generateLicenseResponse_element licResp = new LicenseCertificateMasterClass.generateLicenseResponse_element();
        licResp.xiResult = 1;
        
        //LicenseCertificateMasterClass.getLicense_element licElem = new LicenseCertificateMasterClass.getLicense_element(); 
        //licElem.xsCompany = Lic.Company_Name__c;
        //licElem.xsType = Lic.Type__c;
        //licElem.xsHostKey = Lic.Server_License_Key__c;
        //licElem.xsHostName = Lic.Server_Name__c;
        //licElem.xsServerData = Lic.License_Features_Settings_Blob__c;
        //licElem.xsClientData = Lic.Client_Licenses_Blob__c;
        //licElem.xsLocation = Lic.Location__c;

        LicenseCertificateMasterClass.getLicenseResponse_element licRespElem = new LicenseCertificateMasterClass.getLicenseResponse_element();
        licRespelem.xiResult = 1;
        
        LicenseCertificateMasterClass.generateCertificateResponse_element  genCertRespElem = new LicenseCertificateMasterClass.generateCertificateResponse_element();
        genCertRespElem.xiResult = 1;
        
        LicenseCertificateMasterClass.clicgen stub = new LicenseCertificateMasterClass.clicgen();      
        stub.clientCert_x = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA';
        stub.clientCertPasswd_x = 'verimatrix';
                            
        //String xsCompany = Lic.Company_Name__c;
        //String xsType = Lic.Type__c;
        //String xsHostKey = Lic.Server_License_Key__c;
        //String xsHostName = Lic.Server_Name__c;
        //String xsServerData = Lic.License_Features_Settings_Blob__c;
        //String xsClientData = Lic.Client_Licenses_Blob__c;
        //String xsLocation = Lic.Location__c;
       
        
        System.debug('In License Generator');
        //public Integer generateLicense(String xsCompany,String xsType,String xsHostKey,String xsHostName,String xsServerData,String xsClientData,String xsLocation)
        //Integer output1 = stub.generateLicense (xsCompany,xsType,xsHostKey,xsHostName,xsServerData,xsClientData,xsLocation);    
                                       
        //Integer output1a = stub.getLicense(xsCompany, xsType, xsHostKey, xsHostName, xsServerData, xsClientData, xsLocation);
                                               
        String xsDepartment = 'Test';
        String xsEmail = 'test@gmail.com';
        String xsCity = 'Dallas';
        String xsState = 'TX';
        String xsCountry = 'USA';
        String xsContact = 'Test Cont';
        Boolean xbGenerateSubCA = true;
        Boolean xbTextMode = true;
        Integer xiValidity = 12;
        String xsComponents = 'Test';

        
        certificateRetriever certRet = new certificateRetriever();
        Integer out0b = certificateRetriever.certificateGet('a0Q50000001SYaj');
        
        }
}
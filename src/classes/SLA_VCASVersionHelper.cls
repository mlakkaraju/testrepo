//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Helpers for VCAS Versions

public without sharing class SLA_VCASVersionHelper {
	public static final String STAGE_BETA = 'Beta';
	public static final String STAGE_GA = 'GA';
	public static final String STAGE_EOL = 'EOL';	
	
	public static final VersionNumber VERSION_3_6_1 = new  SLA_VCASVersionHelper.VersionNumber('3.6.1');
	public static final VersionNumber VERSION_3_4 = new  SLA_VCASVersionHelper.VersionNumber('3.4');

	public static Beta_Access_Association__c generateBetaAccess(Id accountId, Id versionId){
		Beta_Access_Association__c newAssoc = new Beta_Access_Association__c();
        newAssoc.Account__c = accountId;
        newAssoc.VCAS_Version__c = versionId;
        return newAssoc;
	}

	//extract the version number out to a number version that can be used for comparisions
	//only gets the most significiate minor verison digit
	//example: 3.14.159 would return 3.1
	public static Decimal getVersionNumber(Id versionId){
		system.debug(versionId);
		VCAS_ViewRight_Version__c version = [SELECT Version_Number__c FROM VCAS_ViewRight_Version__c WHERE Id = :versionId];
		return extractVersionNumber(version.Version_Number__c);
	}

	private static Decimal extractVersionNumber(String versionString){
		Pattern p = Pattern.compile('^(\\d+\\.\\d).*');
		Matcher pm = p.matcher(versionString);

		if(pm.matches()){
			return Decimal.valueOf(pm.group(1));
		}else{
			return -1;
		}
	}

	public static List<VCAS_ViewRight_Version__c> sortVersions(List<VCAS_ViewRight_Version__c> versions){
		List<VCASVersionCompareWrapper> wrappers = new List<VCASVersionCompareWrapper>();
		for(VCAS_ViewRight_Version__c v : versions){
			wrappers.add(new VCASVersionCompareWrapper(v));
		}
		wrappers.sort();
		List<VCAS_ViewRight_Version__c> retVersions = new List<VCAS_ViewRight_Version__c>();
		for(VCASVersionCompareWrapper w : wrappers){
			retVersions.add(w.versionObj);
		}
		return retVersions;
	}

	public class VersionNumber implements Comparable {
		public List<Integer> parts {get; set;}
		public String versionString {get; set;}
		public VersionNumber(String version){
			this.versionString = version;
			parts = new List<Integer>();
			if(String.isEmpty(version)){
				return;
			}

			system.debug(version);
			for(String p : version.split('\\.')){
				String cleanedPart = p.replaceAll('[^0-9]', '');
				if(cleanedPart.length() > 0){
					parts.add(Integer.valueOf(cleanedPart));
				}else{
					parts.add(-1);
				}
			}
			system.debug(parts);
		}

		public Integer compareTo(Object compareTo) {
    		VersionNumber compareToVersion = (VersionNumber) compareTo;
	    	Integer i = 0;
	    	while(i < this.parts.size() || i < compareToVersion.parts.size()){
	    		if(i < this.parts.size() && i < compareToVersion.parts.size()){
	    			if(this.parts[i] < compareToVersion.parts[i]){
	    				return -1;
	    			}else if(this.parts[i] > compareToVersion.parts[i]){
	    				return 1;
	    			}else{
	    				i++;
	    			}
	    		}else if(i < this.parts.size()){
	    			return 1;
	    		}else{
	    			return -1;
	    		}
	    	}
	    	return 0;
		}
	}

	public class VCASVersionCompareWrapper implements Comparable{
		public VCAS_ViewRight_Version__c versionObj {get; set;}
		public VersionNumber versionNumber {get; set;}
		public VCASVersionCompareWrapper(VCAS_ViewRight_Version__c version){
			this.versionObj = version;
			versionNumber = new VersionNumber(version.Version_Number__c);
		}

		public Integer compareTo(Object compareTo) {
			VCASVersionCompareWrapper compareWrapper = (VCASVersionCompareWrapper) compareTo;
			return -1*this.versionNumber.compareTo(compareWrapper.versionNumber);
		}
	}
}
//Author: Charlie@callawaycloudconsulting.com
// Trigger helper used to set certificate.hid_Added_Server__c when new servers are added to system
public without sharing class SLA_ServerAddedTriggerHandler implements TriggerHandler.HandlerInterface {

	public void handle(){
		Set<Id> systemIds = new Set<Id>();
		for(Server__c server : (List<Server__c>) Trigger.new){
			systemIds.add(server.System__c);
		}
		List<System__c> systems = [SELECT Certificate__c FROM System__c WHERE ID IN : systemIds];
		Set<Id> certIds = new Set<Id>();
		for(System__c sys : systems){
			certIds.add(sys.Certificate__c);
		}
		List<Certificate__c> certs = [SELECT Id FROM Certificate__c WHERE Id IN : certIds];
		for(Certificate__c cert : certs){
			cert.hid_Added_Server__c = true;
		}
		update certs;
	}
}
/***************************************************************************
* Name: Project Delivery Handler
* Author:don Cassis
* Date: 5/6/2015
* Description: This class is used to Handle the logic related to the Project 
*                Delivery records when a transaction is completed on them.
*
****************************************************************************
*                                Change Log
****************************************************************************
* Date                  Author                  Description
*
* 5/6/2015             Brandon Cassis           Created
* 
***************************************************************************/
public class ProjectDeliveryHandler {

    //This method is used to copy values from the associated Product.
    public static void setFieldValues(List<Project_Deliverable__c> inputList) {
        Set<Id> productIdSet = new Set<Id>();
        Map<Id, Product2> productMap; 
        
        for(Project_Deliverable__c loopPD: inputList) {
            productIdSet.add(loopPD.Product__c);
        }
        
        try{
            productMap = new Map<Id, Product2>([Select Id, Family, Product_Subfamily__c from Product2 where Id IN :productIdSet]);
        } catch(Exception e) {
        
        }
        
        for(Project_Deliverable__c loopPD: inputList) {
            if(productMap.containsKey(loopPD.Product__c)) {
                if(productMap.get(loopPD.Product__c).Family != null) {
                    loopPD.Product_Family__c =  productMap.get(loopPD.Product__c).Family;
                }
                
                if(productMap.get(loopPD.Product__c).Product_Subfamily__c != null) {
                    loopPD.Product_Subfamily__c  =  productMap.get(loopPD.Product__c).Product_Subfamily__c ;
                }
            }
        }
    }
}
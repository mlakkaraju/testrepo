//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//    Allows SOP to send a project to a customer
public with sharing class SLA_DeliverProjectController {

    public static final String MSG_NO_USERS = 'No Users to Deliver this System to.  Please add Customers Contacts to Project Or Create Users';
    public static final String MSG_DELIVER_SUCCESS = 'The Project has been successfully delivered to the customers';
    public static final String MSG_NO_SELF_SERVICE = 'You must mark "Enable Verimatrix Self-Service Licensing" checkbox on the "Account" or "Ship To" before you can deliver this project!';

    private final Tenrox__c project;

    public User primaryCustomerUser {get; set;}
    public Boolean emailPrimaryCustomer {get; set;}
    {emailPrimaryCustomer = false;}
    
    public User secondaryCustomerUser {get; set;}
    public Boolean emailSecondaryCustomer {get; set;}
    {emailSecondaryCustomer = false;}

    public List<License_Request__c> licenseRequests {get; set;}

    public Boolean canDeliver {get; set;}
    //Forecast_Revenue_Date__c
    public Project_Revenue_Schedule__c milestone {get; set;}

    public Boolean initialProjectLicenseState {get; set;}
    public Boolean accountLicenseState {get; set;}
    public String systemComments {get; set;}

    public Id origionalSystem {get; set;}

    public SLA_DeliverProjectController(ApexPages.StandardController stdController) {

        this.project = (Tenrox__c)stdController.getRecord();
        
        origionalSystem = project.System__c;

        if(project.Max_Number_of_Servers__c == null){
            project.Max_Number_of_Servers__c = 50;
        }

        if(this.project.Days_License_Valid__c == null){
           this.project.Days_License_Valid__c = 90;
        }

        initialProjectLicenseState = project.Approved_for_Permanent_Licenses__c;
        if(project.Account__c!=null){
            accountLicenseState = [SELECT Always_give_Permanent_Licenses__c FROM Account WHERE Id = :project.Account__c].Always_give_Permanent_Licenses__c;
        }else{
            accountLicenseState = false;
        }

        //get system
        try{
            //get license requests
            if(project.System__c != null){
                licenseRequests = SLA_LicenseRequestHelpers.findLicenseRequestBySystem(project.System__c);
            }else{
                licenseRequests = new List<License_Request__c>();
            }
        }catch(Exception e){System.debug(e.getMessage());}

        try{
            primaryCustomerUser = [SELECT 
            Id,
            Name,
            Email 
            FROM User 
            WHERE ContactId =: project.Primary_Customer_Contact__c 
            AND ContactId != null 
            LIMIT 1];
            emailPrimaryCustomer = true;
        }
        catch(Exception e){}

        try{
            secondaryCustomerUser = [SELECT 
            Id, 
            Name,
            Email
            FROM User 
            WHERE ContactId =: project.Secondary_Customer_Contact__c 
            AND ContactId != null 
            LIMIT 1];
            emailSecondaryCustomer = true;
        }catch(Exception e){}

        try{
            milestone = [SELECT Forecast_Revenue_Date__c 
                            FROM Project_Revenue_Schedule__c 
                            WHERE Project__c = :project.Id 
                            ORDER BY CreatedDate ASC
                            LIMIT 1];
        }catch(Exception e){}

        if(!allowSelfService()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, MSG_NO_SELF_SERVICE));
        }

    }

    //validate, save, and send email
    public PageReference deliverSystem(){

        if(!allowSelfService()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MSG_NO_SELF_SERVICE));
            return null;
        }

        if(!emailPrimaryCustomer && !emailSecondaryCustomer){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, MSG_NO_USERS));
            return null;
        }

        if(getShowSystemComments()){
            System__c sys = [SELECT ID FROM System__c WHERE Id = :project.System__c];
            sys.System_Health_Detail__c = systemComments;
            update sys;
        }

        try{
            if(milestone != null){
                update milestone;
            } 
            //only mark as executing if project is not complete or closed
            if(project.Project_Status_CC__c != SLA_ProjectHelpers.STATUS_COMPLETE 
                && project.Project_Status_CC__c != SLA_ProjectHelpers.STATUS_CLOSED){
                project.Project_Status_CC__c = SLA_ProjectHelpers.STATUS_EXECUTING;
            }
            //always clear substatus
            project.Project_Substatus__c = null;

            //if at step 10, move back to step 9
            if(project.Customer_License_Wizard_Step__c == SLA_Communities_Helpers.getUrlBase(Page.SLA_LicenseRequestWizard_Final.getUrl())){
                project.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Review.getUrl();
            }

            //set access field = true
            project.Delivered_to_Customer__c = true;
            update project;
        }catch(Exception e){
            System.Debug('Error: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }

        //send email
        sendCustomerEmail();

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, MSG_DELIVER_SUCCESS));
        return null;
    }

    //creates a customer login
    public PageReference createCustomerLogin(){

        try{
            Id contactId = (Id) ApexPages.currentPage().getParameters().get('contactId');
            User newUser = SLA_CommunityUserFactory.CreateUserFromContact(contactId,false);
            if(contactId == project.Primary_Customer_Contact__c){
                primaryCustomerUser = newUser;
            }
            else if(contactId == project.Secondary_Customer_Contact__c){
                secondaryCustomerUser = newUser;
            }
        }
        catch(Exception e){
            System.Debug('Error: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Could Not create Community User License! Error: ' + e.getMessage()));
        }
        return null;
    }

    //creates a partner login
    public PageReference createPartnerLogin(){
        try{
            Id contactId = (Id) ApexPages.currentPage().getParameters().get('contactId');
            User newUser = SLA_CommunityUserFactory.CreateUserFromContact(contactId,true);
            if(contactId == project.Primary_Customer_Contact__c){
                primaryCustomerUser = newUser;
                emailPrimaryCustomer = true;
            }
            else if(contactId == project.Secondary_Customer_Contact__c){
                secondaryCustomerUser = newUser;
                emailSecondaryCustomer = true;
            }
        }
        catch(Exception e){
            System.Debug('Error: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Could Not create Community User License! Error: ' + e.getMessage()));
        }
        return null;
    }

    //used to render system comments field
    public Boolean getShowSystemComments(){
        return (project.System__c != null && initialProjectLicenseState==true && project.Approved_for_Permanent_Licenses__c==false && accountLicenseState == true);    
    }

    //determines if this project is eligible for self service portal
    public Boolean allowSelfService(){
        Boolean canDeliver = false;
        system.debug(project);
        if(project.Ship_To__c != null){
            return project.Ship_To__r.Enable_Verimatrix_Self_Service_Licensing__c;
        }
        if(project.Account__c != null){
            return project.Account__r.Enable_Verimatrix_Self_Service_Licensing__c;
        }
        return false;
    }

    private void sendCustomerEmail(){
        List<Id> contactIds = new List<Id>();        
        if(emailPrimaryCustomer){
            contactIds.add(project.Primary_Customer_Contact__c);
        }

        if(emailSecondaryCustomer){
            contactIds.add(project.Secondary_Customer_Contact__c);
        }

        SLA_Communities_Helpers.sendCommunityEmail(contactIds, Label.SLA_DeliverProjectEmailTemplate, project.Id);
    }


    public class DeliveryException extends Exception {}

}
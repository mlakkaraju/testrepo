//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Controller for Customer Software Access Page.  Shows avalible software and redirects to download

public without sharing class SLA_SoftwareAccessController {

    public List<VCAS_ViewRight_Version__c> versions {get; private set;}
    
    public SLA_SoftwareAccessController() {
        Id userId = UserInfo.getUserId(); 
        User u = [SELECT contactId FROM User WHERE id=:userId];
        SLA_SoftwareAccessResolver resolver = new SLA_SoftwareAccessResolver(u);
        versions = SLA_VCASVersionHelper.sortVersions(resolver.getSoftware());
    }

    public PageReference downloadVersion() {
        Id version = (Id) ApexPages.currentPage().getParameters().get('versionId');
        System.Debug('versionId: ' + version);
        PageReference pageRef = Page.SLA_SoftwareDownload;
        pageRef.getParameters().put('id', version);
        return pageRef;
    }
}
//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  CONTROLLER TO ALLOW INTERNAL LICENSE GENERATION
public with sharing class SLA_InternalGenerateLicensesController {
  
  private final System__c sys;

    //temporaly license request to bind to UI
  public License_Request__c lr {get; private set;}
  public Boolean submitted {get; private set;}
    public Certificate__c cert {get; private set;}

    public List<SelectOption> systemProjects {get; set;}

  public SLA_InternalGenerateLicensesController(ApexPages.StandardController stdController) {
        this.sys = (System__c)stdController.getRecord();
        lr = new License_Request__c();
        lr.System__c = sys.id;
        systemProjects = new List<SelectOption>();
        for(Tenrox__c project : [SELECT Id, Name FROM Tenrox__c 
                                    WHERE System__c = :sys.Id OR (System__c = null 
                                    AND (Account__c = :sys.Account__c OR Ship_To__c = :sys.Account__c))]){
            SelectOption opt = new SelectOption(project.Id,project.name);
            systemProjects.add(opt);
        }

        submitted = false;
  }

  public PageReference backToSystem(){
         return new PageReference('/' + sys.Id);
    }

    //automaticly set contact 1 & 2 to the projects primary and secondary
    public void populateContacts(){
        Tenrox__c pject = [SELECT Primary_Customer_Contact__c, Secondary_Customer_Contact__c FROM Tenrox__c WHERE ID = :lr.project__c];
        lr.Contact_1__c = pject.Primary_Customer_Contact__c;
        lr.Contact_2__c = pject.Secondary_Customer_Contact__c;
    }

    //fire off license request
    public PageReference generate(){
        //copy the details of the LR we've been working in the page to the new LR
      License_Request__c lrNew = SLA_LicenseRequestHelpers.openNewRequest(sys.Id);
      lrNew.Project__c = lr.project__c;
      

      lrNew.Contact_1__c = lr.Contact_1__c;
      lrNew.Contact_2__c = lr.Contact_2__c;
      lrNew.Contact_3__c = lr.Contact_3__c;
        lrNew.Contact_4__c = lr.Contact_4__c;
        lrNew.Contact_5__c = lr.Contact_5__c;
       Tenrox__c project = [SELECT Id FROM Tenrox__c WHERE Id = :lr.project__c LIMIT 1];
        project.System__c = sys.id;
        project.License_Counts_Set_on_System__c = true;
        update project;
    try{
   update lrNew;                
    }catch(Exception e){
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    return null;
    } 
    SLA_LicenseRequestHelpers.startLicenseGeneration(lrNew);
      submitted = true;
      lr = lrNew;
        update sys.Certificate__r;
      return null;
    }

}
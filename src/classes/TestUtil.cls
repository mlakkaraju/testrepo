/*
     Created By Ralph Callaway
     Description:
       Place all data creation utility functions in this class so they can be shared
       across multiple tests.  That way if new validation rules break a test you only
       have to make a change in one place
       
       Modifiled by: Anil Sahoo
       
        test class modified to to accommodate the new validation rules.

       Modified by: Charles Jonas

         added generate methods for SLA related objects
*/
@isTest
public class TestUtil {

    public static String TEST_STRING = generateRandomString();
    public static String RESELLER_NAME = generateRandomString();
    public static Date TEST_DATE = System.today();

    /* Standard Object Functions */

    // Accounts
    public static Account createAccount() {
        Account account = generateAccount();
        insert account;
        return account;
    }
    public static Account generateAccount() {
        return new Account(name = TEST_STRING, billingCountry = 'Qatar', shippingCountry = 'Qatar'); 
        
    }

    // Reseller
    public static Account getReseller() {
        Account reseller;
        try{
            reseller = [Select Id, name from Account where type = 'Partner' limit 1];
        }catch(Exception e){
            reseller = new Account(name = RESELLER_NAME, billingCountry = 'Qatar', shippingCountry = 'Qatar', type='Partner');
            insert reseller;
        }
        return reseller;
    }
    
    
    // Case
    public static Case createCase() {
        Case testCase = generateCase();
        insert testCase;
        return testCase;
    }
    public static Case generateCase() {
        return new Case(subject = TEST_STRING, description = TEST_STRING);
    }
    
    // Contacts
    public static Contact createContact(Id accountId) {
        Contact contact = generateContact(accountId);
        insert contact;
        return contact;
    }
    public static Contact generateContact(Id accountId) {
        return new Contact(lastName = TEST_STRING, mailingCountry = 'Qatar', accountId = accountId);
    }
    
    // Leads
    public static Lead createLead() {
        Lead lead = generateLead();
        insert lead;
        return lead;
    }
    public static Lead generateLead() {
        return new Lead(lastName = TEST_STRING, company = TEST_STRING, country= TEST_STRING);
    }
     
    // Opportunity
    public static Opportunity createOpportunity(Id accountId) {
        Opportunity opportunity = generateOpportunity(accountId);
        insert opportunity;
        return opportunity;
    }
    public static Opportunity generateOpportunity(Id accountId) {
        
        Account account =[Select Id, name, billingCountry from Account limit 1];
        Account reseller = getReseller();
        //Account account = [Select Id, name from Account where id = :accountId];
        return new Opportunity(
              closeDate = TEST_DATE,
              name = account.name,
              reseller_name__c = reseller.name,
              // reseller_name__c = TEST_STRING,
              stageName = TEST_STRING,
              Delivery_Country__c = account.billingCountry,
              accountId = accountId,
              Reseller__c = reseller.Id,
              //Reseller__c = testAcc.Id,
              PMO_Required__c = 'YES',
              PMO_Required_Reason__c = 'Other'
        );
    }

    // User
    public static User createUser() {
        User testUser = generateUser();
        insert testUser;
        return testUser;
    }
    public static User generateUser() {
        String testString = generateRandomString(8);
        String testEmail = generateRandomEmail();
        return new User(lastName = testString,
            userName = testEmail,
            profileId = SYSADMIN_PROFILE_ID,
            alias = testString,
            email = testEmail,
            emailEncodingKey = 'ISO-8859-1',
            languageLocaleKey = 'en_US',
            localeSidKey = 'en_US',
            timeZoneSidKey = 'America/Los_Angeles'
        );
    }
    
    /* Custom Objects */
    
    // M&S Milestone
    public static M_S_Milestone__c createMandSMilestone(Id accountId) {
        M_S_Milestone__c milestone = generateMandSMilestone(accountId);
        insert milestone;
        return milestone;
    }
    public static M_S_Milestone__c generateMandSMilestone(Id accountId) {
        return new M_S_Milestone__c(
              account__c = accountId
            , start_date__c = TEST_DATE
            , po_number__c = TEST_STRING
            , customer__c = TEST_STRING
        );
    }

    
    // Project

     // Project
    public static tenrox__c createProject() {
        tenrox__c project = generateProject();
        insert project;
        return project;
    }

    public static tenrox__c createProject(Id accountId, Id entitlementId) {
        tenrox__c project = generateProject(accountId, entitlementId);
        insert project;
        return project;
    }
    public static tenrox__c generateProject() {
        return new tenrox__c(project_owner__c = TEST_ADMIN_ID);
    }
    public static tenrox__c generateProject(Id accountId, Id entitlementId) {
        return new tenrox__c(Account__c = accountId,
                            Entitlement__c = entitlementId,
                            project_owner__c = TEST_ADMIN_ID);
    }

    //System
    public static System__c createSystem(){
        Account account = createAccount();
        Contact contact = createContact(account.id);
        return createSystem(account.Id,contact.Id);
    }

    public static System__c createSystem(Id accountId, Id contactId){
        String companyName = generateRandomString();
        VCAS_ViewRight_Version__c vcasVerison = createVCASVersion();
        Certificate__c certificate = createCertificate(accountId,contactId, companyName);
        Site_License__c siteLicense = createSiteLicense(accountId);
        return createSystem(accountId,vcasVerison.Id,certificate.Id,siteLicense.Id, companyName);
    }


    public static System__c createSystem(Id accountId, Id vcasVerisonId,
     Id certificateId, Id siteLicenseId){
        System__c sys = generateSystem(accountId,vcasVerisonId,certificateId,siteLicenseId);
        insert sys;
        return sys;
    }

    public static System__c createSystem(Id accountId, Id vcasVerisonId,
     Id certificateId, Id siteLicenseId, String companyName){
        System__c sys = generateSystem(accountId,vcasVerisonId,certificateId,siteLicenseId,companyName);
        insert sys;
        return sys;
    }

    public static System__c generateSystem(Id accountId, Id vcasVerisonId,
     Id certificateId, Id siteLicenseId){
        return generateSystem(accountId,vcasVerisonId,certificateId,siteLicenseId, generateRandomString());
    }

    public static System__c generateSystem(Id accountId, Id vcasVerisonId,
     Id certificateId, Id siteLicenseId, String name){
        return new System__c(
            Name = name,
            CompanyName__c = name, 
            Account__c = accountId,
            Certificate__c = certificateId,
            VCAS_Version__c = vcasVerisonId,
            Site_License__c = siteLicenseId
        );
    }

    //SERVER
     public static Server__c createServer(Id systemId){
        Server__c server = generateServer(systemId);
        insert server;
        return server;
    }

    public static Server__c generateServer(Id systemId){
        return new Server__c(
            Name = generateRandomString(),
            Server_License_Key__c = generateRandomString(),
            Server_Purchased_Through__c = 'Customer Sourced',
            System__c = systemId
        );
    }

    //license request
    public static License_Request__c createLicenseRequest(Id systemId, Id vcasVerisonId){
        License_Request__c lr = generateLicenseRequest(systemId,vcasVerisonId);
        insert lr;
        return lr;
    }

    public static License_Request__c generateLicenseRequest(Id systemId, Id vcasVerisonId){
        return new License_Request__c(
            System__c = systemId,
            VCAS_Version__c = vcasVerisonId
        );
    }

    //software request
    public static Software_Request__c createSoftwareRequest(Id accountId, Id vcasVerisonId){
        Software_Request__c sr = generateSoftwareRequest(accountId,vcasVerisonId);
        insert sr;
        return sr;
    }

    public static Software_Request__c generateSoftwareRequest(Id accountId, Id vcasVerisonId){
        return new Software_Request__c(
            Account__c = accountId,
            VCAS_Version__c = vcasVerisonId
        );
    }

    //vcas version
    public static VCAS_ViewRight_Version__c createVCASVersion(){
        VCAS_ViewRight_Version__c VCASVersion = generateVCASVersion();
        insert VCASVersion;
        return VCASVersion;
    }

    public static VCAS_ViewRight_Version__c generateVCASVersion(){
        Decimal version = generateRandomDecimal(0,100).setScale(3);
        return generateVCASVersion(version);
    }

    public static VCAS_ViewRight_Version__c generateVCASVersion(Decimal version){
        return new VCAS_ViewRight_Version__c(Version_Number__c = String.valueOf(version));
    }

    //beta access
    public static Beta_Access_Association__c createBetaAccess(Id accountId, Id versionId){
        Beta_Access_Association__c betaAccess = generateBetaAccess(accountId, versionId);
        insert betaAccess;
        return betaAccess;
    }

    public static Beta_Access_Association__c generateBetaAccess(Id accountId, Id versionId){
        return new Beta_Access_Association__c(Account__c = accountId, VCAS_Version__c = versionId);
    }

    //certificate
    public static Certificate__c createCertificate(Id accountId, Id contactId){
        Certificate__c certificate = generateCertificate(accountId, contactId);
        insert certificate;
        return certificate;
    }

        //certificate
    public static Certificate__c createCertificate(Id accountId, Id contactId, String companyName){
        Certificate__c certificate = generateCertificate(accountId, contactId, companyName);
        insert certificate;
        return certificate;
    }

    public static Certificate__c generateCertificate(Id accountId, Id contactId){
        return generateCertificate(accountId, contactId, generateRandomString());
    }

    public static Certificate__c generateCertificate(Id accountId, Id contactId, String companyName){
        return new Certificate__c(
            Account__c = accountId,
            Company_Name__c = companyName,
            Contact__c = contactId,
            Contact_Department__c = generateRandomString(),
            Contact_Email__c = generateRandomEmail(),
            City__c = generateRandomString(),
            State__c = generateRandomString(2),
            Country__c = generateRandomString(2)
        );
    }

    //service contract
    public static ServiceContract createServiceContract(Id accountId){
        ServiceContract serviceContract = generateServiceContract(accountId);
        insert serviceContract;
        return serviceContract;
    }

    public static ServiceContract generateServiceContract(Id accountId){
        return new ServiceContract(Name = generateRandomString(), AccountId = accountId);
    }

    //Entitlements
    public static Entitlement createEntitlement(Id accountId){
        Entitlement entitlement = generateEntitlement(accountId);
        insert entitlement;
        return entitlement;
    }

    public static Entitlement generateEntitlement(Id accountId){
        ServiceContract sc = new ServiceContract(AccountId=accountId, Name=generateRandomString());
        insert sc;
        return new Entitlement(Name = generateRandomString(), AccountId = accountId, ServiceContractId = sc.Id);
    }

    //site license
    public static Site_License__c createSiteLicense(Id accountId){
        Site_License__c siteLicense = generateSiteLicense(accountId);
        insert siteLicense;
        return siteLicense;
    }

    public static Site_License__c generateSiteLicense(Id accountId){
        return new Site_License__c(Account__c = accountId);
    }

    
    // Quote
    public static SFDC_520_Quote__c createQuote(Id opportunityId) {
        SFDC_520_Quote__c quote = generateQuote(opportunityId);
        insert quote;
        return quote;
    }
    public static SFDC_520_Quote__c generateQuote(Id opportunityId) {
        return new SFDC_520_Quote__c(opportunity__c = opportunityId);
    }

    /* Memoized Properties */

    public static Id SYSADMIN_PROFILE_ID
    {
        get {
            if(null == SYSADMIN_PROFILE_ID) {
                SYSADMIN_PROFILE_ID = [select id from Profile where name = 'System Administrator'][0].id;
            }
            return SYSADMIN_PROFILE_ID; 
        }
        set;
    }
    
    public static Id TEST_ADMIN_ID
    {
        get {
            if(null == TEST_ADMIN_ID) {
                TEST_ADMIN_ID = createUser().id;    
            }
            return TEST_ADMIN_ID;
        }
        set;
    }
        
    /* Random Functions */

    public static String generateRandomString(){return generateRandomString(null);}
    public static String generateRandomString(Integer length){
        if(length == null) length = 1+Math.round( Math.random() * 8 );
        String characters = 'abcdefghijklmnopqrstuvwxyz1234567890';
        String returnString = '';
        while(returnString.length() < length){
            Integer charpos = Math.round( Math.random() * (characters.length()-1) );
            returnString += characters.substring( charpos , charpos+1 );
        }
        return returnString;
    }
    public static String generateRandomEmail(){return generateRandomEmail(null);}
    public static String generateRandomEmail(String domain){
        if(domain == null || domain == '')
            domain = generateRandomString() + '.com';
        return generateRandomString() + '@' + domain;
    }

    public static String generateRandomUrl() {
        return 'http://' + generateRandomString() + '.com'; 
    }

    public static Decimal generateRandomDecimal(Integer lower, Integer upper){
        return (Math.random() * (upper - lower)) + lower;
    }

    public static Boolean pageMessagesContain(String msgStr){
        List<Apexpages.Message> msgs = ApexPages.getMessages();

        boolean b = false;

        for(Apexpages.Message msg : msgs){
            if (msg.getDetail() == msgStr)
            {
                b = true;
            }
        }
        return b;
    }

    /* Test Functions */


    @isTest
    private static void testStandardObjects() {
        User testUser = createUser();
        Case testCase = createCase();
        Lead testLead = createLead();
        Account testAccount = createAccount();
        Contact testContact = createContact(testAccount.id);
        Opportunity testOpportunity = createOpportunity(testAccount.id);
    }
    
    @isTest
    private static void testCustomObjects() {
        Account testAccount = createAccount();
        Entitlement entitl = createEntitlement(testAccount.Id);
        Opportunity testOpportunity = createOpportunity(testAccount.id);
        tenrox__c testProject = createProject(testAccount.Id,entitl.Id);
        M_S_Milestone__c testMilestone = createMandSMilestone(testAccount.id);
        SFDC_520_Quote__c testQuote = createQuote(testOpportunity.id);
    }

    @isTest
    private static void testRandomFunctions() {
        String randomString = generateRandomString();
        String randomEmail = generateRandomEmail();
        String randomUrl = generateRandomUrl();
    }

}
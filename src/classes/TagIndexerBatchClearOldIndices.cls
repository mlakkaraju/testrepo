global class TagIndexerBatchClearOldIndices implements Database.Batchable<sObject>{

	global Database.QueryLocator start(Database.BatchableContext BC) {
		// pass in all non-latest content versions
		return Database.getQueryLocator([select contentDocumentId, tagCsv from ContentVersion where isLatest = false]);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> batch) {
		TagIndexerCore.removeOldVersionsFromIndex(batch);
	}

	global void finish(Database.BatchableContext BC) { }
	
	@isTest
	private static void basicTagIndexerBatchClearOldIndicesTest() {
		Test.startTest();
		TagIndexerBatchClearOldIndices batchClass = new TagIndexerBatchClearOldIndices();
		Database.executeBatch(batchClass);
		Test.stopTest();
	}

}
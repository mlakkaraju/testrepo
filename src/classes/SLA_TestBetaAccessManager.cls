@isTest
private class SLA_TestBetaAccessManager {
	
	@isTest static void test_not_beta() {
		Account acc1 = TestUtil.CreateAccount();

		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Stage__c = SLA_VCASVersionHelper.STAGE_GA;
		insert ver;
		
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(ver); 

        SLA_BetaAccessManagerController ctrl = new SLA_BetaAccessManagerController(stdCtrl);

        PageReference pageRef = Page.SLA_BetaAccessManager;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('id',ver.id);

        System.Assert(TestUtil.pageMessagesContain(SLA_BetaAccessManagerController.ERR_NOT_IN_BETA));
	}

	@isTest static void test_select_account() {
		Account acc1 = TestUtil.CreateAccount();
		Account acc2 = TestUtil.CreateAccount();
		Contact	contact = TestUtil.generateContact(acc1.Id);
		contact.MailingCountry = 'US';
		contact.Email = TestUtil.generateRandomEmail();
		insert contact;
		User user = SLA_CommunityUserFactory.createUserFromContact(contact.Id, false);

		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert ver;
		
		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(ver); 

        SLA_BetaAccessManagerController ctrl = new SLA_BetaAccessManagerController(stdCtrl);

        PageReference pageRef = Page.SLA_BetaAccessManager;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('id',ver.id);

        System.AssertEquals(2, ctrl.selectableAccounts[0].size());
        for(List<SLA_BetaAccessManagerController.SelectableAccount> saPage : ctrl.selectableAccounts){
	        for(SLA_BetaAccessManagerController.SelectableAccount sa : saPage){
	        	if(sa.accountId == acc1.Id){
	        		sa.selected = true;
	        	}
	        }
	    }

        ctrl.selectAccounts();
        System.AssertEquals(acc1.Id, ctrl.selectedAccounts[0].Id);

        ctrl.selectableAccountCustomers.get(ctrl.selectedAccounts[0])[0].selected = true;

        ctrl.submit();

        System.AssertEquals(1, ctrl.selectedCustomers.size());

        System.Assert(ctrl.submitted);

	}

	@isTest static void test_beta_access_manager_controller() {
		//we will grant access to acc1 and remove access to acc2
		Account acc1 = TestUtil.CreateAccount();
		Account acc2 = TestUtil.CreateAccount();

		//create User for acc1
		Contact c = TestUtil.generateContact(acc1.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version;

		//set up acc2 with access
		TestUtil.createBetaAccess(acc2.id, version.id);

		PageReference pageRef = Page.SLA_BetaAccessManager;
        Test.setCurrentPage(pageRef);

        ApexPages.StandardController sc = new ApexPages.standardController(version);
        SLA_BetaAccessManagerController controller = new SLA_BetaAccessManagerController(sc);

        System.assertEquals(controller.selectableAccounts[0].size(), 2);

        //test that they get intilized correctly
        for(List<SLA_BetaAccessManagerController.SelectableAccount> saPage : controller.selectableAccounts){
	        for(SLA_BetaAccessManagerController.SelectableAccount sa : saPage){
	        	if(sa.accountId == acc1.id){
	        		System.assertEquals(sa.selected, false);
	        		//check this one
	        		sa.selected = true;
	        	}else if(sa.accountId == acc2.id){
	        		System.assertEquals(sa.selected, true);
	        		//uncheck this one
	        		sa.selected = false;
	        	}
	        }
	    }

        controller.selectAccounts();
        System.assertEquals(controller.selectedAccounts.size(),1);
		System.assertEquals(controller.selectedAccounts[0].id,acc1.id);

		//test user retrieval
		System.assertEquals(controller.selectableAccountCustomers.size(), 1);

		Account t = (new List<Account>(controller.selectableAccountCustomers.keySet()))[0];
		System.assertEquals(t.id, acc1.id);
		System.assertEquals(controller.selectableAccountCustomers.get(t).size(),1);

		SLA_BetaAccessManagerController.SelectableUser su = controller.selectableAccountCustomers.get(t)[0];

		System.assertEquals(su.customer.id, u.id);

		su.selected = true;

		controller.submit();

		System.assertEquals(controller.selectedCustomers.size(),1);

		//check that the associations where created correctly
		List<Beta_Access_Association__c> assocs = [SELECT Account__c FROM Beta_Access_Association__c WHERE VCAS_Version__c = :version.id];
		System.assertEquals(assocs.size(),1);

		//make sure its not the acc2 account
		for(Beta_Access_Association__c assoc : assocs){
			if(assoc.Account__c == acc2.id){
				System.assert(false);
			}
		}

	}

	
}
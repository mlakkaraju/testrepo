public with sharing class SLA_LicenseRequestSupportController {
	public static final String ERR_UNEXPECTED = 'Something went wrong!';

	public void initSupport(){
		String msg = ApexPages.currentPage().getParameters().get('msg');
		if(msg==null){
			msg = ERR_UNEXPECTED;
		}
		String sevString = ApexPages.currentPage().getParameters().get('severity');
		ApexPages.Severity severity;
		for (ApexPages.Severity sev: ApexPages.Severity.values()) {
		    if (sev.name() == sevString) {
		        severity = sev;
		        break;
		    }
		}
		if(severity==null){
			severity = ApexPages.Severity.ERROR;
		}
		ApexPages.addMessage(new ApexPages.Message(severity, msg));
	}
}
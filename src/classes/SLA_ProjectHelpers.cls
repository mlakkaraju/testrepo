public without sharing class SLA_ProjectHelpers {
 	//status
 	public static final String STATUS_EXECUTING = '6. Executing';
 	public static final String STATUS_HOLD_DELIVERY = 'On Hold - Delivery';
 	public static final String STATUS_COMPLETE = '8. Complete';
 	public static final String STATUS_CLOSED = '9. Closed';
 	public static final String STATUS_RELEASED = '4. Released (not scheduled)';

 	//sub status
 	//public static final String SUBSTATUS_LICENSE_ISSUES = 'License Count Discrepancy';
 	public static final String SUBSTATUS_COMMUNITY_ISSUES = 'Support Issues - Community Error';
 	public static final String SUBSTATUS_SUPPORT_ISSUES = 'Support Issues';

	//project deliverable status
	private static final String PD_DELIVERED_STATUS = 'Delivered';

 	public static void completeProject(Id projectId){
		try{
			Tenrox__c project = [SELECT Id, System__r.VCAS_Version__c, Project_Status_CC__c, Project_Substatus__c 
							FROM Tenrox__c 
							WHERE Id = :projectId 
							LIMIT 1];
		
			//dont want to run with these status's
			if(project.Project_Status_CC__c != STATUS_COMPLETE 
				&& project.Project_Status_CC__c != STATUS_CLOSED){

				List<Project_Deliverable__c> projectDeliverables = [SELECT Product_Family__c,
																		Product_SubFamily__c,
																		Product__r.Name, 
																		Product__r.ProductCode, 
																		Product__r.Solution_Type__c,
																		Product__r.Product_Subfamily__c,
																		Quantity__c,
																		Del__c 
																		FROM Project_Deliverable__c 
																		WHERE Project__c = :projectId];

				Boolean allDelivered = true;
				for(Project_Deliverable__c pd : projectDeliverables){
					if(
						(
							pd.Product_Family__c == 'Client License Fees' 
							&& (pd.Product__r.Product_SubFamily__c != 'DVB STB' && pd.Product__r.Product_SubFamily__c != null) 
							&& pd.Product__r.Solution_Type__c != 'DVB'
						)
						|| 
						(
							pd.Product_Family__c == 'Head End Software License Fees' 
							//[TODO: C50]
							&& (	pd.Product__r.Product_SubFamily__c != 'BCSM' 
								 && pd.Product__r.Product_SubFamily__c !='Package'
								 && pd.Product__r.Product_SubFamily__c != 'Other HE Fees'
								 && pd.Product__r.Product_SubFamily__c != 'RTES Backup (0 Channels)'
								 && pd.Product__r.Product_SubFamily__c != null)
						)
						|| 
						(
							pd.Product__r.Name == 'Annual Maintenance Fee' 
							|| pd.Product__r.ProductCode == 'VMX 06-9999'
						)
					){
						pd.Fulfilled_Date__c = Date.today();
						pd.Quantity_Fulfilled__c = pd.Quantity__c;
						pd.Del__c = PD_DELIVERED_STATUS;
					}			

					if(pd.Del__c != PD_DELIVERED_STATUS){
						allDelivered = false;
					}
				}

				update projectDeliverables;

				if(allDelivered){
					project.Project_Status_CC__c = SLA_ProjectHelpers.STATUS_COMPLETE;
					project.VCAS_Server_Version__c = project.System__r.VCAS_Version__c;
				}else{
					project.Project_Status_CC__c = SLA_ProjectHelpers.STATUS_RELEASED;
				}
			}
			if(project.Project_Substatus__c == SUBSTATUS_SUPPORT_ISSUES || project.Project_Substatus__c == SUBSTATUS_COMMUNITY_ISSUES){
				project.Project_Substatus__c = null;
			}
			//remove customer access
			project.Delivered_to_Customer__c = false;
			update project;
		}catch(Exception e){
			throw new ProjectCompletedException('Failed to complete project! Error:' + e.getMessage() + ' | ' +  e.getStackTraceString());
		}
		
	}

	public static void unDeliverProject(Id projectId){
		 Tenrox__c project = [SELECT Id FROM Tenrox__c WHERE Id = :projectId];
		 project.Delivered_to_Customer__c = false;
         update project;
	}

    public static void holdProject(Tenrox__c proj){
        proj.Project_Status_CC__c = STATUS_HOLD_DELIVERY;
        proj.Project_Substatus__c = SUBSTATUS_COMMUNITY_ISSUES;
        update proj;
    }

	public class ProjectCompletedException extends Exception{}			
}
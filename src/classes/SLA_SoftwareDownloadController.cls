//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Controller for downloading software
public without sharing class SLA_SoftwareDownloadController {

    public VCAS_ViewRight_Version__c version {get; private set;}

    private Software_Request__c softwareRequest;

    public Id accountId {get;set;}

    public Boolean canDownload {get; set;}
    public String downloadUrl {get; set;}

    private Beta_Access_Association__c betaAccess;

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public SLA_SoftwareDownloadController() {
        this.version = [SELECT Name, 
                            Stage__c, 
                            Delivery_File_Name__c 
                            FROM VCAS_ViewRight_Version__c 
                            WHERE Id =:ApexPages.currentPage().getParameters().get('id') 
                            LIMIT 1];
        User u = [SELECT contactId FROM User WHERE Id = :UserInfo.getUserId()]; 
        this.accountId = [SELECT accountId FROM Contact WHERE Id=:u.contactId LIMIT 1].accountId;

        canDownload = false;

    }

    public PageReference downloadOrTerms(){

       if(version.stage__c  == SLA_VCASVersionHelper.STAGE_BETA){
           betaAccess = [SELECT Accepted_TOS__c
           FROM Beta_Access_Association__c 
           WHERE Account__c = :accountId 
           AND VCAS_Version__c = :version.Id LIMIT 1];


           if(betaAccess == null){
                return Page.SLA_CustomerSoftwareAccess;
            }
            //ensure they have accepted TOS
            if(betaAccess != null && !betaAccess.Accepted_TOS__c){
                PageReference pageRef = Page.SLA_CustomerBetaTOS;
                pageRef.getParameters().put('id', version.Id);
                return pageRef;
            }
        }

        createSoftwareRequest();
        return null;
    }

public PageReference backToList(){
    return Page.SLA_CustomerSoftwareAccess;
}

private void createSoftwareRequest(){
    Software_Request__c request = SLA_DownloadRequestHelpers.requestSoftware(accountId,version.Id);
    if(request.Download_URL__c != null){
        canDownload = true;
        downloadUrl = request.Download_URL__c;
    }else{
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'FAILED TO GET DOWNLOAD'));
    }
}

}
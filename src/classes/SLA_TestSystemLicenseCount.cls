@isTest
private class SLA_TestSystemLicenseCount {
	@isTest static void test_sync_license() {
		Account acc = TestUtil.createAccount();
		Entitlement entitl = TestUtil.createEntitlement(acc.Id);
		Contact c = TestUtil.createContact(acc.id);
		System__c sys = TestUtil.createSystem(acc.Id, c.Id);
		sys.LicenseCountWebAndroid__c = 1;
		update sys;

		Opportunity opp = TestUtil.generateOpportunity(acc.Id);
		insert opp;

		// set up product2 and Verify that the results are as expected.
		Product2 p2 = new Product2(Name='Test Product',isActive=true, Family = 'Client License Fees', Product_Subfamily__c = 'Web Android' );
		insert p2;
		Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];

		// set up PricebookEntry and Verify that the results are as expected.
		PricebookEntry pbe = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id=p2.Id, UnitPrice=99, isActive=true);
		System.debug(Test.getStandardPricebookId());
		insert pbe;
		PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];

		OpportunityLineItem li = new OpportunityLineItem();
		li.Quantity = 1;
		li.OpportunityId = opp.Id;
		li.Product_SubFamily__c = 'Web Android';
		li.TotalPrice = 1;
		li.PricebookEntryId = pbe.Id; 
		insert li;

		Tenrox__c proj = TestUtil.generateProject(acc.Id,entitl.Id);
		proj.Opportunity__c = opp.Id;
		proj.System__c = sys.Id;
		insert proj;

		SLA_SystemLicenseCount_Helper helper = new SLA_SystemLicenseCount_Helper();
		sys = helper.updateLicenseCounts(proj.Id);

		//sys = [SELECT LicenseCountWebAndroid__c FROM System__c WHERE Id = :sys.Id];
		System.AssertEquals(2,sys.LicenseCountWebAndroid__c);

	}

	@isTest static void test_device_grouping_OTT_37() {
		Account acc = TestUtil.createAccount();
		Entitlement entitl = TestUtil.createEntitlement(acc.Id);
		Contact c = TestUtil.createContact(acc.id);
		//no gouping. valid

		VCAS_ViewRight_Version__c vcasVerison = TestUtil.generateVCASVersion(3.7);
		insert vcasVerison;
        Certificate__c certificate = TestUtil.createCertificate(acc.Id, c.Id, acc.Name);
        Site_License__c siteLicense = TestUtil.createSiteLicense(acc.Id);

		System__c sys = TestUtil.createSystem(acc.Id, vcasVerison.Id, certificate.Id, siteLicense.Id, acc.Name);
		//sysValid.LicenseCountWebAndroid__c = 1;
		//update sysValid;

		Opportunity opp = TestUtil.generateOpportunity(acc.Id);
		insert opp;

		// set up product2 and Verify that the results are as expected.
		Product2 p2 = new Product2(Name='Test Product',isActive=true, Family = 'Client License Fees', Product_Subfamily__c = 'Bundled OTT Client' );
		insert p2;
		Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];

		// set up PricebookEntry and Verify that the results are as expected.
		PricebookEntry pbe = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id=p2.Id, UnitPrice=99, isActive=true);
		insert pbe;
		PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];

		OpportunityLineItem li = new OpportunityLineItem();
		li.Quantity = 100;
		li.OpportunityId = opp.Id;
		li.Product_Family__c = 'Client License Fees';
		li.Product_SubFamily__c = 'Bundled OTT Client';
		li.TotalPrice = 1;
		li.PricebookEntryId = pbe.Id; 
		insert li;

		Tenrox__c proj = TestUtil.generateProject(acc.Id,entitl.Id);
		proj.Opportunity__c = opp.Id;
		proj.System__c = sys.Id;
		insert proj;

		Test.startTest();
		SLA_SystemLicenseCount_Helper helper = new SLA_SystemLicenseCount_Helper();
		sys = helper.updateLicenseCounts(proj.Id);
		SLA_SystemLicenseCount_Helper.getTotalLicenseCount(sys.Id);
		Test.stopTest();

		//sys = [SELECT LicenseCountWebAndroid__c FROM System__c WHERE Id = :sys.Id];

	}

	@isTest static void test_device_grouping_OTT_33() {
		Account acc = TestUtil.createAccount();
		Entitlement entitl = TestUtil.createEntitlement(acc.Id);
		Contact c = TestUtil.createContact(acc.id);
		//no gouping. valid

		VCAS_ViewRight_Version__c vcasVerison = TestUtil.generateVCASVersion(3.3);
		insert vcasVerison;
        Certificate__c certificate = TestUtil.createCertificate(acc.Id, c.Id, acc.Name);
        Site_License__c siteLicense = TestUtil.createSiteLicense(acc.Id);

		System__c sys = TestUtil.createSystem(acc.Id, vcasVerison.Id, certificate.Id, siteLicense.Id, acc.Name);
		//sysValid.LicenseCountWebAndroid__c = 1;
		//update sysValid;

		Opportunity opp = TestUtil.generateOpportunity(acc.Id);
		insert opp;

		// set up product2 and Verify that the results are as expected.
		Product2 p2 = new Product2(Name='Test Product',isActive=true, Family = 'Client License Fees', Product_Subfamily__c = 'Bundled OTT Client' );
		insert p2;
		Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];

		// set up PricebookEntry and Verify that the results are as expected.
		PricebookEntry pbe = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id=p2.Id, UnitPrice=99, isActive=true);
		insert pbe;
		PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];

		OpportunityLineItem li = new OpportunityLineItem();
		li.Quantity = 100;
		li.OpportunityId = opp.Id;
		li.Product_Family__c = 'Client License Fees';
		li.Product_SubFamily__c = 'Bundled OTT Client';
		li.TotalPrice = 1;
		li.PricebookEntryId = pbe.Id; 
		insert li;

		Tenrox__c proj = TestUtil.generateProject(acc.Id,entitl.Id);
		proj.Opportunity__c = opp.Id;
		proj.System__c = sys.Id;
		insert proj;

		Test.startTest();
		SLA_SystemLicenseCount_Helper helper = new SLA_SystemLicenseCount_Helper();
		sys = helper.updateLicenseCounts(proj.Id);
		Test.stopTest();

		//sys = [SELECT LicenseCountWebAndroid__c FROM System__c WHERE Id = :sys.Id];

	}

	@isTest static void test_device_grouping_remove() {
		Account acc = TestUtil.createAccount();
		Entitlement entitl = TestUtil.createEntitlement(acc.Id);
		Contact c = TestUtil.createContact(acc.id);
		
		//no gouping. valid
		System__c sysValid = TestUtil.createSystem(acc.Id, c.Id);
		sysValid.LicenseCountWebAndroid__c = 1;
		update sysValid;

		//valid because its a different group
		System__c sysValid2 = TestUtil.createSystem(acc.Id, c.Id);
		sysValid2.DeviceGroupWebMac__c = 'Group1';
		update sysValid2;

		//group matching license, invalid
		System__c sysInvalid = TestUtil.createSystem(acc.Id, c.Id);
		sysInvalid.DeviceGroupWebAndroid__c = 'Group1';
		update sysInvalid;

		Opportunity opp = TestUtil.generateOpportunity(acc.Id);
		insert opp;

		// set up product2 and Verify that the results are as expected.
		Product2 p2 = new Product2(Name='Test Product',isActive=true, Family = 'Client License Fees', Product_Subfamily__c = 'Web Android' );
		insert p2;
		Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];

		// set up PricebookEntry and Verify that the results are as expected.
		PricebookEntry pbe = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(), Product2Id=p2.Id, UnitPrice=99, isActive=true);
		insert pbe;
		PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];

		OpportunityLineItem li = new OpportunityLineItem();
		li.Quantity = 100;
		li.OpportunityId = opp.Id;
		li.Product_Family__c = 'Client License Fees';
		li.Product_SubFamily__c = 'Web Android';
		li.TotalPrice = 1;
		li.PricebookEntryId = pbe.Id; 
		insert li;

		Tenrox__c proj = TestUtil.generateProject(acc.Id,entitl.Id);
		proj.Opportunity__c = opp.Id;
		insert proj;

		Test.startTest();
		SLA_SystemLicenseCount_Helper helper = new SLA_SystemLicenseCount_Helper();
		List<System__c> validSystems = helper.removeDeviceGroupingConflicts(proj.Id, new List<System__c>{sysValid,sysValid2,sysInvalid});
		Test.stopTest();

		//sys = [SELECT LicenseCountWebAndroid__c FROM System__c WHERE Id = :sys.Id];
		System.AssertEquals(2,validSystems.size());
		for(System__c sys : validSystems){
			System.AssertNotEquals(sysInvalid.Id, sys.Id);
		}
	}
}
/*
Developer: Ralph Callaway <ralphwcallaway@gmail.com>
Description:
  Clears all line items for an opportunity
*/
public with sharing class ClearProducts {

	public ApexPages.StandardController controller;
	
	public ClearProducts(ApexPages.StandardController controller) {
		this.controller = controller;
	}
	
	public PageReference clear() {
		Id opptyId = controller.getId();
		delete [select id from OpportunityLineItem where opportunityId = :opptyId];
		return controller.cancel();
	}
	
	@isTest
	private static void testClearProducts() {
		// create test data
		Integer lineItemCount = 3;
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Account testAccount = TestUtil.createAccount();
		Opportunity testOpportunity = TestUtil.generateOpportunity(testAccount.id);
		testOpportunity.pricebook2Id = productHelper.pricebook.id;
		insert testOpportunity;
		List<OpportunityLineItem> testLineItems = new List<OpportunityLineItem>();
		for(Integer i = 0; i < lineItemCount; i++) {
			testLineItems.add(TestUtilProductHelper.generateOpportunityLineItem(testOpportunity.id, productHelper.testPricebookEntry.id));
		}
		insert testLineItems;
		
		// verify initial conditions
		Integer verifyLineItemCount = [select count() from OpportunityLineItem where opportunityId = :testOpportunity.id];
		system.assertEquals(lineItemCount, verifyLineItemCount);
		
		// load clear product controller 
		ApexPages.StandardController controller = new ApexPages.StandardController(testOpportunity);
		ClearProducts extension = new ClearProducts(controller);
		PageReference nextPage = extension.clear();
		
		// verify results
		System.assertNotEquals(null, nextPage);
		verifyLineItemCount = [select count() from OpportunityLineItem where opportunityId = :testOpportunity.id];
		system.assertEquals(0, verifyLineItemCount);
	}
}
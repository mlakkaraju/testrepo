/*
Created By: ralphwcallaway@gmail.com
Description: Testing helper class for testing product related code.
*/
public with sharing class TestUtilProductHelper {

	static {
		system.assert(Test.isRunningTest(), 'TestUtilProductHelper may only be referenced in test classes');
	}

	public static final Decimal DEFAULT_PRICE = 1;
	
	public Pricebook2 pricebook { get; set; }
	
	public Product2 testProduct { get; set; }
	public Product2 testProduct2 { get; set; }
	
	public PricebookEntry testPricebookEntry { get; set; }
	public PricebookEntry testPricebookEntry2 { get; set; }
	
	public TestUtilProductHelper() {
		// create pricebook
		pricebook = new Pricebook2(name = TestUtil.TEST_STRING, isActive = true);
		insert pricebook;
		
		// create products
		testProduct = new Product2(name = TestUtil.TEST_STRING);
		testProduct2 = new Product2(name = TestUtil.TEST_STRING);
		insert new Product2[] { testProduct, testProduct2 };
		
		// query standard pricebook
		Pricebook2 standardPricebook = [select id from Pricebook2 where isStandard = true];
		
		// create standard pricebook entries
		List<PricebookEntry> standardPricebookEntries = new List<PricebookEntry>();
		PricebookEntry testPricebookEntryStandard =
			new PricebookEntry(pricebook2Id = standardPricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct.id);
		standardPricebookEntries.add(testPricebookEntryStandard);
		PricebookEntry testPricebookEntryStandard2 =
			new PricebookEntry(pricebook2Id = standardPricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct2.id);
		standardPricebookEntries.add(testPricebookEntryStandard2);
		insert standardPricebookEntries;
		
		// create test pricebook entries
		List<PricebookEntry> testPricebookEntries = new List<PricebookEntry>();
		testPricebookEntry =
			new PricebookEntry(pricebook2Id = pricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct.id);
		testPricebookEntries.add(testPricebookEntry);
		testPricebookEntry2 =
			new PricebookEntry(pricebook2Id = pricebook.id, unitPrice = DEFAULT_PRICE,
				isActive = true, product2Id = testProduct2.id);
		testPricebookEntries.add(testPricebookEntry2);
		insert testPricebookEntries;
	}
	
	public static OpportunityLineItem createOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		OpportunityLineItem lineItem = generateOpportunityLineItem(opportunityId, pricebookEntryId);
		insert lineItem;
		return lineItem;
	}
	
	public static OpportunityLineItem generateOpportunityLineItem(Id opportunityId, Id pricebookEntryId) {
		return new OpportunityLineItem(opportunityId = opportunityId,
			pricebookEntryId = pricebookEntryId,
			quantity = 1,
			totalPrice = DEFAULT_PRICE);
	}
	
	@isTest
	private static void testProductHelper() {
		// create product helper
		Test.startTest();
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Test.stopTest();
		
		// verify everything got inserted
		System.assertNotEquals(null, productHelper.pricebook.id);
		System.assertNotEquals(null, productHelper.testProduct.id);
		System.assertNotEquals(null, productHelper.testPricebookEntry.id);
		
		// test OpportunityLineItem creation
		Account testAccount = TestUtil.createAccount();
		Opportunity testOpportunity = TestUtil.generateOpportunity(testAccount.id);
		testOpportunity.pricebook2Id = productHelper.pricebook.id;
		insert testOpportunity;
		OpportunityLineItem testLineItem = TestUtilProductHelper.createOpportunityLineItem(testOpportunity.id, productHelper.testPricebookEntry.id);
	}

}
@isTest
private class TestProjectCreateDeliverables {
	
/* Test Methods */
	@isTest(SeeAllData=true)
	private static void testCreateProjectDeliverablesFromIntercompanyProject() {
		// create test data
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Account testAccount = TestUtil.createAccount();
		Opportunity testMainOppty = TestUtil.createOpportunity(testAccount.id);
		tenrox__c testMainProject = TestUtil.generateProject();
		testMainProject.Opportunity__c = testMainOppty.id;
		insert testMainProject;
 		Opportunity testInterCompanyOppty = TestUtil.generateOpportunity(testAccount.id);
 		testInterCompanyOppty.Pricebook2Id = productHelper.pricebook.id;
 		insert testInterCompanyOppty;
 		OpportunityLineItem testLineItem = TestUtilProductHelper.createOpportunityLineItem(
 			  testInterCompanyOppty.id
 			, productHelper.testPricebookEntry.id
 		);
 		
 		// create intercompany project
 		Test.startTest();
 		tenrox__c testInterCompanyProject = TestUtil.generateProject();
 		testInterCompanyProject.opportunity__c = testInterCompanyOppty.id;
 		testInterCompanyProject.Customer_main_Project__c = testMainProject.id;
 		insert testInterCompanyProject;
 		Test.stopTest();
 		
 		// valdiate project deliverables created with expected fields populated
 		List<Project_Deliverable__c> deliverables = [
 			select amount__c, customer_main_project__c, line_description__c, product__c, quantity__c
 			from Project_Deliverable__c
 			where project__c = :testInterCompanyProject.id
 		]; 
 		system.assertEquals(1, deliverables.size());
 		Project_Deliverable__c deliverable = deliverables[0];
 		system.assertEquals(testMainProject.id, deliverable.customer_main_project__c);
	}
	
	@isTest(SeeAllData=true)
	private static void testCreateProjectDeliverablesFromProject() {
		// create test data
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Account testAccount = TestUtil.createAccount();
		Opportunity testOppty = TestUtil.generateOpportunity(testAccount.id);
		testOppty.pricebook2Id = productHelper.pricebook.id;
		insert testOppty;
		OpportunityLineItem testLineItem = TestUtilProductHelper.generateOpportunityLineItem(
 			  testOppty.id
 			, productHelper.testPricebookEntry.id
 		);
 		testLineItem.description = TestUtil.TEST_STRING;
 		insert testLineItem;
 		
 		// create intercompany project
 		Test.startTest();
 		tenrox__c testProject = TestUtil.generateProject();
		testProject.Opportunity__c = testOppty.id;
		insert testProject;
 		Test.stopTest();
 		
 		// valdiate project deliverables created with expected fields populated
 		List<Project_Deliverable__c> deliverables = [
 			select amount__c, line_description__c, product__c, quantity__c
 			from Project_Deliverable__c
 			where project__c = :testProject.id
 		]; 
 		system.assertEquals(1, deliverables.size());
 		Project_Deliverable__c deliverable = deliverables[0];
 		system.assertEquals(testLineItem.totalPrice, deliverable.amount__c);
 		system.assertEquals(testLineItem.description, deliverable.line_description__c);
 		system.assertEquals(productHelper.testProduct.id, deliverable.product__c);
 		system.assertEquals(testLineItem.quantity, deliverable.quantity__c);
	}

	
}
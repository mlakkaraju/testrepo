//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Client used to make callouts to AWS
//  	1: to request a license download URL
//		2: to request a software download URL

public with sharing class SLA_DeliveryClient {
	@TestVisible private static final String ENDPOINT = Label.SLA_AWSEndpoint;
	private static final String APIKEY = Label.SLA_AWSKey;

	public static APIResponse sendRequest(IAPIMethod requestData){
		HttpRequest req = new HttpRequest();
		req.setEndpoint(ENDPOINT);
		req.setMethod('POST');
		req.setBody(requestData.toJSON());

		Http http = new Http();
		HTTPResponse res = http.send(req);

		system.debug(res.getBody());
		return (APIResponse) JSON.deserialize(res.getBody(), APIResponse.class);
	}

	public interface IAPIMethod {
		String toJSON();
	}

	public class LicenseRequest implements IAPIMethod{
		public final String method = 'zip_licenses';
		public String guid {get;set;}
		public String api_key = SLA_DeliveryClient.APIKEY;
		public Integer num_licenses;

		public LicenseRequest(String licenseRequestId, Integer numLicense){
			this.guid = licenseRequestId;
			this.num_licenses = numLicense;
		}

		public String toJSON(){
			return JSON.serialize(this);
		}
	}

	public class SoftwareRequest implements IAPIMethod{
		public final String method = 'generate_rpm_url';
		public String guid {get;set;}
		public String api_key = SLA_DeliveryClient.APIKEY;
		public String file_name;

		public SoftwareRequest(String softwareRequestId, String fileName){
			this.guid = softwareRequestId;
			this.file_name = fileName;
		}

		public String toJSON(){
			return JSON.serialize(this);
		}
	}

	public class APIResponse{
		public String status {get; set;}
		public String message {get; set;}
	}

}
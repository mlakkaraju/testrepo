@isTest
private class SLA_TestLicenseCertificateMasterClient {
	
	@isTest static void test_generatedCode() {
		Test.setMock(WebServiceMock.class, new SLA_LicenseCertificateWebserviceMock());
		LicenseCertificateMasterClass2.clicgen client = new LicenseCertificateMasterClass2.clicgen();

		client.getCertificate('test','test','test','test',
								'test','test','test',true,
								true, 1,'test','test',
								'test','test','test','test',
								'test','test','test','test',
								'test');

		client.generateCertificate('test','test','test','test',
									'test','test','test',true,
									true, 1,'test','test',
									'test','test','test','test',
									'test','test','test','test',
									'test',true, 
                          			1, 'test', 'test', 'test', 'test');

		client.generateLicense('test','test','test','test',
								'test','test','test',
								'test','test','test',
								'test', 'test', 'test',
								'test', 'test', 'test', 'test');
		
		client.getLicense('test','test','test','test',
							'test','test','test',
							'test','test','test',
							'test', 'test', 'test',
							'test', 'test', 'test');
	}
	

	
}
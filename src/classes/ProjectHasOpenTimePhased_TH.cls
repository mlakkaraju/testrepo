//Updated by: Charlie@callawaycloudconsulting.com
//Refactor of UpdateHasOpenTimePhasedProject.Trigger to use ProjectMasterTrigger instead.
public with sharing class ProjectHasOpenTimePhased_TH implements TriggerHandler.HandlerInterface {


	public void handle(){
		Map<Id, String> accountMap = new Map<Id, String>();

		for (Integer i=0; i<Trigger.new.size();i++)
		{
			Tenrox__c newProj = (Tenrox__c) Trigger.new[i];
			String newRevenueDeliveryType = newProj.Revenue_Delivery_Type__c;
			String oldRevenueDeliveryType = ((Tenrox__c)Trigger.old[i]).Revenue_Delivery_Type__c;

			if (newRevenueDeliveryType == 'Time-phased' && newRevenueDeliveryType != oldRevenueDeliveryType && newProj.Account__c != null)
			{
				accountMap.put(newProj.Account__c, '' );
			}  
		}
		Boolean empty = accountMap.isEmpty();

		if (!empty)
		{
			Set <Id> accountIdSet = new Set<Id>(); 
			accountIdSet = accountMap.keySet();

			Account[] accounts = [Select Id, Has_open_Time_Phased_project__c from Account where Id in: accountIdSet];

			for (Account acc : accounts)
			{
				acc.Has_open_Time_Phased_project__c = true;
			}
			update accounts;
		}
	}
}
public with sharing class PopularContent {

	public Integer contentCount { get; set; }
	
	public List<ContentWrapper> popularContent {
		get {
			if(popularContent == null) {
				// query download history for most popular content
				Map<Id, Integer> downloadCountMap = new Map<Id, Integer>();
				for(AggregateResult downloadSummary : 
				 [select count(id) cnt, contentVersion.contentDocumentId from ContentVersionHistory 
				  where field ='contentVersionDownloaded'
				  group by contentVersion.contentDocumentId 
				  order by count(id) desc limit :contentCount]) {
					
					downloadCountMap.put((Id) downloadSummary.get('contentDocumentId'), (Integer) downloadSummary.get('cnt'));  	
				}
				
				// query content titles and wrap with download count
				List<ContentWrapper> unsortedPopularContent = new List<ContentWrapper>();
				for(ContentDocument content : 
				 [select title from ContentDocument where id in :downloadCountMap.keySet()]) {
					ContentWrapper contentWrapper = new ContentWrapper();
					contentWrapper.content = content;
					contentWrapper.downloads = downloadCountMap.get(content.id);
					unsortedPopularContent.add(contentWrapper);
				}
				
				// sort by download count
				Map<Integer, List<ContentWrapper>> contentByDownloadCnt = new Map<Integer, List<ContentWrapper>>();
				for(ContentWrapper contentWrapper : unsortedPopularContent) {
					if(!contentByDownloadCnt.containsKey(contentWrapper.downloads))
						contentByDownloadCnt.put(contentWrapper.downloads, new List<ContentWrapper>());
					contentByDownloadCnt.get(contentWrapper.downloads).add(contentWrapper);
				}
				popularContent = new List<ContentWrapper>();
				List<Integer> downloadCntList = new List<Integer>();
				downloadCntList.addAll(contentByDownloadCnt.keySet());
				downloadCntList.sort();
				for(Integer i = downloadCntList.size() - 1; i >= 0; i--) {
					popularContent.addAll(contentByDownloadCnt.get(downloadCntList.get(i)));
				}
			}
			return popularContent;
		}
		private set;
	}
	
	public class ContentWrapper {
		public ContentDocument content { get; set; }
		public Integer downloads { get; set; }
	}
	
	@isTest
	private static void basicPopularContentTest() {
		PopularContent controller = new PopularContent();
		controller.contentCount = 5;
		List<ContentWrapper> popularContent = controller.popularContent;
	}

}
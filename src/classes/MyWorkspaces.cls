public with sharing class MyWorkspaces {

	public List<ContentWorkspace> workspaces {
		get {
			if(workspaces == null) {
				workspaces = [select id, name, description from ContentWorkspace];
			}
			return workspaces;
		}
		private set;
	}

	@isTest 
	private static void basicMyWorkSpaceTest() {
		MyWorkspaces controller = new MyWorkspaces();
		List<ContentWorkspace> workspaces = controller.workspaces;
	}

}
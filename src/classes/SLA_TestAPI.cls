//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Tests for Webservice Endpoints with AWS Delievery Platform
@isTest
private class SLA_TestAPI {
	
	@isTest static void test_expire_link_no_auth() {
		System__c sys = TestUtil.createSystem();
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		License_Request__c lr = TestUtil.createLicenseRequest(sys.id, version.id);

		SLA_ExpireLinkEndpoints.ExpireLinkReturn ret =
		SLA_ExpireLinkEndpoints.expireLink(null,
			SLA_ExpireLinkEndpoints.OPPERATION_LICENSE,
			lr.id
			);

		System.assertEquals(ret.code, SLA_ExpireLinkEndpoints.STATUS_UNAUTHORIZED);
		System.assertEquals(ret.message, SLA_ExpireLinkEndpoints.STATUS_UNAUTHORIZED_MESSAGE);
	}
	
	@isTest static void test_expire_link_bad_method() {
		System__c sys = TestUtil.createSystem();
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		License_Request__c lr = TestUtil.createLicenseRequest(sys.id, version.id);

		SLA_ExpireLinkEndpoints.ExpireLinkReturn ret =
		SLA_ExpireLinkEndpoints.expireLink(SLA_ExpireLinkEndpoints.AUTH_KEY,
			'abc123',
			lr.id
			);

		System.assertEquals(ret.code, SLA_ExpireLinkEndpoints.STATUS_BAD);
		System.assertEquals(ret.message, SLA_ExpireLinkEndpoints.STATUS_BAD_TYPE_MESSAGE);
	}

	//404
	@isTest static void test_expire_link_not_found() {
		System__c sys = TestUtil.createSystem();
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		License_Request__c lr = TestUtil.createLicenseRequest(sys.id, version.id);

		SLA_ExpireLinkEndpoints.ExpireLinkReturn ret =
		SLA_ExpireLinkEndpoints.expireLink(SLA_ExpireLinkEndpoints.AUTH_KEY,
			SLA_ExpireLinkEndpoints.OPPERATION_LICENSE,
			version.id
			);

		System.assertEquals(ret.code, SLA_ExpireLinkEndpoints.STATUS_NOTFOUND);
		System.assertEquals(ret.message, SLA_ExpireLinkEndpoints.STATUS_NOTFOUND_MESSAGE);
	}

	//success
	@isTest static void test_expire_link_success() {
		System__c sys = TestUtil.createSystem();
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		License_Request__c lr = TestUtil.createLicenseRequest(sys.id, version.id);

		SLA_ExpireLinkEndpoints.ExpireLinkReturn ret =
		SLA_ExpireLinkEndpoints.expireLink(SLA_ExpireLinkEndpoints.AUTH_KEY,
			SLA_ExpireLinkEndpoints.OPPERATION_LICENSE,
			lr.id
			);

		System.assertEquals(ret.code, SLA_ExpireLinkEndpoints.STATUS_OK);
		System.assertEquals(ret.message, SLA_ExpireLinkEndpoints.STATUS_OK_MESSAGE);

		//check that it actually updated the record
		lr = [SELECT status__c FROM License_Request__c WHERE Id =:lr.id];
		System.assertEquals(lr.status__c, SLA_LicenseRequestHelpers.STATUS_COMPLETE);
	}

	//success
	@isTest static void test_expire_link_success2() {
		Account acc = TestUtil.createAccount();
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		Software_Request__c sr = TestUtil.createSoftwareRequest(acc.id, version.id);
		sr.Reference_Id__c = '123';
		update sr;

		SLA_ExpireLinkEndpoints.ExpireLinkReturn ret =
		SLA_ExpireLinkEndpoints.expireLink(SLA_ExpireLinkEndpoints.AUTH_KEY,
			SLA_ExpireLinkEndpoints.OPPERATION_SOFTWARE,
			sr.Reference_Id__c
			);

		System.assertEquals(SLA_ExpireLinkEndpoints.STATUS_OK, ret.code);
		System.assertEquals(SLA_ExpireLinkEndpoints.STATUS_OK_MESSAGE, ret.message);

		//check that it actually updated the record
		sr = [SELECT status__c FROM Software_Request__c WHERE Id =:sr.id];
		System.assertEquals(SLA_DownloadRequestHelpers.STATUS_EXPIRED ,sr.status__c);
	}

	
}
@isTest
private class SLA_TestCommunityHelpers {
	
	@isTest static void test_user_creation() {
		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		c.firstName = 'john';
		c.lastName = 'doe';
		insert c;
		User u1 = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		Contact c2 = TestUtil.generateContact(acc.id);
		c2.email = TestUtil.generateRandomEmail();
		c2.firstName = 'john';
		c2.lastName = 'doe';
		insert c2;
		User u2 = SLA_CommunityUserFactory.createUserFromContact(c2.id,false);

		u1 = [SELECT communityNickname FROM User WHERE Id = :u1.Id];
		u2 = [SELECT communityNickname FROM User WHERE Id = :u2.Id];

		System.AssertNotEquals(u1.communityNickname,u2.communityNickname);
	}
	

	@isTest static void test_version_number_extract() {
		
		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Version_Number__c = '4.31.2 Lab';
		insert ver;
		Decimal versionNum = SLA_VCASVersionHelper.getVersionNumber(ver.id);
		System.AssertEquals(4.3, versionNum);
	}

	@isTest static void test_version_number_compare() {
		List<String> vStrings = new List<String>{'1.1.1.1','1.1.1', '1.1.2', '1.1.1b', '1.1.b', '1.1.', '2.1', '2', '1.23ada', 'aba'};
		List<SLA_VCASVersionHelper.VersionNumber> versions = new List<SLA_VCASVersionHelper.VersionNumber>();
		for(String s : vStrings){
			versions.add(new SLA_VCASVersionHelper.VersionNumber(s));
		}
		versions.sort();
		system.assertEquals('2.1', versions[9].versionString);
		system.assertEquals('2', versions[8].versionString);
		system.assertEquals('1.23ada', versions[7].versionString);
		system.assertEquals('1.1.2', versions[6].versionString);
		system.assertEquals('1.1.1.1', versions[5].versionString);
		if(!(versions[4].versionString == '1.1.1' || versions[4].versionString == '1.1.1b')){
			system.assert(false);
		}
		if(!(versions[3].versionString == '1.1.1' || versions[3].versionString == '1.1.1b')){
			system.assert(false);
		}
		if(!(versions[2].versionString == '1.1.' || versions[2].versionString == '1.1.b')){
			system.assert(false);
		}
		if(!(versions[1].versionString == '1.1.' || versions[1].versionString == '1.1.b')){
			system.assert(false);
		}
		system.assertEquals('aba', versions[0].versionString);
	}
}
@isTest public class SLA_LicenseCertificateWebserviceMock implements WebServiceMock {
 public void doInvoke(
                     Object stub,
                     Object request,
                     Map<String, Object> response,
                     String endpoint,
                     String soapAction,
                     String requestName,
                     String responseNS,
                     String responseName,
                     String responseType){

    if(responseName == 'generateCertificateResponse'){
      LicenseCertificateMasterClass2.generateCertificateResponse_element responseElement =
       new LicenseCertificateMasterClass2.generateCertificateResponse_element();
      responseElement.xiResult = 0;
      response.put('response_x', responseElement);
    }else if(responseName == 'getCertificateResponse'){
      LicenseCertificateMasterClass2.getCertificateResponse_element responseElement =
       new LicenseCertificateMasterClass2.getCertificateResponse_element();
      responseElement.xiResult = 0;
      response.put('response_x', responseElement);
    }else if(responseName == 'generateLicenseResponse'){
      LicenseCertificateMasterClass2.generateLicenseResponse_element responseElement = 
       new LicenseCertificateMasterClass2.generateLicenseResponse_element();
      responseElement.xiResult = 0;
      response.put('response_x', responseElement);
    }else if(responseName == 'getLicenseResponse'){
      LicenseCertificateMasterClass2.getLicenseResponse_element responseElement =
       new LicenseCertificateMasterClass2.getLicenseResponse_element();
      responseElement.xiResult = 0;
      response.put('response_x', responseElement);
    }
    
  }
}
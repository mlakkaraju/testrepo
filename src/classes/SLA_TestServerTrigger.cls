@isTest
private class SLA_TestServerTrigger {
	
	@isTest static void test_NewServer1() {
		System__c sys1 = TestUtil.createSystem();
		TestUtil.createServer(sys1.Id);
		Certificate__c cert = [SELECT id FROM Certificate__c WHERE Id = :sys1.certificate__c];
		cert.hid_Added_Server__c = false;
		update cert;
		Test.startTest();
		Server__c server = TestUtil.createServer(sys1.Id);
		Test.stopTest();
		sys1 = [SELECT certificate__r.hid_Added_Server__c FROM System__c WHERE Id = :sys1.Id];
		System.assertEquals(true, sys1.certificate__r.hid_Added_Server__c);
	}

	@isTest static void test_NewServer2() {
		System__c sys1 = TestUtil.createSystem();
		Test.startTest();
		Server__c server = TestUtil.createServer(sys1.Id);
		Test.stopTest();
		sys1 = [SELECT certificate__r.hid_Added_Server__c FROM System__c WHERE Id = :sys1.Id];
		System.assertEquals(true, sys1.certificate__r.hid_Added_Server__c);
	}

	@isTest static void test_resetAddedField() {

		System__c sys1 = TestUtil.createSystem();
		Server__c server = TestUtil.createServer(sys1.Id);
		sys1 = [SELECT certificate__r.hid_Added_Server__c FROM System__c WHERE Id = :sys1.Id];
		System.assertEquals(true, sys1.certificate__r.hid_Added_Server__c);
		
		License_Request__c lr = SLA_LicenseRequestHelpers.openNewRequest(sys1.Id);
		Test.startTest();
		SLA_LicenseRequestHelpers.startLicenseGeneration(lr);
		Test.stopTest();
		sys1 = [SELECT certificate__r.hid_Added_Server__c FROM System__c WHERE Id = :sys1.Id];
		System.assertEquals(false, sys1.certificate__r.hid_Added_Server__c);
	}
}
@isTest
private class SLA_TestLicenseRequestWizard {
	
	@isTest static void test_wizard_general() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;

        	System.AssertEquals(false, ctrl.getShowSubmit()); 
        	System.AssertEquals(false, ctrl.getShowPrevious());
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(true, ctrl.getShowPrevious());
        	pf = ctrl.previous();
        	ctrl.supportCaseFeedback = 'Test feedback';
        	ctrl.createUserCase();
        }
        System.AssertEquals(Page.SLA_LicenseRequestWizard_ProjectSelect.getUrl(), pf.getUrl());
	}

	@isTest static void test_project_select() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	pf = ctrl.next();
        }
        System.AssertEquals(tdata.acc.Id, ctrl.currentAccount.Id);
        System.AssertEquals(tdata.proj.Id, ctrl.project.Id);
        System.AssertEquals(Page.SLA_LicenseRequestWizard_AccountSelect.getUrl(), pf.getUrl());

	}

	@isTest static void test_project_select_fastforward() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
		tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Servers.getUrl();
		update tdata.proj;

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	pf = ctrl.next();
        }
        System.AssertEquals(Page.SLA_LicenseRequestWizard_Servers.getUrl(), pf.getUrl());
	}
	
	@isTest static void test_project_upgrade() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.upgradeSystem());	
        	System.AssertEquals(null, ctrl.project);
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_SystemSelect.getUrl(), ApexPages.CurrentPage().getUrl());
        	ctrl.initSystemSelect();
        	System.AssertEquals(1, ctrl.selectableSystems.values().size());
        }
        
	}

	@isTest static void test_account_select_current() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
            //account select
            ctrl.initAccountSelect();
        	ctrl.current = true;
        	pf = ctrl.next();
        }
        System.AssertEquals(tdata.acc.Id, ctrl.project.Account__c);
        System.AssertEquals(Page.SLA_LicenseRequestWizard_SystemSelect.getUrl(), pf.getUrl());
	}

	@isTest static void test_account_select_other() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	//account select
            ctrl.initAccountSelect();
        	ctrl.other = true;
        	ctrl.otherCompanyName = 'test';
        	ctrl.otherCompanyLocation = 'test';
        	pf = ctrl.next();
        }
        System.Assert(pf.getUrl().contains(Page.SLA_LicenseRequestWizard_Support.getUrl()));
        System.AssertEquals(SLA_LicenseRequestWizardController.PROJECT_CASE_STATUS, ctrl.project.Project_Status_CC__c);
        System.AssertEquals(SLA_LicenseRequestWizardController.PROJECT_CASE_SUBSTATUS, ctrl.project.Status_Comments__c);
	}
	

	@isTest static void test_account_select_other_missing_data() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
            //account select
            ctrl.initAccountSelect();
        	ctrl.other = true;
        	ctrl.otherCompanyName = '';
        	ctrl.otherCompanyLocation = 'test';
        	pf = ctrl.next();
        }
        System.AssertEquals(null, pf);
        System.Assert(TestUtil.pageMessagesContain(SLA_LicenseRequestWizardController.ERR_ACCOUNT_OTHER_MISSING_INFO));
        
	}

	@isTest static void test_system_select() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
        tdata.proj.system__c = null;
        update tdata.proj;
        
		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	//account select
            ctrl.initAccountSelect();
        	ctrl.current = true;
        	Test.setCurrentPage(ctrl.next());
        	//system
        	ctrl.initSystemSelect();

            System.debug(ctrl.selectableSystems);
        	ctrl.selectableSystems.values()[0][0].selected = true;
        	pf = ctrl.next();
        }
        System.AssertEquals(tdata.sys.Id, ctrl.systemId);
        System.AssertEquals(Page.SLA_LicenseRequestWizard_System1.getUrl(), pf.getUrl());
	}


	@isTest static void test_system_select_regenerate() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();

		PageReference pageRef = Page.SLA_LicenseRequestWizard_System1;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//system select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ApexPages.currentPage().getParameters().put('system_id',tdata.sys.Id);
        	//system
        	ctrl.initSystem1();
        	pf = ctrl.next();
        }
        System.AssertEquals(tdata.sys.Id, ctrl.systemId);
        System.AssertEquals(Page.SLA_LicenseRequestWizard_Servers.getUrl(), pf.getUrl());
	}


	@isTest static void test_system_new() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
        tdata.proj.system__c = null;
        update tdata.proj;

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        PageReference pf; 
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	//account select
            ctrl.initAccountSelect();
        	ctrl.current = true;
        	Test.setCurrentPage(ctrl.next());
        	//system
        	ctrl.initSystemSelect();
        	
        	Test.setCurrentPage(ctrl.createNewSystem());
        	System.Assert(ctrl.isNewSystem);
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_System1.getUrl(), ApexPages.CurrentPage().getUrl());
        	
        	ctrl.initSystem1();
        	ctrl.sys.Type__c = 'Lab';
            ctrl.sys.CompanyName__c = 'testing';
            ctrl.sys.VCAS_Version__c = TestUtil.CreateVCASVersion().Id;
            Test.setCurrentPage(ctrl.next());

        	System.AssertNotEquals(ctrl.sys.Certificate__c, null);
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Servers.getUrl(), ApexPages.CurrentPage().getUrl());
        	System__c newSys = [SELECT CompanyName__c FROM System__c WHERE Id = :ctrl.sys.Id];
        	System.AssertEquals('testing', newSys.CompanyName__c);
        } 
	}

	@isTest static void test_servers() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
		tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Servers.getUrl();
		tdata.proj.Max_Number_of_Servers__c = 2;
		Server__c server = TestUtil.createServer(tdata.sys.Id);
		update tdata.proj;

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Servers.getUrl(), ApexPages.CurrentPage().getUrl());
        	ctrl.initServers();
        	

        	//add two new servers, bringing us up to 3 total
        	ctrl.addNewServer();
        	Server__c newServer = ctrl.activeServers.get(ctrl.activeServers.size()-1).server;
        	newServer.Server_License_Key__c = '123';
        	
        	ctrl.addNewServer();
        	Server__c newServer2 = ctrl.activeServers.get(ctrl.activeServers.size()-1).server;
        	newServer2.Name = 'test.com';
        	newServer2.Server_License_Key__c = '123';
        	
        	//over our limit
        	System.AssertEquals(null,ctrl.next());
        	System.Assert(TestUtil.pageMessagesContain(SLA_LicenseRequestWizardController.ERR_MAX_SERVERS_FINISH));	
        	
        	//mark the origional one inactive
        	ApexPages.currentPage().getParameters().put('deletePos','0');
        	ctrl.markServerInactive();
        	System.AssertEquals(server.Id, ctrl.inactiveServers[0].Id);
        	
        	//mising reuqired data
        	System.AssertEquals(null,ctrl.next());
        	System.Assert(TestUtil.pageMessagesContain(SLA_LicenseRequestWizardController.ERR_SERVER_MISSING_FIELDS));
        	
        	//test duplicate
        	newServer.Name = 'test.com';
        	//mising reuqired data
        	System.AssertEquals(null,ctrl.next());
        	System.Assert(TestUtil.pageMessagesContain(SLA_LicenseRequestWizardController.ERR_DUPLICATE_SERVER));

			//should be good now
       		newServer.Name = 'test2.com';
        	
        	PageReference pf = ctrl.next();
        	System.Debug('Current: ' + ApexPages.CurrentPage().getUrl());
        	System.Debug('NEXT: ' + pf.getURL());
        	Test.setCurrentPage(pf);
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_ClientDevices.getUrl(), ApexPages.CurrentPage().getUrl());
        }
	}

	@isTest static void test_clientdevices() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
		tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_ClientDevices.getUrl();
		update tdata.proj;

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_ClientDevices.getUrl(), ApexPages.CurrentPage().getUrl());
        	ctrl.initClientDevices();
        	ctrl.addNewClientDevice();
        	ApexPages.currentPage().getParameters().put('deletePos','0');
			ctrl.removeClientDevice();
        	ctrl.addNewClientDevice();
        	ctrl.clientDevices[0].device.Other_Client_Device_Name__c = 'Test';
        	ctrl.clientDevices[0].device.Client_Device_Solutions__c = 'SO';
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Review.getUrl(), ApexPages.CurrentPage().getUrl());
        }
	}

	@isTest static void test_review() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
		tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Review.getUrl();
		update tdata.proj;

		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Review.getUrl(), ApexPages.CurrentPage().getUrl());
        	ctrl.initReview();
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Final.getUrl(), ApexPages.CurrentPage().getUrl());
        }
	}

	@isTest static void test_final() {
		LRWTestData tdata = new LRWTestData();
		tdata.generateTestData();
		tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Final.getUrl();
		update tdata.proj;

        Contact c = TestUtil.generateContact(tdata.acc.Id);
        c.email = TestUtil.generateRandomEmail();
        insert c;
        User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		TestUtil.createServer(tdata.sys.Id);
		PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
        	//project select
        	ctrl = new SLA_LicenseRequestWizardController();
        	ctrl.initProjectSelect();
        	ctrl.selectableProjects[0].selected = true;
        	Test.setCurrentPage(ctrl.next());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Final.getUrl(), ApexPages.CurrentPage().getUrl());
        	ctrl.initFinal();
            ctrl.selectableUsers[0].selected = true;
        	Test.setCurrentPage(ctrl.submit());
        	System.AssertEquals(Page.SLA_LicenseRequestWizard_Confirmation.getUrl(), ApexPages.CurrentPage().getUrl());
        }
	}

    @isTest static void test_support() {
        PageReference pageRef = Page.SLA_LicenseRequestWizard_Support;
        Test.setCurrentPage(pageRef);
        SLA_LicenseRequestSupportController ctrl = new SLA_LicenseRequestSupportController();
        ctrl.initSupport();
    }

    @isTest static void test_various() {
        LRWTestData tdata = new LRWTestData();
        tdata.generateTestData();
        tdata.proj.Customer_License_Wizard_Step__c = Page.SLA_LicenseRequestWizard_Final.getUrl();
        update tdata.proj;
        TestUtil.createServer(tdata.sys.Id);
        PageReference pageRef = Page.SLA_LicenseRequestWizard_ProjectSelect;
        Test.setCurrentPage(pageRef);

        SLA_LicenseRequestWizardController ctrl;
        System.RunAs(tdata.user){
            ctrl = new SLA_LicenseRequestWizardController();
            ctrl.getShowCancel();
            ctrl.cancel();
            ctrl.getVCASVersions();
            ctrl.getNumberOfSteps();
            ctrl.getCurrentStepNumber();
            PaginatedSelectList ps = ctrl.allClientDevices;
        }
    }


	//***TEST HELPERS***
 

	private class LRWTestData{
		public Account acc {get; set;}
		public Tenrox__c proj {get; set;}
		public Contact contact {get; set;}
		public User user {get; set;}
		public System__c sys {get; set;}
		public void generateTestData(){
			if(acc == null){
				acc = TestUtil.createAccount();
			}
			if(contact == null){
				contact = TestUtil.generateContact(acc.Id);
				contact.MailingCountry = 'US';
				contact.Email = TestUtil.generateRandomEmail();
				insert contact;
			}
			if(user == null){
				user = SLA_CommunityUserFactory.createUserFromContact(contact.Id, false);
			}
			if(sys == null){
				sys = TestUtil.createSystem(acc.Id,contact.Id);
			}
			if(proj == null){
				proj = TestUtil.generateProject();
				Entitlement entitl;
                if(sys != null){
                    proj.Account__c = sys.Account__c;
                    entitl = TestUtil.createEntitlement(sys.Account__c);
                }else{
                    proj.Account__c = acc.Id;
                    entitl = TestUtil.createEntitlement(acc.Id);
                }
                
				proj.System__c = sys.Id;
				
				proj.Entitlement__c = entitl.Id;
				proj.Project_Status_CC__c = SLA_ProjectHelpers.STATUS_EXECUTING;
                proj.Delivered_to_Customer__c = true;
				insert proj;
			}

			//other things
			Client_Device__c cd = new Client_Device__c(
					Name = SLA_LicenseRequestWizardController.DEFAULT_CLIENT_DEVICE,
					Generic_Model__c = true
				);
			insert cd;
		}
	}
}
//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Controller for managing beta access to a view right version

public with sharing class SLA_BetaAccessManagerController {

    @TestVisible private static final String ERR_NOT_IN_BETA = 'VCAS Version is not in beta stage!';
    @TestVisible private static final String ERR_NO_ACCOUNTS = 'No Accounts Selected!';

	private final VCAS_ViewRight_Version__c version;

    public List<List<SelectableAccount>> selectableAccounts {get; set;}

    public List<Account> selectedAccounts {get; set;}

    public Map<Account,List<SelectableUser>> selectableAccountCustomers {get; set;}

    public List<User> selectedCustomers {get; set;}

    public Boolean submitted {get; private set;}

    //potentially make this a 2 step wizard.  
    //Step 1 select the accounts
    //Step 2 select contacts from each account to eamil

    public SLA_BetaAccessManagerController(ApexPages.StandardController stdController) {
        submitted = false;
        this.version = (VCAS_ViewRight_Version__c)stdController.getRecord();
        if(version.Stage__c != SLA_VCASVersionHelper.STAGE_BETA){
            // page message
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ERR_NOT_IN_BETA));
            return;
        }

        List<Account> allAccounts = [SELECT Name FROM Account];

        //load relationships
        List<Beta_Access_Association__c> associations = [SELECT Account__c
        FROM Beta_Access_Association__c 
        WHERE VCAS_Version__c = :version.Id];
        
        
        Set<Id> selectedIds = new Set<Id>();
        for(Beta_Access_Association__c assoc : associations){
            selectedIds.add(assoc.Account__c);
        }


        selectableAccounts = new List<List<SelectableAccount>>();
        List<SelectableAccount> tempL = new List<SelectableAccount>();
        //for(Account acc : allAccounts){
        Integer accSize = allAccounts.size();
        for(Integer i = 0; i < accSize; i++){
            Account acc = allAccounts[i];
            SelectableAccount sa = new SelectableAccount();
            sa.selected = selectedIds.contains(acc.id);
            sa.accountId = acc.Id;
            sa.accountName = acc.Name;
            tempL.add(sa);
            if(tempL.size() == 1000){
                selectableAccounts.add(tempL);
                tempL = new List<SelectableAccount>();
            }
        }
        if(tempL.size() > 0){
            selectableAccounts.add(tempL);
        }
    }

    //select accounts and continue to next step
    public PageReference selectAccounts(){
        
        List<Id> accountIds = new List<Id>();
        for(List<SelectableAccount> accountList : selectableAccounts){
            for(SelectableAccount acc : accountList){
                if(acc.selected){
                    accountIds.add(acc.accountId);
                }
            }  
        }

        selectedAccounts = [SELECT Name FROM Account WHERE Id IN : accountIds];

        if(selectedAccounts.size() == 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ERR_NO_ACCOUNTS));
            return null;
        }

        //populate account/customer user data
        //[TODO: Add check for no users]
        Map<Id,Contact> contactMap = new Map<Id,Contact>([SELECT Account.Name FROM Contact WHERE AccountId IN :accountIds]);
        List<User> users = [SELECT ContactId, Name, Username, Email FROM User WHERE ContactId IN :contactMap.keySet()];

        selectableAccountCustomers = new Map<Account,List<SelectableUser>>();

        for(User u : users){
            SelectableUser su = new SelectableUser();
            su.selected = false;
            su.customer = u;
            Account a = contactMap.get(u.ContactId).Account;
            
            if(selectableAccountCustomers.containsKey(a)){
                selectableAccountCustomers.get(a).add(su);
            }else{
                selectableAccountCustomers.put(a, new List<SelectableUser>{su});
            }
        }
        return Page.SLA_BetaAccessManagerSubmit;
    }

    //public PageReference back(){
    //    return Page.SLA_BetaAccessManager;
    //}

    public PageReference backToVersion(){
        return new PageReference('/' + version.Id);
    }

    //update relationships and send email
    public PageReference submit(){
        
        Map<Id,Account> accountMap = new Map<Id,Account>(selectedAccounts);

        //get assocications that didn't get set again
        List<Beta_Access_Association__c> assocToDelete = [SELECT Id 
        FROM Beta_Access_Association__c WHERE VCAS_Version__c = :version.Id 
        AND Account__c NOT IN :accountMap.keySet()];

        //determine which are new
        List<Beta_Access_Association__c> currentAssoc = [SELECT Account__c 
        FROM Beta_Access_Association__c WHERE VCAS_Version__c = :version.Id 
        AND Account__c IN :accountMap.keySet()];

        Set<Id> currentAssocAccountIds = new Set<Id>();
        for(Beta_Access_Association__c assoc : currentAssoc){
            currentAssocAccountIds.add(assoc.Account__c);
        }

        //find new associations
        List<Beta_Access_Association__c> assocToInsert = new List<Beta_Access_Association__c>();
        for(Id accountId : accountMap.keySet()){
            if(!currentAssocAccountIds.contains(accountId)){
                Beta_Access_Association__c newAssoc = SLA_VCASVersionHelper.generateBetaAccess(accountId, version.Id);
                assocToInsert.add(newAssoc);
            }
        }

        insert assocToInsert;
        delete assocToDelete;

        //Email customers
        selectedCustomers = new List<User>();
        for(List<SelectableUser> su : selectableAccountCustomers.values()){
            for(SelectableUser u : su){
                if(u.selected){
                    selectedCustomers.add(u.customer);
                }
            }
        }

        EmailTemplate templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :Label.SLA_BetaAccessEmailTemplate LIMIT 1];
        List<Messaging.SingleEmailMessage> msgs = new List<Messaging.SingleEmailMessage>();
        
        List<Id> contactIds = new List<Id>();
        for(User u : selectedCustomers){
            contactIds.add(u.contactId);
        }

        SLA_Communities_Helpers.sendCommunityEmail(contactIds, Label.SLA_BetaAccessEmailTemplate, version.Id);

        submitted = true;
        return null;
    }

    //wrapper class to select accounts
    public class SelectableAccount{
        public Boolean selected {get; set;}
        public Id accountId {get; set;}
        public transient String accountName {get; set;}
    }

    //wrapper class to select users to email
    public class SelectableUser{
        public Boolean selected {get; set;}
        public User customer {get; set;}
    }

}
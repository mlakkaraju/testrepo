Global class touchSys implements Database.Batchable<SObject>
{
String quer;
global Database.QueryLocator start(Database.BatchableContext BC)
{
query = 'Select Id,Name from System__c';
      return Database.getQueryLocator(quer);
}
global void execute(Database.BatchableContext BC, List<System__c> allSys)
{
for(System__c s :allSys)
{
s.License_Destination__c = 'EC2';
update s;

System.debug(s.id);
}
}
global void finish(Database.BatchableContext BC)
{
}
}

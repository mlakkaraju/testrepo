public class QuoteLineItemEditRedirectController {

	private Id quoteId;
	
	public QuoteLineItemEditRedirectController(ApexPages.StandardController controller) {
		SFDC_520_QuoteLine__c quoteLine = (SFDC_520_QuoteLine__c) controller.getRecord();
		quoteId = quoteLine.Quote__c;
	}
	
	public PageReference redirect() {
		PageReference redirect = Page.editQuoteLines;
		redirect.getParameters().put('id', quoteId);
		return redirect;
	}
	
	@isTest
	private static void testQuoteLineItemEditRedirect() {
		// create some test data
		SFDC_520_QuoteLine__c quoteLine = new SFDC_520_QuoteLine__c();
		Id quoteId = Schema.SObjectType.SFDC_520_Quote__c.getKeyPrefix() + '000000000000';
		quoteLine.quote__c = quoteId;
		
		// load redirect controller
		ApexPages.StandardController controller = new ApexPages.StandardController(quoteLine);
		QuoteLineItemEditRedirectController extension = new QuoteLineItemEditRedirectController(controller);
		
		// redirect and validate url
		PageReference redirect = extension.redirect();
		system.assertNotEquals(null, redirect);
		system.assert(redirect.getUrl().contains(Page.EditQuoteLines.getUrl()));
		system.assert(redirect.getUrl().contains('id=' + quoteId));
	}

}
//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Test functions for All things software access
//    Beta Access Manager Page
//    Customer Software Access Page
//    Software Access Retriever

@isTest
private class SLA_TestSoftwareAccess {


	//--- Software Access Controller Tests---
	@isTest static void test_customer_access_controller() {
		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		//add some software (more intensly tested in other tests)
		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version;

		TestUtil.createBetaAccess(acc.id, version.id);

		PageReference pageRef = Page.SLA_CustomerSoftwareAccess;
        Test.setCurrentPage(pageRef);

        System.runAs(u) {
        	SLA_SoftwareAccessController controller = new SLA_SoftwareAccessController();
        	List<VCAS_ViewRight_Version__c> software = controller.versions;
        	System.assertEquals(software.size(), 1);
        	
        	//test download action
        	ApexPages.currentPage().getParameters().put('versionId',software[0].Id);

        	PageReference download = controller.downloadVersion();
        	System.Debug(download.getUrl());
        	System.assert(download.getUrl().contains('/apex/sla_softwaredownload'));
        	System.assertEquals(download.getParameters().get('id'), software[0].Id);
   	 	}
	}

	//--- Software Retreiver Tests---

	@isTest static void test_ga_access() {

		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		Entitlement entitl = TestUtil.generateEntitlement(acc.Id);
		entitl.Status__c = 'Current';
		insert entitl;

		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_GA;
		insert version;

		VCAS_ViewRight_Version__c version2 = TestUtil.generateVCASVersion();
		version2.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version2;

		SLA_SoftwareAccessResolver resolver = new SLA_SoftwareAccessResolver(u);
		List<VCAS_ViewRight_Version__c> software = resolver.getSoftware();

		//only the GA should show up
		System.assertEquals(software.size(), 1);
		System.assertEquals(software[0].Stage__c, SLA_VCASVersionHelper.STAGE_GA);
	}

	@isTest static void test_no_ga_access() {

		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		String invalidStatus = 'Inactive';

		Entitlement entitl = TestUtil.generateEntitlement(acc.Id);
		entitl.Status__c = invalidStatus;
		insert entitl;

		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_GA;
		insert version;

		VCAS_ViewRight_Version__c version2 = TestUtil.generateVCASVersion();
		version2.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version2;

		SLA_SoftwareAccessResolver resolver = new SLA_SoftwareAccessResolver(u);
		List<VCAS_ViewRight_Version__c> software = resolver.getSoftware();

		//Noting should show up
		System.assertEquals(software.size(), 0);
	}

	@isTest static void test_beta_access() {
		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);

		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_GA;
		insert version;

		VCAS_ViewRight_Version__c version2 = TestUtil.generateVCASVersion();
		version2.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version2;

		//give beta access links to both
		TestUtil.createBetaAccess(acc.id, version.id);
		TestUtil.createBetaAccess(acc.id, version2.id);

		SLA_SoftwareAccessResolver resolver = new SLA_SoftwareAccessResolver(u);
		List<VCAS_ViewRight_Version__c> software = resolver.getSoftware();

		//only the beta should show up
		System.assertEquals(1,software.size());
		System.assertEquals(software[0].Stage__c, SLA_VCASVersionHelper.STAGE_BETA);
	}

	@isTest static void test_access() {
		Account acc = TestUtil.CreateAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);


		Entitlement entitl = TestUtil.generateEntitlement(acc.Id);
		entitl.Status__c = 'Current';
		insert entitl;

		VCAS_ViewRight_Version__c version = TestUtil.generateVCASVersion();
		version.Stage__c = SLA_VCASVersionHelper.STAGE_GA;
		insert version;

		VCAS_ViewRight_Version__c version2 = TestUtil.generateVCASVersion();
		version2.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert version2;

		//give beta access links to beat only
		TestUtil.createBetaAccess(acc.id, version2.id);

		SLA_SoftwareAccessResolver resolver = new SLA_SoftwareAccessResolver(u);
		List<VCAS_ViewRight_Version__c> software = resolver.getSoftware();

		//only the beta should show up
		System.assertEquals(software.size(), 2);
	}

	@isTest static void test_beta_download_tos() {
		Account acc = TestUtil.CreateAccount();

		Contact	contact = TestUtil.generateContact(acc.Id);
		contact.MailingCountry = 'US';
		contact.Email = TestUtil.generateRandomEmail();
		insert contact;
		User user = SLA_CommunityUserFactory.createUserFromContact(contact.Id, false);

		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert ver;

		TestUtil.createBetaAccess(acc.id, ver.id);
		
		System.RunAs(user){
			Test.setCurrentPageReference(Page.SLA_SoftwareDownload);
			System.currentPageReference().getParameters().put('id', ver.Id);
        	SLA_SoftwareDownloadController ctrl = new SLA_SoftwareDownloadController();

        	PageReference pf = ctrl.downloadOrTerms();

        	System.Assert(pf.getUrl().contains(Page.SLA_CustomerBetaTOS.getUrl()));
		}
	}

	@isTest static void test_beta_download() {
		Account acc = TestUtil.CreateAccount();

		Contact	contact = TestUtil.generateContact(acc.Id);
		contact.MailingCountry = 'US';
		contact.Email = TestUtil.generateRandomEmail();
		insert contact;
		User user = SLA_CommunityUserFactory.createUserFromContact(contact.Id, false);

		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert ver;

		Beta_Access_Association__c baa = TestUtil.generateBetaAccess(acc.id, ver.id);
		baa.Accepted_TOS__c = true;
		insert baa;

		System.RunAs(user){
			Test.setCurrentPageReference(Page.SLA_SoftwareDownload);
			System.currentPageReference().getParameters().put('id', ver.Id);
        	SLA_SoftwareDownloadController ctrl = new SLA_SoftwareDownloadController();

        	PageReference pf = ctrl.downloadOrTerms();

        	System.assertEquals(true, ctrl.canDownload);
		}
	}

	@isTest static void test_beta_tos() {

		Account acc = TestUtil.CreateAccount();

		Contact	contact = TestUtil.generateContact(acc.Id);
		contact.MailingCountry = 'US';
		contact.Email = TestUtil.generateRandomEmail();
		insert contact;
		User user = SLA_CommunityUserFactory.createUserFromContact(contact.Id, false);

		VCAS_ViewRight_Version__c ver = TestUtil.generateVCASVersion();
		ver.Stage__c = SLA_VCASVersionHelper.STAGE_BETA;
		insert ver;

		Beta_Access_Association__c baa = TestUtil.createBetaAccess(acc.id, ver.id);

		System.RunAs(user){
			Test.setCurrentPageReference(Page.SLA_CustomerBetaTOS);
			System.currentPageReference().getParameters().put('id', ver.Id);

        	SLA_CustomerBetaTOSController ctrl = new SLA_CustomerBetaTOSController();

        	PageReference pf = ctrl.acceptTOS();

		}
		baa = [SELECT Accepted_TOS__c FROM Beta_Access_Association__c WHERE Id = :baa.Id];

        System.Assert(baa.Accepted_TOS__c);

	}
	
}
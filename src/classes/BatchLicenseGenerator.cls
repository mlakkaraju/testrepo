//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  BATCH CLASS TO GENERATE LICENSES
//      * Retrives Information from System
//      * Attempts to generate License for each Server
//      * Attempts to generate Certificate
//      * Updates LR if successful
//      * If it fails to generate license, it will recursivly retry, up to a Max number of attempts
//      * After it fails a max number of times, it will create a case.

global class BatchLicenseGenerator implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful{

    private static final String FAILURE_CASE_SUBJECT = 'License Generation Batch Failed';
    private static final String EXCEPTION_CASE_SUBJECT = 'License Generation Exception';

    private static final Integer MAX_GENERATION_ATTEMPTS = 1; //temporarily changing for test ML 4/23/2015
    private static final String CLIENT_CERT = 'MIIKkQIBAzCCClcGCSqGSIb3DQEHAaCCCkgEggpEMIIKQDCCBz8GCSqGSIb3DQEHBqCCBzAwggcsAgEAMIIHJQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIuD6y3yWd6gYCAggAgIIG+ClZylpzsK8R7+SD4qrmCYDg9rXT8nV+2+qLgitGDdNW3aNUdITq61uitAD7oI5zt9apaVoRJvOl94unG63NnX0Q1pTjU3hLkWPVM2V7GKXfBw45knqMZ2LMnWWLIzTYX6EwjMeQ8gSSESll6nTbcoybiDCDtDbQn235psdwqxw2Eb6u1gbbU/vP+jVlcHkvDJW915gA1QfhgVh0pEQzpCMR/LKH9mxi8YG+sp4q6oQHMEGb1MpgWh2RD6a1/v0KlKdMLAPBd76aeYgA8J0T1t6T1x8FNc87wEwUP4l8pbB22pBt5+7cY5iclTTRzgd8IOCbAeqO4GPtKIDsl4fKVxP5imCFM9ZtgBL9mEfCy2JHeBveN2jUiT8LYds44akO64kPJPDd/oo6zqltHn5tLJb9NA5nSL16ZAyLsQE3BHRALdV0XuORgMMUjw9hnC7uwTyF3efWw5lWV2Pdd0OzYgDpXsFwexZWVALJBJidOhBob3mkidf71GjyCigr0Dy78LTUITDf3mbM62cpIwdjQZf8/cM+zzide2tx+8q8De5sr2Z85P+lXr1NUUaO7fIo1vct+MnVGbiIU+pZjciWecYhqHqyh3Od/7XJr/pojRI14S3moise6IbSOUSsZYkFHzaaNsF8Q/lBTaMGndt2Wex6oUpv6uZsTaa4Wl1KSrrwI7Gm0MO2xs2gUQhQaGlDgfc3Fe9wutoWf+MIHub7urE+mYDALdNFdeDHYdDMAmGnGD6BJYwG/DbE4MJVIbK0uvshXbetAG33GTmLgc7C4BerswL3B4bC3ZhtBP2mCd0H7ygoa+of8ETx4mhpkmSqEcoJzpMTWM7dupCsrPXkPDVK/in86ti2Bk31qalPthpso7RL4MAXs8w00k7WGC+kkgltcBHtsziMOPYHOXcSxCX+wZnQyfvTQTyydt3Ca/S15Cb9fY4xdgSxhwiFPdDdxoh4Lw3cWQt9+XUq6Krhodot9OFf8p/sbVC3B+brPsiPH7dv1uMiRxuCI21MKpGIsujkbmY0ecQDDbLgXDlrggLh08GTBgc+EUMDMFmBC4vQAAF3YcN3+TN8qrIS5KLkeONl8RleFnag6WfBhfZGt3MK6PVuLT5sz9m/Or4O7vkYV89HwzbYAP5eX/Qz0rzZQf365pQ8vYreSi/ybkLtLgp3HdaxTLbLkaQbRFpzzTxwKjNXAypA0tDRgtoLGcAuG290wy1ECd7XU8lU0DBRA0GicvEceDWtOUTiApss2X6fkQqZgNITRPB+cCbkFlsKsZJzG+nVejKUjX5Slj3CaWmUjgsUOBLTHZLdBdIQjxH7QvcGZAqmsr1cLKtFryLottPWp5dbK7n8ht80JCHCd8u0yOBNefGh+Y30Guoqpy0Cbhp1Z4GW8RDrTKDyf/oNWQ/Zw1G86IwaUPXfA57emtwbeDrOHSiTnyBMNiZWkVzUR8ZQs63JEGSE2GdsfATr+W2ZSUwXXMgrYYY3JunfhT+a74GrNLL5gZpL76s+SqawD428/1WlZ4vtf668Imi5ZC5OgSnqSy7MMA1TKcvnxp1O50DoB/yx+Rg528GybiCYFYa9b7klXwg0//91x6YrjjouDCVR0QZocUGUHkcYg051dyMXirV5WYU83Wwce5KS/pWDKO/vyLIN05JBX6dr8ZLCD28yOTDLkjej/zyXXFPwfI7vaJ5Om0ugHgHwL7prMMljy9+iVmBb/CEgYQtWXPqJDr1342pN6CocjTKIa+ZVAqrO65PPv029EtMH8mJqVLsx06xDeSeEyiX8DQczF36w5R9G0NM6Z0WDh62pyPl1qvO2smtzpB1BgMhEwXZQp6ojdw+ZVcf26K9AwKIwiaODpermj1xgdFziiX4Rr+xgD9JKhiZhroQ1EdstyE4jb1QbgEdseGasK7EDOtjtrh0SmcHb6wwkIwoMRoFSLHwl93woMiFAVVLt7niomIVcTkNnrEl8jtny68HSPPVI9opXSCWaVFXsI5ceOCMZGB5idgRRq7gSZ60Y7aUEw1cidARpOxX3zxYYLmKFrb3+eGlU98oAi9VIHgh2ZzDnqmfnJVm7xThza2wpOJZeaYiTipLk2CgOIZBhDRu9auhhfux1LTpFIlBPoR1lno9Ij7CDbY0MgZFiBn3Lz9psigM6L6Y29rl19Em4MA6Kfxv6N6udYVsMYZMQGQjOp66ZHSyLTFn20fZCdqZfyHY3WeTXJAPauCVsSlIugD6n0+YoSDdcaTFhCvuAUN/nIuJnFJdLMAJ8Fy/y3A0hrx5EcHM/wkaimAdE6XjuAD+GEepGcXZ/wpDjhYfkmxwwX7ZO9mft7tcQvckecghxqN1C28cQiTgUZtwWpXBIIpUueswU4KPKuZeFNEBlMIIC+QYJKoZIhvcNAQcBoIIC6gSCAuYwggLiMIIC3gYLKoZIhvcNAQwKAQKgggKmMIICojAcBgoqhkiG9w0BDAEDMA4ECL/kom90+kHRAgIIAASCAoBMJUUcUW5j7YLzL4XMp9YrbD+gcxOalRQIw/YWOP/VO2fRYe/D93nluHThpbo0yVYbderyBgy/FXfylTukleamTxvgKZNAPh04EKeattG+a1Z93ZsIQbp2dcrU5bUg/cSfHHRroVj5ow0sGXCD940XSqskCBUhsTXTGO8dOtoBJFthofycxFzqLWuPxGeYIyxhjD/U8JRac+K6Vd6YWLZesvLlZukXN0ObEezjZLoEAKS0A4x7QyEGp39beDaGQ1gjYRx6XyAF0QWPao+Sg7PBD2t3KK9xYYh1iJGhsOxbfK6HsA/CnsXT0aJSPRXov9h7iokw9abq9D0ri+V6YwR+41h6w4sWYGl9eLphBmBZZk9PcsKtwCnJJazNHvecdbTn7Nr/HVcFzpNU+2h61Mw8/moNp4PBiOb22y/1VTZexU1uU7LV06Ro/WFyYScBKUwC4uoHV808+68bfYphXKyXL6B0FijhoRmwj7SPZbp0fuMrrnhaIi2mKzh2c81dR5mb+x1cs1Ui5Guo8TLQfoV50BOGBzryT0ZVXo/IDwEWdJ2t1LVKf3u5vvejDB8lCfaXKUbYVQuvmKHEWAo/Em7Tq+88AumvA9hRWoMtjQVa/M56iVGQN1sjcnG8SRKiYqVQedhW2tXJKzg/BC9yoI8SRfcFoNsKfdmBirvuWx3npkKPs5yh2C/N9khpv+QsWNmFLw3Hu8u8e+Wpwi17GmbRhl54Xk7iS1K+6/b4bEQtNGjsfVc+hoXixBqD+f+LCc0yKilHla0LF5cDP5LlwSgNvIKminOKJCJJHCAkc1YjdDAYCyiC+4+G0YbPH0L5FWpREaDOq283qF5SA8X8W1/tMSUwIwYJKoZIhvcNAQkVMRYEFKkUJFzeSXZhCcDpzZMSv2WLLZWgMDEwITAJBgUrDgMCGgUABBT3GJfNNeek4Jf3G6+lA1swZ/Wu6QQIObCbH1vXdacCAggA'; //
    private static final String CLIENT_CERT_PASSWORD = 'verimatrix'; //custom
    
    public Integer generationAttempt {get; set;}
    
    private Boolean failure = false;
    private License_Request__c lr;

    private String sysId;
    private String query; 
    private Integer countLicensesLastGeneratedOnSystem = 0;   
    private System__c sys; 

    private String xsCopyType; 
    private String xsCopyCommand = 'copy_license';
    private String xsCopyDestination;
    private String xsApiKey = Label.SLA_AWSKey;
    private String xsCompany;
    private String xsType;
    private String xsServerData;
    private String xsClientData;
    private String xsVCASVer; 
    private String xsVmxSFVer = Label.SLA_VmxSFVersion;
    private String xsRequestGUID;
    public String xsSystemExtra;

    private String xsUsername;
    private String xsPassword;
    private String xsPath;

    private Date LicenseStartDate;
    private List<Server__c> failedServers = new List<Server__c>();
    private List<Id> serverIds;
    private LicenseCertificateMasterClass2.clicgen licenseClient;

    //constructor for starting with a license request
    //lr must have system
    public BatchLicenseGenerator(License_Request__c lr){
        this.lr = lr;
        sysId = lr.System__c;
        loadSystem();
    }

    //used for regeneration
    //could also be used to selectivly only generate specific severs, but the system and LR must be pasted in as compelte
    public BatchLicenseGenerator(License_Request__c lr, System__c sys, List<Server__c> serversToGenerate){
        this.sys = sys;
        serverIds = new List<Id>();
        this.lr = lr;
        for(Server__c server : serversToGenerate){
            serverIds.add(server.Id);
        }   
    }

    private void init(){
        if(generationAttempt == null){
            generationAttempt = 0;
        }

        xsCompany = sys.CompanyName__c;        
        xsType = sys.Type__c;        
        xsServerData = sys.License_Features_Settings_Blob__c;        
        xsClientData = sys.Client_Licenses_Blob__c;        
        xsRequestGUID  = lr.Id;
        LicenseStartDate = sys.LicenseStartDate__c;
        xsVCASVer = sys.VCAS_Version__r.Version_Number__c;

        xsCopyType = sys.Copy_Type__c;
        xsCopyDestination = sys.License_Copy_Server__c;
        try{
            User u = [Select Id, FTP_Username__c, FTP_Password__c from User where id = :UserInfo.getUserId()];
            xsUsername = u.FTP_Username__c;        
            xsPassword = u.FTP_Password__c;
        }catch(Exception e){}

        xsPath = sys.Location__c;
        
        //create client
        licenseClient = new LicenseCertificateMasterClass2.clicgen();  
        licenseClient.Timeout_x = 120000;    
        licenseClient.clientCert_x = CLIENT_CERT;
        licenseClient.clientCertPasswd_x = CLIENT_CERT_PASSWORD;
        xsSystemExtra = (sys.Special_Exceptions__c!=null)?sys.Special_Exceptions__c.replace(';',','):'';

        sys.hid_Generated_with_vgen3__c = true;
        update sys;
    }

    private void loadSystem(){
        sys = [SELECT Id,
            License_Features_Settings_Blob__c,
            LicenseStartDate__c, 
            Client_Licenses_Blob__c, 
            Type__c, 
            Location__c, 
            CompanyName__c,
            LocationHostname__c, 
            New_License_Server__c, 
            Last_New_License_s_Generated_Date_Time__c, 
            Last_New_License_s_Generated_Count__c, 
            Hid_License_Blobs_Changed__c,
            VCAS_Version__r.Version_Number__c,
            License_Destination__c,
            Copy_Type__c,
            License_Copy_Server__c,
            Certificate__c,
            Special_Exceptions__c,
            MaxBroadcastChannels__c,
            LicenseEndDate__c,
            Account__c
            FROM System__c WHERE Id =:sysId];
    }

    //executes the query using Database.getQueryLocator(query) and calls
    // the execute method with the result(List of allServers in this case)
    global Database.QueryLocator start(Database.BatchableContext BC){
        try{
            init();

            System.Debug('Start Attempt: ' + generationAttempt);

            query = 'Select Id, Name, Server_License_Key__c, Success_Error_Code__c, License_Start_Date__c, Inactive__c FROM Server__c WHERE ';
            
            //we want to only generate for the servers that were passed in
            if(serverIds != null && serverIds.size() > 0){
                query += ' Id IN :serverIds';
            }else{
                query += ' System__c =: sysId';
            }
            System.Debug(query);
            return Database.getQueryLocator(query);
             
        }catch(Exception e){
            createGenerationExceptionCase(e);
            return null;
        }
    }

   //executes once for each batch
    global void execute(Database.BatchableContext BC, List<Server__c> allServers){
        if(!failure){
            try{
                //System.Debug('Execute Attempt: ' + generationAttempt);
                for(Server__c server : allServers){
                    //Make sure Server is active
                    if (server.Inactive__c != true){           
                        //Make sure license key does not already exist for this Server
                        String xsServerLicenseKey = server.Server_License_Key__c;                        
                        String xsServerName = server.Name;                        
                        Integer statusCode;
                        if(!Test.isRunningTest()){
                            //make request
                            statusCode = licenseClient.generateLicense(xsCompany,
                                                                        xsType,
                                                                        xsServerLicenseKey,
                                                                        xsServerName,
                                                                        xsServerData,
                                                                        xsClientData,
                                                                        xsUsername,
                                                                        xsPassword,                                                             
                                                                        xsPath,
                                                                        xsVCASVer,
                                                                        xsVmxSFVer,
                                                                        xsRequestGUID,
                                                                        xsAPIKey,
                                                                        xsCopyType,
                                                                        xsCopyCommand,
                                                                        xsCopyDestination,
                                                                        xsSystemExtra);

                            System.Debug('Status Code: ' + statusCode);
                        }else{ 
                            statusCode=0;
                        }

                        // Check if license was successfully generated
                        if (statusCode == 0){
                            server.License_Start_Date__c = LicenseStartDate;
                            //Set License Start Date on server with Start Date from System
                            countLicensesLastGeneratedOnSystem++;
                        }else{ //Failure. add server back to the list
                            failedServers.add(server);
                        }
                        server.Success_Error_Code__c = statusCode;
                    
                    //Otherwise, if server is inactive, clear Success/Error Code
                    }else{ 
                        server.Success_Error_Code__c = null;  
                    }
                }
                if(!Test.isRunningTest()){ 
                    update allServers;
                }
            }catch(Exception e){
                system.debug('Failure Caught');
                failure = true;
                createGenerationExceptionCase(e);
            }
        }
    }    


    global void finish(Database.BatchableContext BC){
        if(!failure){
            try{
                System.Debug('Finish Attempt: ' + generationAttempt);
                Boolean updateSystem = false;

                this.generationAttempt++;

                if (countLicensesLastGeneratedOnSystem >= 1)  {
                    sys.Last_New_License_s_Generated_Date_Time__c = datetime.now();  //Set the Last License(s) Generated Date/Time                               
                    sys.Last_New_License_s_Generated_Count__c  = countLicensesLastGeneratedOnSystem;  //Set the Number of Licenses Last Generated
                    sys.Hid_License_Blobs_Changed__c = false;   //Unmark License Blobs Changed boolean
                    
                    //use this to prevent error when calling out
                    updateSystem = true;
                }

                //there were some failures!
                //recursily try again up to a max number of attempts
                System.debug(failedServers);
                if(failedServers.size() > 0){
                    
                    //cap the number of retries
                    if(generationAttempt < MAX_GENERATION_ATTEMPTS){
                        BatchLicenseGenerator retryBatch = new BatchLicenseGenerator(lr, sys, failedServers);
                        //set the exit condition
                        retryBatch.generationAttempt = this.generationAttempt;
                        //recursivly fire the batch
                        System.Debug('retrying Batch! Recursion: ' + this.generationAttempt);
                        if (updateSystem){
                            update sys;
                            updateSystem = false;
                        }
                        Database.executeBatch(retryBatch,1);
                    }else{
                        
                        createFailureCase(SLA_LicenseRequestHelpers.STATUS_FAILED, 'Failed to generate system licenses for servers! License Request: ' + lr.Name);
                        //generation failed! maybe open a case or something
                        System.Debug('FAILURE!!!');
                    }
                }else{ //all good update license request and notify user the download is ready     
                    Integer certStatus;
                    Certificate__c cert;
                    try{
                        cert = generateCertificate();
                        certStatus = (Integer) cert.Success_Error_Code__c;
                        if(certStatus == 0){ //success
                            if(cert.Certificate_Start_Date__c == null){
                                cert.Certificate_Start_Date__c = Date.today();
                            }
                            cert.hid_Added_Server__c = false;
                            lr.Generate_Certificate__c = true;
                        }
                    }catch(Exception e){
                        certStatus = -1;
                        System.Debug(e.getMessage());
                    }
                    
                    if(certStatus == 0 || certStatus == 1){
                        //generate certificate
                        if(sys.License_Destination__c == null || sys.License_Destination__c == 'EC2'){
                            try{
                                lr.Max_Broadcast_Channels__c = sys.MaxBroadcastChannels__c;
                                lr.License_End_Date__c = sys.LicenseEndDate__c;
                                //update lr;
                                SLA_LicenseRequestHelpers.requestLicenses(lr);
                            }catch(SLA_ProjectHelpers.ProjectCompletedException e){
                                if(lr.project__c != null){
                                    SLA_ProjectHelpers.unDeliverProject(lr.project__c);
                                }
                                createFailureCase(SLA_LicenseRequestHelpers.STATUS_FAILED_EC2, 'Failed to complete project: ' + e.getMessage() + ' License Request: ' + lr.Name);
                                System.Debug('Project Complete Failed!');
                            }catch(Exception e){
                                createFailureCase(SLA_LicenseRequestHelpers.STATUS_FAILED_EC2, 'Failed to retrieve licenses from EC2! Message: ' + e.getMessage() + 'Line No:' + e.getLineNumber() + 'Stack Trace:' + e.getStackTraceString()  + ' License Request: ' + lr.Name);
                                System.Debug('License Request Failed!');
                            }
                        }else{  //generation is FTP so lets just mark it as complete
                            lr.Error_Message__c = 'DESTINATION FTP';
                            lr.Status__c = SLA_LicenseRequestHelpers.STATUS_COMPLETE;
                            update lr;
                        }
                    }else{
                        System.Debug('Certificate Failure!!! Status: '+ certStatus);
                        createFailureCase(SLA_LicenseRequestHelpers.STATUS_FAILED, 'Failed to generate certificate! License Request: ' + lr.Name + '. Cert Status: ' + certStatus);
                    }
                    if(cert != null){
                        cert.hid_Generated_with_vgen3__c = true;
                        update cert;
                    }
                }
                if (updateSystem){
                    update sys;
                }

                lr.Generation_Complete_Date__c = Datetime.now();
                System.debug('updating license request');
                update lr;
            }catch(Exception e){
                createGenerationExceptionCase(e);
            }

        }

    }

    private Certificate__c generateCertificate(){
        System.Debug('Generating Certificate');
        Certificate__c cert = [SELECT Name, 
                                  Company_Name__c, 
                                  Contact_Department__c, 
                                  Contact_Email__c, 
                                  City__c, 
                                  State__c, 
                                  Country__c, 
                                  Contact_Name_TEXT__c, 
                                  Generate_Sub_CA__c, 
                                  Keep_HumanReadable_Text__c,
                                  Validity__c,
                                  Server_Components_Blob__c,
                                  Retrieval_Location_Path__c,
                                  New_Certificate_Server__c,
                                  Retrieval_Location_Hostname__c,
                                  Hid_Added_Server__c,
                                  Certificate_Start_Date__c,
                                  Certificate_End_Date__c,
                                  Special_Exceptions__c,
                                  Hid_Import_Source__c
                                  FROM Certificate__c 
                                  WHERE Id = :sys.Certificate__c
                                  LIMIT 1];

        String xsCertCompany = cert.Company_Name__c;
        String xsDepartment = cert.Contact_Department__c;
        String xsEmail = cert.Contact_Email__c;
        String xsCity = cert.City__c;
        String xsState = cert.State__c;
        String xsCountry = cert.Country__c;
        String xsContact = cert.Contact_Name_TEXT__c;
        Boolean xbGenerateSubCA = cert.Generate_Sub_CA__c;
        Boolean xbTextMode = cert.Keep_HumanReadable_Text__c;
        Integer xiValidity = cert.Validity__c.intValue();
        String xsComponents = cert.Server_Components_Blob__c;
        String xsCertPath = cert.Retrieval_Location_Path__c;

        Boolean xblsNewServer = cert.Hid_Added_Server__c;

         
        //new fields
        Date startDate;
        if(cert.Certificate_Start_Date__c == null){
            startDate = Date.today();
        }else{
            startDate = cert.Certificate_Start_Date__c;
        }

        String xsStartDate = datetime.newInstance(startDate, Time.newInstance(0,0,0,0)).format('ddMMMyyyy');
        xsStartDate = xsStartDate.toUpperCase();
        String xsEndDate = datetime.newInstance(cert.Certificate_End_Date__c, Time.newInstance(0,0,0,0)).format('ddMMMyyyy');
        xsEndDate = xsEndDate.toUpperCase();

        System.debug(xsEndDate);
        System.debug(xsStartDate);
        String xsExtra = (cert.Special_Exceptions__c!=null)?cert.Special_Exceptions__c.replace(';',','):'';
        
        Integer xiCertBundleId = Integer.valueOf(cert.Name.replace('CERT-',''));
        String xsImportSource = cert.Hid_Import_Source__c;
        
        Integer status;
        if(!Test.isRunningTest()){
            status = licenseClient.generateCertificate(xsCertCompany,
                                                           xsDepartment,
                                                           xsEmail,
                                                           xsCity,
                                                           xsState,
                                                           xsCountry,
                                                           xsContact,
                                                           xbGenerateSubCA,
                                                           xbTextMode,
                                                           xiValidity,
                                                           xsComponents,
                                                           xsUsername,
                                                           xsPassword,                                                         
                                                           xsCertPath,
                                                           xsVCASVer, 
                                                           xsVmxSFVer,
                                                           xsRequestGUID, 
                                                           xsAPIKey, 
                                                           xsCopyType, 
                                                           xsCopyCommand, 
                                                           xsCopyDestination,
                                                           xblsNewServer, 
                                                           xiCertBundleId, 
                                                           xsImportSource,
                                                           xsStartDate,
                                                           xsEndDate,
                                                           xsExtra);
        }else{
            status = 0;
        }

        cert.Success_Error_Code__c = status;
        return cert;
    }


    private void holdProject(){
        if(lr.project__c != null){
            Tenrox__c proj = [SELECT Id from Tenrox__c WHERE ID = :lr.project__c LIMIT 1];
            SLA_ProjectHelpers.holdProject(proj);
        }
    }

    private void createGenerationExceptionCase(Exception err){
        String caseString = 'An unexpected error occured! License Request: '+ lr.Name +' ERROR:' + err.getMessage();
        System.Debug(err.getMessage() + '\n' + err.getStackTraceString());
        lr.Error_Message__c = err.getMessage() + '\n' + err.getStackTraceString();
        lr.Status__c = SLA_LicenseRequestHelpers.STATUS_FAILED;
        update lr;
        
        holdProject();

        createCase(EXCEPTION_CASE_SUBJECT, caseString);
    }

    //create a case on failure
    private void createFailureCase(String status, String description){
        createCase(FAILURE_CASE_SUBJECT, description);
        holdProject();
        lr.Status__c = status;
        lr.Error_Message__c = description;
        update lr;
    }

    private void createCase(String subject, String description){
        try{
            User currentUser = [SELECT UserName, ContactId FROM User WHERE id=:UserInfo.getUserId()];
            
            Id accountId;
            if(currentUser.ContactId != null){
                accountId = [SELECT accountId FROM Contact WHERE Id = :currentUser.ContactId].accountId;
            }

            SLA_Communities_Helpers.createCase(SLA_Communities_Helpers.LICENSE_REQUEST_CASE_TYPE, subject, description,
                accountId, currentUser.contactId, lr.Project__c, lr.System__c);
        }catch(Exception e){} // dont create cases for internal users
    }

    public class LicenseGeneratorException extends Exception{}
}
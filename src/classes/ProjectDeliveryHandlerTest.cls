/***************************************************************************
* Name: Project Delivery Handler Test
* Author:don Cassis
* Date: 5/6/2015
* Description: This class is used to test the logic related to the Project 
*                Delivery records when a transaction is completed on them.
*
****************************************************************************
*                                Change Log
****************************************************************************
* Date                  Author                  Description
*
* 5/6/2015             Brandon Cassis           Created
* 
***************************************************************************/

@isTest

private class ProjectDeliveryHandlerTest {

    //This method tests the setFieldValues method in the Handler Class.
    static testMethod void setFieldValuesTest() {
        String testFamily = 'TestFamilyValue';
        String testSubfamily = 'TestSubfamilyValue';
        Product2 testProduct = new Product2();
        testProduct.Family = testFamily;
        testProduct.Product_Subfamily__c = testSubfamily;
        testProduct.Name = 'testProduct';
        
        insert testProduct;
        
        tenrox__c testProject = new tenrox__c();
        insert testProject;
        
        Project_Deliverable__c testPD = new Project_Deliverable__c();
        testPD.Product__c = testProduct.Id;
        testPD.Project__c = testProject.Id;
        
        insert testPD;
        
        Project_Deliverable__c checktestPD = new Project_Deliverable__c();
        
        checktestPD = [select id, Product_Family__c, Product_Subfamily__c from Project_Deliverable__c where Id = :testPD.Id limit 1];
        
        System.assertEquals(testFamily, checktestPD.Product_Family__c);
        System.assertEquals(testSubfamily, checktestPD.Product_Subfamily__c);
    }

}
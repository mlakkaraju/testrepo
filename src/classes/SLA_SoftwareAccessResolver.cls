//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Class to determine VCAS_ViewRight_Version__c the user has access to download
//   For GA software they must have a "valid entitlement" (determined by Status)
//	 For BETA they must have a Beta_Access_Association__c

public without sharing class SLA_SoftwareAccessResolver {

	@TestVisible private Id accountId;

	//fields used to determine stage is valid
	@testVisible private static final Set<String> INVALID_ENTITLEMENT_STAGES = 
	new Set<String>{'Inactive','Maintenance Hold', 'Replaced'};


	private Set<Id> versionIds;

	public SLA_SoftwareAccessResolver(Id AccountId) {
		this.accountId = AccountId;
	}

	public SLA_SoftwareAccessResolver(User user) {
		try{
			this.accountId = [SELECT accountId FROM Contact WHERE Id=:user.contactId LIMIT 1].accountId;
		}catch(Exception e){
			//throw new exception
			throw new SoftwareAccessException('Failed to find an account related to this user.');
		}
	}

	//public method to load all software user has access to
	public List<VCAS_ViewRight_Version__c> getSoftware(){
		versionIds = new Set<Id>();

		//Determine GA Access
		getGaSoftware();

		//Determine Additonal Access from Beta
		getBetaSoftware();

		return queryVersions();
	}

	//returns GA software user has access rights to.
	//Account must have a valid entilement
	private void getGaSoftware(){
		Set<Id> gaVersionIds = new Set<Id>();

		if(isEntitled()){
			gaVersionIds = new Map<Id,VCAS_ViewRight_Version__c>(
			[SELECT Id
			 FROM VCAS_ViewRight_Version__c 
			 WHERE Stage__c = :SLA_VCASVersionHelper.STAGE_GA]
			)
			.keySet();
			versionIds.addAll(gaVersionIds);
		}
		
	}

	//1: Account Must have relationship to beta software
	//2: Software must still be in beta
	private void getBetaSoftware(){

		 List<Beta_Access_Association__c> betaAccess = [SELECT 
		 VCAS_Version__r.Name,
		 VCAS_Version__r.Stage__c 
		 	FROM Beta_Access_Association__c 
		 	WHERE Account__c = :accountId
		 	AND VCAS_Version__r.Stage__c = :SLA_VCASVersionHelper.STAGE_BETA];
		 for(Beta_Access_Association__c accessEntry : betaAccess){
		 	//make sure its still in beta
		 	versionIds.add(accessEntry.VCAS_Version__r.Id);
		 }
	}

	//Determines if account is entitled to GA software
	//An entitlment is valid if...???
	private Boolean isEntitled(){

		Set<Id> scId = new Map<Id,ServiceContract>(
			[SELECT Id FROM ServiceContract WHERE AccountId = :accountId]).keySet();

		List<Entitlement> entitlements = [SELECT Status__c
											FROM Entitlement 
											WHERE AccountId = :accountId
											OR ServiceContractId IN :scId];

		if(entitlements.size()==0){
			return false;
		}

		for(Entitlement e : entitlements){
			if(isEntitlementValid(e)){
				return true;
			}
		}
		return false;
	}

	private Boolean isEntitlementValid(Entitlement e){
		if(INVALID_ENTITLEMENT_STAGES.contains(e.Status__c)){
				return false;
		}
		return true;
	}

	private List<VCAS_ViewRight_Version__c> queryVersions(){
		return [SELECT Name, Stage__c, Version_Number__c
				 FROM VCAS_ViewRight_Version__c 
				 WHERE Id IN : versionIds 
				 ORDER BY CreatedDate DESC];	
	}

	public class SoftwareAccessException extends Exception{}

}
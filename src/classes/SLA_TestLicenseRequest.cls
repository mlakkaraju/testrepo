// A salesforce bug is preventing any useful assertions from being made:
// See https://developer.salesforce.com/forums/#!/feedtype=SINGLE_QUESTION_DETAIL&dc=Apex_Code_Development&criteria=OPENQUESTIONS&id=906F00000009DMlIAM
// AND http://salesforce.stackexchange.com/questions/20546/test-class-for-batch-apex-with-webservice-callout
// for more details

@isTest
private class SLA_TestLicenseRequest {
	
	@isTest static void test_request_licenses() {
		Test.startTest();
		System__c sys = TestUtil.createSystem();
		sys.License_Features_Settings_Blob__c = TestUtil.generateRandomString();
		sys.Client_Licenses_Blob__c = TestUtil.generateRandomString();
		sys.License_Destination__c = 'EC2';
		update sys;
		Server__c server1 = TestUtil.createServer(sys.id);

		License_Request__c lr = SLA_LicenseRequestHelpers.openNewRequest(sys.Id);
		System.AssertEquals(1,lr.Number_of_Licenses__c);

		System.Assert(SLA_LicenseRequestHelpers.isRecent(lr.Id));

		sys.Last_New_License_s_Generated_Count__c = 3;
		update sys;

		System.AssertEquals(false, SLA_LicenseRequestHelpers.isRecent(lr.Id));
		Test.setMock(WebServiceMock.class, new SLA_LicenseCertificateWebserviceMock());
		SLA_LicenseRequestHelpers.startLicenseGeneration(lr);
		Test.stopTest();
		
		
		System.AssertEquals(SLA_LicenseRequestHelpers.STATUS_READY, [SELECT Status__c FROM License_Request__c WHERE Id = :lr.Id LIMIT 1].Status__c);
	}

	@isTest static void test_request_licenses_retrieve() {
		System__c sys = TestUtil.createSystem();
		Contact c = TestUtil.createContact(sys.Account__c);
		License_Request__c lr = new License_Request__c(
			System__c = sys.Id,
			VCAS_Version__c = sys.VCAS_version__c,
			Number_of_Licenses__c = 2,
			Contact_1__c = c.Id
		);

		insert lr;
		
		Test.StartTest();
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
			SLA_DeliveryClient.ENDPOINT,
			SLA_TestDeliveryClient.RESP_REQUEST_SUCCESS
			);
		mockResponse.contentType = 'application/json';

		Test.setMock(HttpCalloutMock.class,mockResponse);
		SLA_LicenseRequestHelpers.requestLicenses(lr);
		Test.StopTest();
	}

	@isTest static void test_internalGeneratePage() {
		System__c sys = TestUtil.createSystem();
		sys = [SELECT Id, Certificate__r.hid_Added_Server__c, Account__c FROM System__c WHERE Id = :sys.Id];
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Contact primary = TestUtil.createContact(sys.Account__c);
		Contact secondary = TestUtil.createContact(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		proj.System__c = sys.Id;
		proj.Primary_Customer_Contact__c = primary.Id;
		proj.Secondary_Customer_Contact__c = secondary.Id;
		insert proj;

		PageReference pageRef = Page.SLA_InternalGenerateLicenses;
        Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdCtrl = new ApexPages.StandardController(sys); 

        SLA_InternalGenerateLicensesController ctrl = new SLA_InternalGenerateLicensesController(stdCtrl);

        system.assertEquals(ctrl.systemProjects[0].getValue(), proj.Id);

		
		MockHttpResponseGenerator mockResponse = new MockHttpResponseGenerator(
			SLA_DeliveryClient.ENDPOINT,
			SLA_TestDeliveryClient.RESP_REQUEST_SUCCESS
			);
		mockResponse.contentType = 'application/json';

		ctrl.lr.project__c = proj.Id;

		//test contacts get set correctly from project
		ctrl.populateContacts();
		system.assertEquals(ctrl.lr.contact_1__c, primary.Id);
		system.assertEquals(ctrl.lr.contact_2__c, secondary.Id);

		Test.setMock(HttpCalloutMock.class,mockResponse);
		Test.StartTest();
		ctrl.generate();
		Test.StopTest();
	}

	@isTest static void test_licenseAccess() {
		System__c sys = TestUtil.createSystem();
		Contact c = TestUtil.generateContact(sys.Account__c);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);

		License_Request__c lr = new License_Request__c(
			System__c = sys.Id,
			VCAS_Version__c = sys.VCAS_version__c,
			Number_of_Licenses__c = 2
		);
		License_Request__c lr2 = new License_Request__c(
			Status__c = SLA_LicenseRequestHelpers.STATUS_GENERATING,
			System__c = sys.Id,
			VCAS_Version__c = sys.VCAS_version__c,
			Number_of_Licenses__c = 2
		);
		License_Request__c lr3 = new License_Request__c(
			Status__c = SLA_LicenseRequestHelpers.STATUS_READY,
			System__c = sys.Id,
			VCAS_Version__c = sys.VCAS_version__c,
			Number_of_Licenses__c = 2
		);
		License_Request__c lr4 = new License_Request__c(
			Status__c = SLA_LicenseRequestHelpers.STATUS_COMPLETE,
			System__c = sys.Id,
			VCAS_Version__c = sys.VCAS_version__c,
			Number_of_Licenses__c = 2
		);

		insert lr;
		insert lr2;
		insert lr3;
		insert lr4;

		System.runAs(u) {
			PageReference pageRef = Page.SLA_CustomerLicenseAccess;
        	Test.setCurrentPage(pageRef);
        	SLA_CustomerLicenseAccessController ctrl = new SLA_CustomerLicenseAccessController();
		}
	}
	
}
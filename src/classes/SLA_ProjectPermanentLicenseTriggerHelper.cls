//Author: Charlie@callawaycloudconsulting
// This trigger is used to copy over the 
//  account -> Always Give Permanent License to the project -> Approved for Permanent licnese
//  when new projects are created.
//  This can be copied from either the project.account__c or the project.ship_to__c
public with sharing class SLA_ProjectPermanentLicenseTriggerHelper implements TriggerHandler.HandlerInterface{
	public void handle(){
		Map<Id,Tenrox__c> accountMap = new Map<Id,Tenrox__c>();
		Map<Id,Tenrox__c> shipToMap = new Map<Id,Tenrox__c>();
		for(Tenrox__c proj : (List<Tenrox__c>) Trigger.new){
			accountMap.put(proj.Account__c, proj);
			shipToMap.put(proj.Ship_To__c, proj);
		}
		for(Account acc : [SELECT Always_give_Permanent_Licenses__c 
							FROM Account 
							WHERE Id IN :accountMap.keySet() OR Id IN :shipToMap.keySet()]){
			
			Tenrox__c proj = null; 
			if(accountMap.containsKey(acc.Id)){
				proj = accountMap.get(acc.Id);
			}else if(shipToMap.containsKey(acc.Id)){
				proj = shipToMap.get(acc.Id);
			}

			if(acc.Always_give_Permanent_Licenses__c){
				proj.Approved_for_Permanent_Licenses__c = true;
			}
		}		
	}
}
@istest(seeAllData= true)
public class TestBatchLicenseGenRegen{
//test method for BatchLicenseGeneratorOnSystem
public static testMethod void testBatchLicenseGenerator()
{
     Test.StartTest();
     System__c s= [select ID from System__c Order By LastModifiedDate Desc Limit 1 ];
     //created a list of 13 servers to test the batch license generator and regenerator. the number can be changed to any value    
     list <Server__c> sobj= [Select Id, Name, Server_License_Key__c ,Success_Error_Code__c,License_Start_Date__c,Inactive__c  from Server__c Order By LastModifiedDate Desc limit 13];
     BatchLicenseGeneratorOnSystem b = new BatchLicenseGeneratorOnSystem(s.ID);
     ID bpid = Database.executeBatch(b);
     Test.StopTest();
     System.AssertEquals(10,10);
}
//test method for BatchLicenseRegeneratorOnSystem
public static testMethod void testBatchLicenseRegenerator()
{
     Test.StartTest();
     System__c s= [select ID from System__c Order By LastModifiedDate Desc Limit 1 ];
     list <Server__c> sobj= [Select Id, Name, Server_License_Key__c ,Success_Error_Code__c,License_Start_Date__c,Inactive__c  from Server__c Order By LastModifiedDate Desc limit 13];
     BatchLicenseRegeneratorOnSystem b = new BatchLicenseRegeneratorOnSystem(s.ID);
     ID bpid = Database.executeBatch(b);
     Test.StopTest();
     System.AssertEquals(10,10);
}

}
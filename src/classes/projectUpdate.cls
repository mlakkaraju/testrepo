/*
 * projectUpdate
 * 
 * Created by: Colin Turner ,  10/4/2010 10:22 PM
 *
 * Change log:
 * ~~~~~NAME~~~~~~DATE~~~~~~~~EDIT~~~~~~~~~
 * Todd Trevisan, 7/1/2015 -Added "Salesperson_8"
 * 
 * 
 */



public class projectUpdate
{
    public static void updateAccount(String strId)
    {
        if (strId == null || strId == '')
            return;
        Double Number_of_VCAS_SMC_Servers = 0;
        Double Number_of_RTES_Servers = 0;
        Double Number_of_VOD_Encryption_Servers = 0;
        Double Number_of_Dedicated_Database_Servers = 0;
        Double vecmg = 0;
        Double varc = 0;
        Double bcsm = 0;
        Double stbLicenses = 0;
        
        tenrox__c[] projList = [select Id,Number_of_VCAS_SMC_Servers__c,Number_of_RTES_Servers__c,Number_of_VOD_Encryption_Servers__c,Number_of_Dedicated_Database_Servers__c, Number_of_VECMG_Servers__c, Number_of_VARC_Servers__c,Number_of_BCSM_DVB_Servers__c  from tenrox__c where Account__c = :strId ];
        for (Integer intPos = 0; intPos < projList.size(); intPos++)
        {
            tenrox__c proj = projList[intPos];
            if (proj.Number_of_VCAS_SMC_Servers__c != null)
                Number_of_VCAS_SMC_Servers += proj.Number_of_VCAS_SMC_Servers__c;
            if (proj.Number_of_RTES_Servers__c != null )
                Number_of_RTES_Servers += proj.Number_of_RTES_Servers__c;
            if (proj.Number_of_VOD_Encryption_Servers__c != null)
                Number_of_VOD_Encryption_Servers += proj.Number_of_VOD_Encryption_Servers__c;
            if (proj.Number_of_Dedicated_Database_Servers__c != null )
                Number_of_Dedicated_Database_Servers += proj.Number_of_Dedicated_Database_Servers__c;
            if (proj.Number_of_VECMG_Servers__c != null)
                vecmg += proj.Number_of_VECMG_Servers__c;   
            if (proj.Number_of_VARC_Servers__c != null)
                varc += proj.Number_of_VARC_Servers__c;   
            if (proj.Number_of_BCSM_DVB_Servers__c != null)
                bcsm += proj.Number_of_BCSM_DVB_Servers__c;          }
        
        Account acct = [select id,Total_VCAS_SMC_Servers__c, Total_RTES_Servers__c, Total_VOD_Encryption_Servers__c,Total_Dedicated_Database_Servers__c,Total_VECMG_Servers__c,Total_VARC_Servers__c,Total_BCSM_DVB_Servers__c  from Account where Id = :strId limit 1];
        
        acct.Total_VCAS_SMC_Servers__c = Number_of_VCAS_SMC_Servers;
        acct.Total_RTES_Servers__c = Number_of_RTES_Servers;
        acct.Total_VOD_Encryption_Servers__c = Number_of_VOD_Encryption_Servers;
        acct.Total_Dedicated_Database_Servers__c = Number_of_Dedicated_Database_Servers;
        acct.Total_VECMG_Servers__c = vecmg;
        acct.Total_VARC_Servers__c = varc;
        acct.Total_BCSM_DVB_Servers__c = bcsm;
        update acct;
    }

    public static void updateMetaComment(String strOppId, String strProjectId)
    {
        String crlf = '\n';
        for (tenrox__c projRec: [select Id, Status_Comments__c, Total_Comments__c, Opportunity__r.description,Opportunity__r.Project_Manager_Comments__c, Opportunity__r.Legal_Comments__c from tenrox__c where Id = :strProjectId OR Opportunity__c = :strOppId] )
        {
            String totalComment = '';
            if (projRec.Status_Comments__c != null)
                totalComment += 'Project Status Comments : ' + projRec.Status_Comments__c  + crlf;
            //if (projRec.Opportunity__r.description != null ) 
            //    totalComment += 'Opportunity Description : ' + projRec.Opportunity__r.description   + crlf;
            if (projRec.Opportunity__r.Project_Manager_Comments__c != null ) 
                totalComment += 'Project Manager Comments : ' + projRec.Opportunity__r.Project_Manager_Comments__c   + crlf;
            if (projRec.Opportunity__r.Legal_Comments__c != null )
                totalComment += 'Legal Comments : ' + projRec.Opportunity__r.Legal_Comments__c;
                
            projRec.Total_Comments__c = totalComment;
            update(projRec);
        }   
    }
    
    public static String ifNull(String input)
    {
        if (input == null)
            return '';
        else
            return input;
    }    

    public static void updateEstContractComplete(String strId, Date complDate)
    {
        tenrox__c[] projList = [select Id, Estimated_Contract_Complete_Date__c from tenrox__c where Account__c = :strId ];
        for (Integer intPos = 0; intPos < projList.size(); intPos++)
        {
            tenrox__c proj = projList[intPos];
            proj.Estimated_Contract_Complete_Date__c = complDate;
            update proj;
        }
    }


    public static void createOrUpdateRevenue(String strType, String strProjectId)
    {
        Double Sum_Revenue_Amount = 0;
        tenrox__c projRec = [select Id, Forecast_Project_Close_Acceptance_Date__c, Project_Total_Amount__c from tenrox__c where Id = :strProjectId];
        Project_Deliverable__c[] projDelivList = [select project__c, Amount__c from Project_Deliverable__c where project__c = :strProjectId];
        Project_Revenue_Schedule__c[] projRevList = [select Project__c, Forecast_Revenue_Date__c, Forecast_Revenue_Date_2__c, Revenue_Amount__c from Project_Revenue_Schedule__c where project__c = :strProjectId];
        
        if (strType == 'DELIVERABLE')
        {

          for (Integer intPos = 0; intPos < projDelivList.size(); intPos++)
          {
              Project_Deliverable__c delivRec = projDelivList[intPos];
              if (delivRec.Amount__c != null)
              {
                Sum_Revenue_Amount = Sum_Revenue_Amount + delivRec.Amount__c;
              }
          }
            projRec.Project_Total_Amount__c = Sum_Revenue_Amount;
            update(projRec);
        }

        if (projRevList.size() > 1)
        {
            return;
        }

        if (strType == 'DELIVERABLE')
        {
            if (projRevList.size() == 0)
            {
                Project_Revenue_Schedule__c revRec = new Project_Revenue_Schedule__c();
                revRec.Project__c = strProjectId;
//                revRec.Forecast_Revenue_Date__c = projRec.Forecast_Project_Close_Acceptance_Date__c;
                revRec.Percent_Completed__c = 100;
                insert(revRec);
                return;
            }
        }

        for (Integer intPos = 0; intPos < projRevList.size(); intPos++)
        {
            Project_Revenue_Schedule__c revRec = projRevList[intPos];
            if (strType == 'PROJECT')
            {
//               revRec.Forecast_Revenue_Date__c   = projRec.Forecast_Project_Close_Acceptance_Date__c;
//               update(revRec);
            }
           
        }
    }  
    
    public static void updateOpportunity(String strId, String strProjectId)
    {
        if (strId != null && strProjectId != null)
        {
            Opportunity opp = [select id,Project__c  from Opportunity where Id = :strId limit 1];

            opp.Project__c = strProjectId;
            update opp;
        }
    }    

    public static void createCommissionRecords(String strOppId)
    {
        List<Commission_Entry__c> insCommission = new List<Commission_Entry__c>();
        for (Opportunity oppRec: [select Id, Maintenance_Only_Order__c, Salesperson_1__c, Salesperson_2__c, Salesperson_3__c, Salesperson_4__c, Salesperson_5__c, Salesperson_6__c, Salesperson_7__c, Salesperson_8__c From Opportunity where Id = :strOppId] )
        {
                if (oppRec.Salesperson_1__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_1__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);
                    
                    system.debug('Sales Person 1: ' + oppRec.Salesperson_1__c);
                    system.debug('%age: ' + 100);
                }
                
                if (oppRec.Salesperson_2__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_2__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 2: ' + oppRec.Salesperson_2__c);
                    system.debug('%age: ' + 100);

                }

                if (oppRec.Salesperson_3__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_3__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 3: ' + oppRec.Salesperson_3__c);
                    system.debug('%age: ' + 100);

                }
                
                if (oppRec.Salesperson_4__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_4__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 4: ' + oppRec.Salesperson_4__c);
                    system.debug('%age: ' + 100);
                    
                }

                if (oppRec.Salesperson_5__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_5__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 5: ' + oppRec.Salesperson_5__c);
                    system.debug('%age: ' + 100);

                }
                if (oppRec.Salesperson_6__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_6__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 6: ' + oppRec.Salesperson_6__c);
                    system.debug('%age: ' + 100);

                }
                
                if (oppRec.Salesperson_7__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_7__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 7: ' + oppRec.Salesperson_7__c);
                    system.debug('%age: ' + 100);

                }
                
                if (oppRec.Salesperson_8__c != null)
                {
                    Commission_Entry__c commissionRec = new Commission_Entry__c();
                    
                    commissionRec.Commissionee__c = oppRec.Salesperson_8__c;
                    commissionRec.Percent_Split_del__c = 100;
                    commissionRec.Opportunity__c = oppRec.Id;
                    commissionRec.Maintenance_Only_Order__c = oppRec.Maintenance_Only_Order__c;
                    insCommission.add(commissionRec);

                    system.debug('Sales Person 8: ' + oppRec.Salesperson_8__c);
                    system.debug('%age: ' + 100);

                }
        }   
        if (insCommission.size() > 0)
        {
            system.debug('No. of Commission records:' + insCommission.size());
            insert(insCommission);
        }
    }
}
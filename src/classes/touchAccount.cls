Global class touchAccount implements Database.Batchable<sObject>
{
String query;
global Database.QueryLocator start(Database.BatchableContext BC)
{
query = 'Select Id, Name  from Account';
return Database.getQueryLocator(query);

}

global void execute(Database.BatchableContext BC, List<Account> allAccs)
{
for(Account a : allAccs)
{
update a;
}
}

global void finish(Database.BatchableContext BC)
{

}

}
//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Class that helps to retrieve projects and systems that a customer is "entitled" too
public class SLA_EntitledAccessRetriever {
    @TestVisible private Id accountId;
    private Set<Id> systemIds;
    private Set<Id> projectIds;


    public SLA_EntitledAccessRetriever(Id AccountId) {
        this.accountId = AccountId;
        init();
    }

    public SLA_EntitledAccessRetriever(User user) {
        try{
            this.accountId = [SELECT accountId FROM Contact WHERE Id=:user.contactId LIMIT 1].accountId;
            init();
        }catch(Exception e){
            //throw new exception
            throw new EntitledAccessException('Failed to find a contact related to this user.');
        }
    }

    private void init(){
        systemIds = new Set<Id>();
        projectIds = new Set<Id>();
        Set<Id> contractIds = new Map<ID, ServiceContract>([SELECT id FROM ServiceContract WHERE AccountId=:accountId]).keySet();
        findByContracts(contractIds);

        //it seems to be possible for an account to have entitlements but no service contract...
        //this means that we probably need to check the base entitlements object as well,
        //(it might turn out that these only need to be checked if no contracts are found)
        Set<Id> entitlementIds = new Map<ID, Entitlement>([SELECT id FROM Entitlement WHERE AccountId=:accountId]).keySet();
        findByEntitlements(entitlementIds);

        findByProject();
    }

    //searches for any systems the Account should have access to
    public List<System__c> getSystems(){
        return querySystems();
    }

    public Set<Id> getSystemIdsByEntitlement(String entitlementId){
        Set<Id> systems = new Set<Id>();
        if(entitlementId != null){
            List<Tenrox__c> projects = [SELECT system__c FROM Tenrox__c WHERE Entitlement__c = :entitlementId];
            for(Tenrox__c project : projects){
                systems.add(project.system__c);
            }
        }
        return systems;
    }

    public Set<Id> getAllSystemIds(){
        return systemIds;
    }

    //searches for any systems the Account should have access to
    public Set<Id> getProjectIds(){
        return projectIds;
    }

    private void findByContracts(Set<Id> contractIds){
        Set<Id> entitlementIds = new Map<ID, Entitlement>([SELECT id FROM Entitlement WHERE ServiceContractId IN :contractIds]).keySet();
        findByEntitlements(entitlementIds);
    }

    private void findByEntitlements(Set<Id> entitlementIds){
        List<Tenrox__c> projects = [SELECT system__c FROM Tenrox__c WHERE Entitlement__c in :entitlementIds];
        for(Tenrox__c project : projects){
            projectIds.add(project.Id);
            if(project.system__c != null){
                systemIds.add(project.system__c);
            }
        }
    }

    public void findByProject(){
        List<Tenrox__c> projects = [SELECT System__c FROM Tenrox__c WHERE Account__c =: accountId OR Ship_To__c = :accountId];
        for(Tenrox__c project : projects){
            projectIds.add(project.Id);
            systemIds.add(project.system__c);
        }
    }


    //helper to get fields we care about
    private List<System__c> querySystems(){
        return [SELECT Name,
            Hid_System_Unique_ID__c,
            MaxBroadcastChannels__c,
            LicenseEndDate__c, 
                (SELECT
                Name, 
                Download_URL__c,
                Expired_On_Date__c,
                Generation_Complete_Date__c,
                Generation_Request_Date__c,
                VCAS_Version__c,
                VCAS_Version__r.Name,
                License_End_Date__c,
                Max_Broadcast_Channels__c,
                System__c,
                Status__c,
                CreatedDate,
                CreatedById,
                project__r.Incremental_Clients_for_this_Project__c,
                project__r.Purchase_Order_Number__c,
                project__r.Code__c
                FROM License_Requests__r 
                ORDER BY CreatedDate DESC) 
            FROM System__c 
            WHERE Id IN :systemIds
            OR Account__c = :accountId
            ORDER BY LastModifiedDate DESC
            ];
    }

    public class EntitledAccessException extends Exception{}
}
public with sharing class PopularTags {

	public Integer tagCount { get; set; }
	
	public List<TagWrapper> popularTags {
		get {
			if(null == popularTags) {
				
				popularTags = new List<TagWrapper>();
				
				// find work spaces user has access to
				Set<Id> workspaceIds = new Set<Id>();
				for(ContentWorkspace workspace : [select id from ContentWorkspace]) {
					workspaceIds.add(workspace.id);
				}				
				
				// bail if user doesn't have access to any workspaces
				if(workspaceIds.isEmpty())
					return popularTags;
				
				// query top tag count
				for(AggregateResult tagSummary : Database.query(buildTagQuery(workspaceIds))) {
					TagWrapper popularTag = new TagWrapper();
					popularTag.cnt = (Integer) tagSummary.get('cnt');
					popularTag.tag = (String) tagSummary.get('name');
					popularTags.add(popularTag);
				}
			}
			return popularTags;
		}
		private set;
	}

	private String buildTagQuery(Set<Id> workspaceIds) {
		String query = 'select count(id) cnt, name from TagIndex__c where ';
		String workspaceWhereClause = '';
		for(Id workspaceId : workspaceIds) {
			workspaceWhereClause += 'or workspaceIds__c includes (\'' + workspaceId + '\') ';
		}
		query += workspaceWhereClause.subString(3);
		query += ' group by name order by count(id) desc limit ' + tagCount;
		return query;
	}

	public class TagWrapper {
		public String tag { get; set; }
		public Integer cnt { get; set; }
	}
	
	@isTest
	private static void basicPopularTagTest() {
		PopularTags controller = new PopularTags();
		controller.tagCount = 5;
		List<TagWrapper> popularTags = controller.popularTags;
	}

}
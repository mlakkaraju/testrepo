global class TagIndexerScheduler implements Schedulable {

	global void execute(SchedulableContext SC) {
		TagIndexerBatchUpdateTagIndex indexUpdater = new TagIndexerBatchUpdateTagIndex();
		Database.executeBatch(indexUpdater);
		TagIndexerBatchClearOldIndices indexCleaner = new TagIndexerBatchClearOldIndices();
		Database.executeBatch(indexCleaner);
	}
	
	@isTest
	private static void basicTagIndexerSchedulerTest() {
		Test.startTest();
		String cronExpression = '0 0 0 3 9 ? 2022';
		System.schedule('testing', cronExpression, new TagIndexerScheduler());
		Test.stopTest();
	}
}
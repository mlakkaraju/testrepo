global class ServerIterableWrapper implements Iterable<Server__c>{
    List<Server__c> sys {get; set;} 
    
    public ServerIterableWrapper()
    {}
  public ServerIterableWrapper(List<Server__c> s)
  {
   sys=s;
   System.debug(sys);
   System.debug(s);
  }
  
  global Iterator<Server__c> Iterator()
  {
      return new ServerIterable(sys);
   }
  

}
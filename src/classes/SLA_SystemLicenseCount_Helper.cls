//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Class that handles moving/updating license counts from the project oppertunity over to the system

public without sharing class SLA_SystemLicenseCount_Helper {
    public static final Date MAX_DATE = date.newInstance(2037, 12, 31);
    private static final String SYNC_LICENSE_COUNT_COMMENT = 'NOTE: This system was given extra generic OTT licenses due to the grouping limitations of pre-3.4 VCAS. When upgrading to VCAS 3.4+: add all devices to Group 1, match the Generic license count from Account, and notify customer.';
    private static final String GROUP_1 = 'Group 1';
    private static final String GROUP_2 = 'Group 2';
    private static final Integer TEMP_LICENSE_ADD_DAYS = 90;

    private Tenrox__c project;
    private System__c sys;
    private Opportunity opp;

    //map used to prevent system and oppertunity license count conflicts
    private static final Map<String,String> conflictMap = new Map<String,String>{
        'Number_of_Android_Clients__c'=>'DeviceGroupWebAndroid__c',
        'Number_of_Web_Mac_Clients__c'=>'DeviceGroupWebMac__c',
        'Number_of_Web_PC_Clients__c'=>'DeviceGroupWebPC__c',
        'Number_of_Web_STBs__c'=>'DeviceGroupWebSTB__c',
        'Number_of_iOS_Clients__c'=> 'DeviceGroupiPhone__c',
        'hid_Number_of_Gateway__c'=> 'DeviceGroupWebGateway__c'
    };

    //returns a list of systems that will not cause 
    //grouping errors given the current project's oppertunity
    //If an oppertunity has license counts to add, the system cannot have the corrisponding group set
    public List<System__c> removeDeviceGroupingConflicts(Id projectId, List<System__c> systems){
        List<System__c> retSystems = new List<System__c>();

        loadProject(projectId);
        if(project.Opportunity__c == null){
            return systems;
        }
        loadOpportunity(project.Opportunity__c);
        
        system.debug(opp);

        //check sys grouping fields vs oppertunity fields
        for(System__c sys : systems){
            system.debug(sys);
            Boolean canAdd = true;
            for(String oppField : conflictMap.keySet()){
                Decimal oppValue = (Decimal) opp.get(oppField);
                system.debug(oppField);
                system.debug(oppValue);
                if(oppValue != null && oppValue != 0){
                    System.debug(oppValue);
                    String sysValue = (String) sys.get(conflictMap.get(oppField));
                    if(sysValue != null){
                        canAdd = false;
                    }
                }
            }

            if(canAdd){ //no conflicts
                retSystems.add(sys);
            }
        }
        return retSystems;
    }

    //Updates license counts and dates on system
    //Returns a system with updated license information, but does not save changes
    public System__c updateLicenseCounts(Id projectId){
        loadProject(projectId);

        //get the system from the project
        sys = loadSystem(project.System__c);

        if(project.Opportunity__c != null){
            System.Debug('Opp Id: ' + project.Opportunity__c);
            //get oppertunity from project
            loadOpportunity(project.Opportunity__c);
            syncLicenseCounts();
        }
        return sys;
    }


    private void syncLicenseCounts(){
        Boolean updateLicenseDates = false;

        Integer prevLicenseCount = getTotalLicenseCount(sys);

        SLA_VCASVersionHelper.VersionNumber versionNumber = new SLA_VCASVersionHelper.VersionNumber(sys.VCAS_Version__r.Version_Number__c);

        sys.LicenseCountDesktopPC__c += opp.Number_of_PC_Players__c;
        sys.LicenseCountHybridSTB__c += opp.Number_of_DVB_STBs__c;
        sys.LicenseCountWebAndroid__c += opp.Number_of_Android_Clients__c;
        sys.LicenseCountWebMac__c += opp.Number_of_Web_Mac_Clients__c;
        sys.LicenseCountWebPC__c += opp.Number_of_Web_PC_Clients__c;
        sys.LicenseCountWebSTB__c += opp.Number_of_Web_STBs__c;
        sys.LicenseCountWebiPhone__c += opp.Number_of_iOS_Clients__c;
        if(opp.hid_Number_of_Gateway__c != null)
        {
            sys.License_Count_WebGateway__c += opp.hid_Number_of_Gateway__c;
        }
        else
        {
            sys.License_Count_WebGateway__c += 0;
        }
        
        if(sys.LicenseCountLegacy__c > 0 && sys.LicenseCountIPTVSTB__c == 0){
            sys.LicenseCountLegacy__c += opp.Number_of_IP_STBs__c;
        }else{
            sys.LicenseCountIPTVSTB__c += opp.Number_of_IP_STBs__c;
        }
        sys.MaxBroadcastChannels__c += opp.Number_of_Broadcast_Channels__c;
        sys.MaxInternetStreams__c += opp.Number_of_OTT_Channels__c;

        if(opp.Number_of_Generic_OTT_Clients__c != 0){
            if(versionNumber.compareTo(SLA_VCASVersionHelper.VERSION_3_4) >= 0){
                //add to Group 1 only && set Group 2 == 0. Set all "Device Group -" fields to "Group 1"
                sys.LicenseCountGroup1__c += opp.Number_of_Generic_OTT_Clients__c;
                sys.LicenseCountGroup2__c = 0;

                if(sys.LicenseCountWebAndroid__c == 0){
                    sys.DeviceGroupWebAndroid__c = GROUP_1;
                }
                if(sys.LicenseCountWebSTB__c == 0){
                    sys.DeviceGroupWebSTB__c = GROUP_1;
                }
                if(sys.LicenseCountWebiPhone__c == 0){
                    sys.DeviceGroupiPhone__c = GROUP_1;
                }
                if(sys.LicenseCountWebPC__c == 0){
                    sys.DeviceGroupWebPC__c = GROUP_1;
                }
                if(sys.LicenseCountWebMac__c == 0){
                    sys.DeviceGroupWebMac__c = GROUP_1;
                }

                if(versionNumber.compareTo(SLA_VCASVersionHelper.VERSION_3_6_1) >= 0){
                    if(sys.License_Count_WebGateway__c == 0){
                        sys.DeviceGroupWebGateway__c = GROUP_1;
                    }
                }

            }else{
                if(sys.LicenseCountGroup1__c > 0 || sys.LicenseCountGroup2__c > 0){
                    if(sys.LicenseCountGroup1__c > 0){
                        sys.LicenseCountGroup1__c += opp.Number_of_Generic_OTT_Clients__c;
                    }
                    if(sys.LicenseCountGroup2__c > 0){
                        sys.LicenseCountGroup2__c += opp.Number_of_Generic_OTT_Clients__c;
                    }
                }else{ //this will only happen when there are no device counts in either group 1 or 2
                    //add count to both groups (1/2) and set iOS, Android, Web STBs = 'Group 1" && Web Mac, Web PC = 'Group 2'
                    sys.LicenseCountGroup1__c += opp.Number_of_Generic_OTT_Clients__c;
                    sys.LicenseCountGroup2__c += opp.Number_of_Generic_OTT_Clients__c;
                    
                    if(sys.LicenseCountWebAndroid__c == 0){
                        sys.DeviceGroupWebAndroid__c = GROUP_1;
                    }
                    if(sys.LicenseCountWebSTB__c == 0){
                        sys.DeviceGroupWebSTB__c = GROUP_1;
                    }
                    if(sys.LicenseCountWebiPhone__c == 0){
                        sys.DeviceGroupiPhone__c = GROUP_1;
                    }
                    if(sys.LicenseCountWebPC__c == 0){
                        sys.DeviceGroupWebPC__c = GROUP_2;
                    }
                    if(sys.LicenseCountWebMac__c == 0){
                        sys.DeviceGroupWebMac__c = GROUP_2;
                    }

                    sys.System_Comments__c = SYNC_LICENSE_COUNT_COMMENT;
                }
            }
        }

        //get updated license count
        Integer newLicenseCount = getTotalLicenseCount(sys);
        if(newLicenseCount>prevLicenseCount){
            updateLicenseDates();
        }
        System.Debug('--Device groups--');
        System.Debug('DeviceGroupWebMac__c' + sys.DeviceGroupWebMac__c);
        System.Debug('DeviceGroupWebPC__c' + sys.DeviceGroupWebPC__c);
        System.Debug('DeviceGroupWebSTB__c' + sys.DeviceGroupWebSTB__c);
        System.Debug('DeviceGroupWebAndroid__c' + sys.DeviceGroupWebAndroid__c);
        System.Debug('--License Counts--');
        System.Debug('LicenseCountGroup1__c' + sys.LicenseCountGroup1__c);
        System.Debug('LicenseCountGroup2__c' + sys.LicenseCountGroup2__c);
        System.Debug('LicenseCountWebAndroid__c' + sys.LicenseCountWebAndroid__c);
    }

    //updates the license dates approapriately 
    private void updateLicenseDates(){
        sys.LicenseStartDate__c = Date.today();
        
        Boolean setMaxDate = project.Approved_for_Permanent_Licenses__c;
        sys.LicenseEndDate__c = calculateLicenseEndDate(setMaxDate, (Integer) project.Days_License_Valid__c);
    }

    //Calculates a license end date for systems
    //Boolean setMaxDate: passing true will result in the max date being used
    //Integer addDays: the number of days for TODAY to set for a temporarly licnese.
    //   license dates will end on the next Wednesday
    public static Date calculateLicenseEndDate(Boolean setMaxDate, Integer addDays){
        if(setMaxDate){
            return MAX_DATE;
        }else{
            addDays = (Integer) (addDays == null ? TEMP_LICENSE_ADD_DAYS : addDays);
            return getNextWednesday(Date.today().addDays(addDays));
        }
    }

    public void loadProject(Id projectId){
        project = [SELECT Opportunity__c, 
                            System__c, 
                            Approved_for_Permanent_Licenses__c, 
                            Days_License_Valid__c
                            FROM Tenrox__c 
                            WHERE Id =: projectId LIMIT 1];
    }

    public static System__c loadSystem(Id systemId){
        System__c sys = [SELECT 
                        Name,
                        Hid_System_Unique_ID__c, 
                        Site_ID__c,
                        CompanyName__c,
                        Certificate__c,
                        VCAS_Solution__c,
                        VCAS_Version__c,
                        VCAS_Version__r.Version_Number__c,
                        Operating_System__c,
                        Virtualized__c,
                        Database__c,
                        Database_Redundancy_Method__c,
                        Encryption_Algorithm__c,
                        VOD_Server__c,
                        Broadcast_Failover_Method__c,
                        MUX_Encoder_Streamer_picklist__c,
                        DVB_IP_Hybrid_CAS_ID__c,
                        Middleware__c,
                        Certificate__r.Name,
                        Type__c,
                        Other_Reason_for_Separate_System__c,
                        License_Destination__c,
                        Account__c,
                        Site_License__c,
                        //licensing fields
                        LicenseStartDate__c,
                        LicenseEndDate__c,
                        LicenseCountDesktopMac__c,
                        LicenseCountDesktopPC__c,
                        LicenseCountGroup1__c,
                        LicenseCountGroup2__c,
                        LicenseCountHybridSTB__c,
                        LicenseCountIPTVSTB__c,
                        LicenseCountLegacy__c,
                        LicenseCountUnknown__c,
                        LicenseCountWebAndroid__c,
                        LicenseCountWebiPhone__c,
                        LicenseCountWebMac__c,
                        LicenseCountWebPC__c,
                        LicenseCountWebSTB__c,
                        License_Count_WebGateway__c,
                        DeviceGroupWebMac__c,
                        DeviceGroupWebPC__c,
                        DeviceGroupWebAndroid__c,
                        DeviceGroupiPhone__c,
                        DeviceGroupWebSTB__c,
                        DeviceGroupWebGateway__c,
                        System_Comments__c,
                        MaxBroadcastChannels__c,
                        MaxInternetStreams__c,
                        Devices_Group_1__c,
                        Devices_Group_2__c
                        FROM System__c
                        WHERE Id = :systemId 
                        LIMIT 1];
        return sys;
    }

    private void loadOpportunity(Id opportunityId){
        opp = [SELECT Number_of_Android_Clients__c,
        Number_of_Broadcast_Channels__c,
        Number_of_Commissions__c,
        Number_of_Connected_TVs__c,
        Number_of_DVB_STBs__c,
        Number_of_Generic_OTT_Clients__c,
        Number_of_HD_Channels__c,
        Number_of_IP_DVB_STBs__c,
        Number_of_IP_STBs__c,
        Number_of_MR_PlayReady_HE_Licenses__c,
        Number_of_OTT_Channels__c,
        Number_of_OTT_Clients__c,
        Number_of_PC_Players__c,
        Number_of_SD_Channels__c,
        Number_of_Web_Mac_Clients__c,
        Number_of_Web_PC_Clients__c,
        Number_of_Web_STBs__c,
        Number_of_iOS_Clients__c,
        Hid_Number_of_Gateway__c
        FROM Opportunity WHERE Id = :opportunityId 
        LIMIT 1];
    }

    public static Integer getTotalLicenseCount(Id systemId){
        System__c sys = loadSystem(systemId);
        return getTotalLicenseCount(sys);
    }

    private static Integer getTotalLicenseCount(System__c sys){
        Integer count = 0;
        count += (Integer) sys.LicenseCountDesktopPC__c;
        count += (Integer) sys.LicenseCountHybridSTB__c;
        count += (Integer) sys.LicenseCountWebAndroid__c;
        count += (Integer) sys.LicenseCountWebMac__c;
        count += (Integer) sys.LicenseCountWebPC__c;
        count += (Integer) sys.LicenseCountWebSTB__c;
        count += (Integer) sys.LicenseCountWebiPhone__c;
        count += (Integer) sys.LicenseCountLegacy__c;
        count += (Integer) sys.MaxBroadcastChannels__c;
        count += (Integer) sys.MaxInternetStreams__c;
        count += (Integer) sys.LicenseCountGroup1__c;
        count += (Integer) sys.LicenseCountGroup2__c;
        //[TODO]count += (Integer) sys.LicenseCountUnknown__c;

        return count;
    }


    private static final List<String> listDay = new List<String>{'Sunday' , 'Monday' , 'Tuesday' , 'Wednesday' , 'Thursday' , 'Friday', 'Saturday'};
    public static Date getNextWednesday(Date d){
        Datetime dt = datetime.newInstance(d.year(), d.month(),d.day());
        String day = dt.format('EEEE');
        Integer index;
        for(Integer i = 0; i < listday.size(); i++){
            if(listday[i] == day){
                index = i;
            }
        } 
        if(index < 3){
            d = d.toStartOfWeek();
            return d.addDays(3);
        }else if(index > 3){
            d = d.toStartOfWeek();
            return d.addDays(10);
        }
        return d;
    }
}
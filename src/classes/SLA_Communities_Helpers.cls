//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  General helpers for the self service communities

public without sharing class SLA_Communities_Helpers {

    public static final String LICENSE_REQUEST_CASE_TYPE = 'VCAS License Request';

    //generic case creation
    public static void createCase(String type, String subject, String description,
     Id accountId, Id contactId, Id projectId, Id systemId){
        Case c = new Case();
        c.AccountId = accountId;
        c.ContactId = contactId;
        c.Type = type;
        c.Upgrade_Project__c = projectId;
        c.System__c = systemId;
        c.Subject = subject; 
        c.Description = description;

        //case assignment
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.AssignmentRuleHeader.useDefaultRule = true;
        dmo.EmailHeader.triggerUserEmail = true;
        c.setOptions(dmo);
        c.IsVisibleInSelfService = true;
        insert c;
    }

    //returns the base URL of the current page.
    // EX: "/apex/mypage?hello=world" would return "/apex/mypage"
    public static String getCurrentUrlBase(){
        String currentPageUrl = ApexPages.currentPage().getUrl().toLowerCase();
        return getUrlBase(currentPageUrl);
    }

    public static String getUrlBase(String url){
        Integer qIndex = url.indexOf('?');
        if (qIndex >= 0) {
            url = url.substring(0, qIndex);
        }
        return url;
    }

    //Generic send email template
    public static void sendCommunityEmail(List<Id> contactIds, String templateName, Id objectId){
        Id templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName LIMIT 1].Id;
        List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
        for(Id contactId : contactIds){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateID(templateId);
            mail.setSenderDisplayName(Label.SLA_EmailSenderDisplayName); 
            mail.setTargetObjectId(contactId);
            mail.setWhatId(objectId);
            allmsg.add(mail);
        }
        Messaging.sendEmail(allmsg,false);
    }
    
    //Method written by mahesh
    //Assigns the activity to the project after send... Not generic!
    public static void sendCommunityEmail2(List<Id> contactIds, String templateName, Id objectId, License_Request__c lr){
        try{
            Id templateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName LIMIT 1].Id;
        
            Id primaryContact = contactIds.remove(0);
            List<Contact> contacts = [SELECT Email FROM Contact WHERE Id IN : contactIds];
            List<String> ccEmails = new List<String>();
            for(Contact c : contacts){
                ccEmails.add(c.email);
            }
            List<Messaging.SingleEmailMessage> allmsg = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTemplateID(templateId);
            mail.setSenderDisplayName(Label.SLA_EmailSenderDisplayName); 
            mail.setTargetObjectId(primaryContact);
            mail.setCcAddresses(ccEmails); 
            update lr; 
            mail.setWhatId(lr.Id);            
            allmsg.add(mail);
            Messaging.sendEmail(allmsg,false);
            
            Task t = [SELECT Id FROM Task WHERE WhatID =: lr.id limit 1];        
            t.Project__c = objectId;
            update t; 

        }catch(Exception e){}
    }
}
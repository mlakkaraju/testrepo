/*
Modified By: Ralph Callaway <ralph@callawaycloudconsulting.com>
*/
public class salesQuotes {
    
    public static final String MAINTENANCE_PRODUCT_NAME = 'Annual Maintenance Fee';
    
    public SFDC_520_Quote__c quote { get; private set;} 
    public SFDC_520_QuoteLine__c[] quoteLineItems { get; private set; }
    public SFDC_520_QuoteLine__c[] quoteLineItemsPlusZero { get; private set; }
    public Opportunity opp { get; private set; }  
    public Boolean hasContact { get { return (opp.opportunityContactRoles.size()  == 1);} }
    public Contact contact { get; private set; }  

    private ApexPages.StandardController controller;
    
      // constructor, loads the quote and any opportunity lines
    void queryQuoteLines(id id) { 
        quote = [  Select s.Opportunity__r.Pricebook2Id, Quote_Amount_rollup__c,
                    (Select Unit_Price__c, Unit_Net_Price__c, ServiceDate__c, Sales_Discount__c, 
                    Quote__c, Qty_Ordered__c, Product2__c, Product2__r.Name, Product2__r.Unlimited_Special_Discount__c,  Name,  
                    Ext_Net_Price__c, Ext_Price_tmp__c, Description__c, Id, Ext_Price__c, Product_Family_Group__c,
                    Product_Code__c, Product_Name__c, Product_Description__c, Product_Tier_Order__c,
                    Special_Discount_Price__c, Swap_Out__c  From Quote_Lines__r where Qty_Ordered__c != 0
                    order by Product_Family_Conversion_to_Number__c, Quote_Position__c, Product_Code__c, Product_Tier_Order__c), 
            s.Opportunity__r.HasOpportunityLineItem, s.Opportunity__r.Name, s.Name,
            s.Opportunity__r.Id, s.Opportunity__c, s.Quote_Reference__c  From SFDC_520_Quote__c s 
            where s.id = :id limit 1]; 
        quoteLineItems = quote.Quote_Lines__r;  
        for ( SFDC_520_QuoteLine__c ql : quoteLineItems ) { 
            ql.Ext_Price_tmp__c = ql.Ext_Net_Price__c;
            if ( ql.Sales_Discount__c == null ) ql.Sales_Discount__c = 0.0;
        }
    }
    void queryQuoteLinesPlusZero(id id) { 
        quote = [  Select t.Opportunity__r.Pricebook2Id, Quote_Amount_rollup__c,
                    (Select Unit_Price__c, Unit_Net_Price__c, ServiceDate__c, Sales_Discount__c, 
                    Quote__c, Qty_Ordered__c, Product2__c, Product2__r.Name, Product2__r.Unlimited_Special_Discount__c,  Name,  
                    Ext_Net_Price__c, Ext_Price_tmp__c, Description__c, Id, Ext_Price__c, Product_Family_Group__c,
                    Product_Code__c, Product_Name__c, Product_Description__c, Product_Tier_Order__c,
                    Special_Discount_Price__c, Swap_Out__c  From Quote_Lines__r
                    order by Product_Family_Conversion_to_Number__c, Quote_Position__c, Product_Code__c, Product_Tier_Order__c), 
            t.Opportunity__r.HasOpportunityLineItem, t.Opportunity__r.Name, t.Name,
            t.Opportunity__r.Id, t.Opportunity__c, t.Quote_Reference__c  From SFDC_520_Quote__c t 
            where t.id = :id limit 1]; 
        quoteLineItemsPlusZero = quote.Quote_Lines__r;  
        for ( SFDC_520_QuoteLine__c rl : quoteLineItems ) { 
            rl.Ext_Price_tmp__c = rl.Ext_Net_Price__c;
            if ( rl.Sales_Discount__c == null ) rl.Sales_Discount__c = 0.0;
        }
    }

    public opportunity total { get { 
        double amount = 0.0; 
        for ( SFDC_520_QuoteLine__c ql : quoteLineItems ) { 
            amount += ql.Unit_Price__c * 
                ql.Qty_Ordered__c * 
                (1-(ql.Sales_Discount__c/100)) ; 
        } 
        Opportunity ret = new Opportunity( amount= amount ); 
        return ret;
    } }
    public pagereference CopyOperation() { 
        // implement copy to and from opportunity given page parameters
        string reloadQuote = ApexPages.CurrentPage().getParameters().get('reloadQuote');
        string reloadOpp = ApexPages.CurrentPage().getParameters().get('reloadOpp');
        
        if ( reloadOpp != null ) { 
            copyLineItemsFromQuoteToOppty();        
            return opportunityPR() ;    
        }
        if ( reloadQuote != null ) { 
            copyLineItemsFromOpptyToQuote( ); 
            return quotePR(); 
        }
        return null;
    }
    public pagereference recalc() {
         
        for ( SFDC_520_QuoteLine__c ql : quoteLineItems ) { 
            if( ql.Special_Discount_Price__c == null )
                ql.Ext_Price_tmp__c = ql.Unit_Price__c * 
                ql.Qty_Ordered__c * 
                (1-(ql.Sales_Discount__c/100)) ;
                
            if( ql.Special_Discount_Price__c != null )
                ql.Ext_Price_tmp__c = ql.Special_Discount_Price__c * 
                ql.Qty_Ordered__c ;
            
            if( ql.Special_Discount_Price__c != null && !ql.Product2__r.Unlimited_Special_Discount__c)
                ql.Sales_Discount__c = (1-( ql.Special_Discount_Price__c / ql.Unit_Price__c))*100 ;
                
            // see if this is a new line, if so then, fill in a bit more info 
            if ( ql.Product2__c != null && ql.product2__r == null ) { 
                ql.product2__r = [ select id, name from Product2 where id = :ql.Product2__c limit 1];
                //system.debug (    ql.product2__r );
            }       
        }
        return null;
    }
    public pagereference recalcPlusZero() {
       for ( SFDC_520_QuoteLine__c rl : quoteLineItemsPlusZero ) { 
            if( rl.Special_Discount_Price__c == null )
                rl.Ext_Price_tmp__c = rl.Unit_Price__c * 
                rl.Qty_Ordered__c * 
                (1-(rl.Sales_Discount__c/100)) ;
                
            if( rl.Special_Discount_Price__c != null )
                rl.Ext_Price_tmp__c = rl.Special_Discount_Price__c * 
                rl.Qty_Ordered__c ;
            
            if( rl.Special_Discount_Price__c != null && !rl.Product2__r.Unlimited_Special_Discount__c)
                rl.Sales_Discount__c = (1-( rl.Special_Discount_Price__c / rl.Unit_Price__c))*100 ;
                
            // see if this is a new line, if so then, fill in a bit more info 
            if ( rl.Product2__c != null && rl.product2__r == null ) { 
                rl.product2__r = [ select id, name from Product2 where id = :rl.Product2__c limit 1];
                //system.debug (    rl.product2__r );
            }       
        }
        return null;
    }
    id quoteid; 
    public salesQuotes() {
        quoteid = ApexPages.CurrentPage().getParameters().get('id');
        init(); 
    }
    public salesQuotes(ApexPages.StandardController c) {
        controller = c;
        quoteid = c.getRecord().id;
        init();
    } 
    
    // load up quote lines, opportunity lines, opportunity details and contact info
    void init() { 
        queryQuoteLines(quoteid);
        queryQuoteLinesPlusZero(quoteid);
        
        if ( quote.opportunity__c != null ) {
            /* Set the related opportunity with the result of the query that traverses to account for display of the name
               and down to get the primary contact role. */
            opp = [select name, account.name,
                           (select contact.name, contact.mailingStreet, contact.mailingcity, contact.mailingstate, 
                                   contact.mailingpostalcode , contact.phone
                            from opportunityContactRoles 
                            where isPrimary = true)
                    from opportunity 
                    where id = :quote.opportunity__c];
            if ( hasContact ) {
                contact = opp.opportunityContactRoles[0].contact;
            }
        }
    }

    public PageReference createMaintLine()
    {
        SFDC_520_QuoteLine__c ql;
        Product2 maintProd = [select Id from Product2 where name = :MAINTENANCE_PRODUCT_NAME];
        if (maintProd == null)
            return null;
        
        Id maintProdId = maintProd.Id; 
        SFDC_520_Quote__c q = [select Id, Name, M_S_Percentage_FORMULA__c,Quote_Amount_rollup__c from SFDC_520_Quote__c where id = :quoteid];

        Decimal maintAmt = q.Quote_Amount_rollup__c * (q.M_S_Percentage_FORMULA__c/100);
        SFDC_520_QuoteLine__c[] qlList = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :maintProdId];
        if (qlList.size() == 0)
        {
            ql = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = 1,
                Unit_Price__c = maintAmt,
                Description__c = 'Test',
                Product2__c = maintProdId
                ); 
            insert ql;
        }
        else
        {
            ql = (SFDC_520_QuoteLine__c) qlList[0];
            ql.Unit_Price__c = maintAmt ;
            update ql;
        }                    
        /* send the user back to the quote detail page */
        return controller.view();
    
    }
    
    public PageReference createSTBlines()
    {

      SFDC_520_Quote__c q = [select Id, Name, License_Type__c, X1_50_000__c, X1_10_000__c, X1_1000__c, X1000_10_000__c, X10_000_50_000__c, X50_000_100_000__c, X100_000_500_000__c, X500_000_1_000_000__c, X1_000_000__c, opportunity__r.Pricebook2.id from SFDC_520_Quote__c where id = :quoteid];

      if (q.opportunity__r.Pricebook2.id != null)
      {
        
        SFDC_520_QuoteLine__c q2;
        
        if (q.License_Type__c != 'Web for STB' && q.License_Type__c != 'Web for PC' && q.License_Type__c != 'Web for Mac' &&
                q.License_Type__c != 'Web for iPhone/iPad' && q.License_Type__c != 'Web for Android' && q.License_Type__c != 'Web for Connected TV') {
        
        Product2 STB1Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB1Prod = [select Id from Product2 where Id = '01t50000001BnuY'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB1Prod = [select Id from Product2 where Id = '01t50000000vLAt'];
        } else {
        STB1Prod = [select Id from Product2 where Id = '01t50000001Br2M'];
        }
        
        if (STB1Prod == null)
            return null;
        
        Id STB1ProdId = STB1Prod.Id;
                    
        PricebookEntry STB1pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB1ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB1pbeUnitPrice = STB1pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q2List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB1Prod.Id];
        if (q2List.size() == 0)
        {
            q2 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X1_10_000__c,
                Unit_Price__c = STB1pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB1ProdId
                ); 
        if (q2.Qty_Ordered__c != 0) insert q2;
        }
        else
        {
            q2 = (SFDC_520_QuoteLine__c) q2List[0];
            q2.Qty_Ordered__c = q.X1_10_000__c ;
            update q2;
        } 
        }
        
        SFDC_520_QuoteLine__c q3;
        
        if (q.License_Type__c != 'Web for STB' && q.License_Type__c != 'Web for PC' && q.License_Type__c != 'Web for Mac' &&
                q.License_Type__c != 'Web for iPhone/iPad' && q.License_Type__c != 'Web for Android' && q.License_Type__c != 'Web for Connected TV') {
                
        Product2 STB2Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB2Prod = [select Id from Product2 where Id = '01t50000001BrOY'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB2Prod = [select Id from Product2 where Id = '01t50000000vLBW'];
        } else {
        STB2Prod = [select Id from Product2 where Id = '01t50000001BhAU'];
        }
        
        if (STB2Prod == null)
            return null;
        
        Id STB2ProdId = STB2Prod.Id;
                    
        PricebookEntry STB2pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB2ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB2pbeUnitPrice = STB2pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q3List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB2Prod.Id];
        if (q3List.size() == 0)
        {
            q3 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X10_000_50_000__c,
                Unit_Price__c = STB2pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB2ProdId
                ); 
        if (q3.Qty_Ordered__c != 0) insert q3;
        }
        else
        {
            q3 = (SFDC_520_QuoteLine__c) q3List[0];
            q3.Qty_Ordered__c = q.X10_000_50_000__c ;
            update q3;
        } 
        }
        
        
        
        SFDC_520_QuoteLine__c q4;
        
        if (q.License_Type__c == 'Web for STB' || q.License_Type__c == 'Web for PC' || q.License_Type__c == 'Web for Mac' ||
                q.License_Type__c == 'Web for iPhone/iPad' || q.License_Type__c == 'Web for Android' || q.License_Type__c == 'Web for Connected TV') {
        
        Product2 STB3Prod;
        if (q.License_Type__c == 'Web for STB') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001Bh7z'];
        } else if (q.License_Type__c == 'Web for PC') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001BUfr'];
        } else if (q.License_Type__c == 'Web for Mac') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001BrO9'];
        } else if (q.License_Type__c == 'Web for iPhone/iPad') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001BrQP'];
        } else if (q.License_Type__c == 'Web for Android') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001BrQ5'];
        } else if (q.License_Type__c == 'Web for Connected TV') {
        STB3Prod = [select Id from Product2 where Id = '01t50000001Btcz'];
        } else {
        return null;
        }
        
        if (STB3Prod == null)
            return null;

        Id STB3ProdId = STB3Prod.Id;

        PricebookEntry STB3pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB3ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB3pbeUnitPrice = STB3pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q4List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB3Prod.Id];
        if (q4List.size() == 0)
        {
            q4 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = (q.X1_10_000__c + q.X10_000_50_000__c + q.X50_000_100_000__c),
                Unit_Price__c = STB3pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB3ProdId
                ); 
        if (q4.Qty_Ordered__c != 0)    insert q4;
        }
        else
        {
            q4 = (SFDC_520_QuoteLine__c) q4List[0];
            q4.Qty_Ordered__c = (q.X1_10_000__c + q.X10_000_50_000__c + q.X50_000_100_000__c) ;
            update q4;
        }
        }

        SFDC_520_QuoteLine__c q5;  
        
        if (q.License_Type__c != 'Web for STB' && q.License_Type__c != 'Web for PC' && q.License_Type__c != 'Web for Mac' &&
                q.License_Type__c != 'Web for iPhone/iPad' && q.License_Type__c != 'Web for Android' && q.License_Type__c != 'Web for Connected TV') {
                         
        Product2 STB4Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB4Prod = [select Id from Product2 where Id = '01t50000001Bnui'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB4Prod = [select Id from Product2 where Id = '01t50000000vLBg'];
        } else {
        STB4Prod = [select Id from Product2 where Id = '01t50000000v2uf'];
        }
                    
        if (STB4Prod == null)
            return null;
        
        Id STB4ProdId = STB4Prod.Id;

        PricebookEntry STB4pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB4ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB4pbeUnitPrice = STB4pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q5List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB4Prod.Id];
        if (q5List.size() == 0)
        {
            q5 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X50_000_100_000__c,
                Unit_Price__c = STB4pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB4ProdId
                ); 
        if (q5.Qty_Ordered__c != 0)    insert q5;
        }
        else
        {
            q5 = (SFDC_520_QuoteLine__c) q5List[0];
            q5.Qty_Ordered__c = q.X50_000_100_000__c ;
            update q5;
        } 
        }

        SFDC_520_QuoteLine__c q6;
        
        Product2 STB5Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001Bnun'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB5Prod = [select Id from Product2 where Id = '01t50000000vLBh'];
        } else if (q.License_Type__c == 'Web for STB') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001BrNu'];
        } else if (q.License_Type__c == 'Web for PC') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001BUfw'];
        } else if (q.License_Type__c == 'Web for Mac') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001BrOE'];
        } else if (q.License_Type__c == 'Web for iPhone/iPad') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001BrQU'];
        } else if (q.License_Type__c == 'Web for Android') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001BrQA'];
        } else if (q.License_Type__c == 'Web for Connected TV') {
        STB5Prod = [select Id from Product2 where Id = '01t50000001Btd4'];
        } else {
        STB5Prod = [select Id from Product2 where Id = '01t50000000v2ug'];
        }
        
        if (STB5Prod == null)
            return null;

        Id STB5ProdId = STB5Prod.Id;

        PricebookEntry STB5pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB5ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB5pbeUnitPrice = STB5pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q6List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB5Prod.Id];
        if (q6List.size() == 0)
        {
            q6 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X100_000_500_000__c,
                Unit_Price__c = STB5pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB5ProdId
                ); 
        if (q6.Qty_Ordered__c != 0)    insert q6;
        }
        else
        {
            q6 = (SFDC_520_QuoteLine__c) q6List[0];
            q6.Qty_Ordered__c = q.X100_000_500_000__c ;
            update q6;
        }

        SFDC_520_QuoteLine__c q7;
        
        Product2 STB6Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001Bnus'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB6Prod = [select Id from Product2 where Id = '01t50000000vLEI'];
        } else if (q.License_Type__c == 'Web for STB') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001BrNz'];
        } else if (q.License_Type__c == 'Web for PC') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001BUg1'];
        } else if (q.License_Type__c == 'Web for Mac') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001BrOJ'];
        } else if (q.License_Type__c == 'Web for iPhone/iPad') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001BrQZ'];
        } else if (q.License_Type__c == 'Web for Android') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001BrQF'];
        } else if (q.License_Type__c == 'Web for Connected TV') {
        STB6Prod = [select Id from Product2 where Id = '01t50000001Btd9'];
        } else {
        STB6Prod = [select Id from Product2 where Id = '01t50000000v2uh'];
        }
        
        if (STB6Prod == null)
            return null;

        Id STB6ProdId = STB6Prod.Id;
        
        PricebookEntry STB6pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB6ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB6pbeUnitPrice = STB6pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q7List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB6Prod.Id];
        if (q7List.size() == 0)
        {
            q7 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X500_000_1_000_000__c,
                Unit_Price__c = STB6pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB6ProdId
                ); 
        if (q7.Qty_Ordered__c != 0)    insert q7;
        }
        else
        {
            q7 = (SFDC_520_QuoteLine__c) q7List[0];
            q7.Qty_Ordered__c = q.X500_000_1_000_000__c ;
            update q7;
        }

        SFDC_520_QuoteLine__c q8; 
        
        Product2 STB7Prod;
        if (q.License_Type__c == 'STB DVB') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001Bnux'];
        } else if (q.License_Type__c == 'PC Player (MPEG-2&4)') {
        STB7Prod = [select Id from Product2 where Id = '01t50000000vLEN'];
        } else if (q.License_Type__c == 'Web for STB') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BrO4'];
        } else if (q.License_Type__c == 'Web for PC') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BUg6'];
        } else if (q.License_Type__c == 'Web for Mac') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BrOO'];
        } else if (q.License_Type__c == 'Web for iPhone/iPad') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BrQe'];
        } else if (q.License_Type__c == 'Web for Android') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BrQK'];
        } else if (q.License_Type__c == 'Web for Connected TV') {
        STB7Prod = [select Id from Product2 where Id = '01t50000001BtdE'];
        } else {
        STB7Prod = [select Id from Product2 where Id = '01t50000000v2ui'];
        }
        
        if (STB7Prod == null)
            return null;

        Id STB7ProdId = STB7Prod.Id;
                 
        PricebookEntry STB7pbe = [select Id,UnitPrice from PricebookEntry where Product2id= :STB7ProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];

        Decimal STB7pbeUnitPrice = STB7pbe.UnitPrice;

        SFDC_520_QuoteLine__c[] q8List = [select Id, Unit_Price__c from SFDC_520_QuoteLine__c where Quote__c = :quoteId and Product2__c = :STB7Prod.Id];
        if (q8List.size() == 0)
        {
            q8 = new SFDC_520_QuoteLine__c (
                Quote__c = quoteid,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = q.X1_000_000__c,
                Unit_Price__c = STB7pbeUnitPrice,
                Description__c = 'Automatic Calculation',
                Product2__c = STB7ProdId
                ); 
        if (q8.Qty_Ordered__c != 0)    insert q8;
        }
        else
        {
            q8 = (SFDC_520_QuoteLine__c) q8List[0];
            q8.Qty_Ordered__c = q.X1_000_000__c ;
            update q8;
        }
        }
        

                               
        /* send the user back to the quote detail page */
        return controller.view();
    }
    
    /* The action method that will generate a PDF document from the QuotePDF page and attach it to 
       the quote provided by the standard controller. Called by the action binding for the attachQuote
       page, this will do the work and take the user back to the quote detail page. */
    public PageReference attachQuote() {
        /* Get the page definition */
        PageReference pdfPage = new PageReference( '/apex/quotePDF' );
        
        /* set the quote id on the page definition */
        pdfPage.getParameters().put('id',quote.id);
        
        /* generate the pdf blob */
        //Blob pdfBlob = pdfPage.getContent();
        //Wrapping getContent in an if{} as it is null during tests -BRITTANY
        Blob pdfBlob;
        if (Test.isRunningTest()){
            pdfBlob = Blob.valueOf('Unit Test');
        }
        else{
            pdfBlob = pdfPage.getContent();
        }
        
        /* create the attachment against the quote */
        Attachment a = new Attachment(parentId = quote.id, name=quote.Quote_Reference__c + '.pdf', body = pdfBlob);
        
        /* insert the attachment */
        insert a;
        
        /* send the user back to the quote detail page */
        return controller.view();
    }
    
    // copy lines from the oppportunity to this quote
    public void copyLineItemsFromOpptyToQuote() { 
        // delete any existing quote lines
        OpportunityLineItem[] olist;
        olist = [Select Id, OpportunityId, SortOrder, PricebookEntryId, Quantity, 
            TotalPrice, UnitPrice, ListPrice, ServiceDate, Description, CreatedDate, 
            CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, 
            IsDeleted , PricebookEntry.Name, PricebookEntry.Product2id, 
            PricebookEntry.Product2.name, PricebookEntry.Product2.ProductCode
            From OpportunityLineItem 
            where OpportunityId = :quote.Opportunity__c and PricebookEntry.Product2.name != :MAINTENANCE_PRODUCT_NAME
            order by PricebookEntry.Product2.ProductCode];
        
        // create new quote lines
        SFDC_520_QuoteLine__c[] qlines = new list<SFDC_520_QuoteLine__c>(); 
        for ( OpportunityLineItem oitem : olist) {
            SFDC_520_QuoteLine__c ql = new SFDC_520_QuoteLine__c(
                Quote__c = quote.id,
                Sales_Discount__c = 0.0,
                Qty_Ordered__c = oitem.Quantity,
                Unit_Price__c = oitem.ListPrice,
                Description__c = oitem.Description,
                ServiceDate__c = oitem.ServiceDate,
                Product2__c = oitem.PricebookEntry.Product2id
                ); 
            qlines.add(ql);     
        }
        
        delete quote.Quote_Lines__r; 
        if ( qlines.size() > 0 ) {
            insert qlines;
        }
        
    }
    
    void makeQuotePrimary() { 
        sobject[] allquotes =  Database.query( 'Select  s.Opportunity__c, S.ID, s.name '+
            ' From SFDC_520_Quote__c s  where s.Opportunity__c = \''+ quote.Opportunity__c + '\' ' );
        for ( sobject so : allquotes) { 
            if ( so.get('Id') == quote.id ) {
                so.put('Primary__c',true);  
            } else { 
                so.put('Primary__c',false);     
            }
        }
        update allquotes;
    }
     
    public void copyLineItemsFromQuoteToOppty() {
    
        // find the appropriate pricebookentries for each product2 id
        List<ID> productIdList = new List<ID>();
        for (SFDC_520_QuoteLine__c q : quoteLineItems) {
            productIdList.add((id) q.get('Product2__c'));
        }
        
        List<PricebookEntry> priceBookEntries = [Select id, Product2Id, Pricebook2Id From PricebookEntry
                                                 where pricebook2id = :quote.Opportunity__r.Pricebook2Id and
                                                 product2id in :productIdList];
                                                 
        Map<Id,Id> productIdToPricebookEntryIdMap = new Map<Id,Id>();
        
        for(PricebookEntry p : pricebookEntries) {
            productIdToPricebookEntryIdMap.put(p.product2Id, p.id);
        }
        
        // clear out old opportunity line items
        id opptyId = quote.Opportunity__r.id; 
        List<OpportunityLineItem> oldLineItems = [select id 
            from OpportunityLineItem where opportunityId = :opptyId];
        delete oldLineItems;
        
        // insert new ones
        List<OpportunityLineItem> newLineItems = new List<OpportunityLineItem>();
        for ( SFDC_520_QuoteLine__c s: quoteLineItems){ 
            newLineItems.add(New OpportunityLineItem(opportunityId = opptyId,
                                                 quantity = (double) s.get('Qty_ordered__c'),
                                                 unitPrice =  (Double) s.get('Unit_Net_Price__c'),
                                                 pricebookEntryId = productIdToPricebookEntryIdMap.get((id)s.get('Product2__c')),
                                                 serviceDate = (date)s.get('serviceDate__c'),
                                                 description = (string)s.get('Description__c')
                                                 ));
        }
        system.debug(newLineItems);
        insert newLineItems;
        
//  add maintenance Opp product
      
        OpportunityLineItem oL;
        
        Product2 maintProd = [select Id from Product2 where name = :MAINTENANCE_PRODUCT_NAME];
        
        Id maintProdId = maintProd.Id; 
        SFDC_520_Quote__c q = [select Id, Name, Annual_Support_Fee__c, opportunity__r.Pricebook2.id from SFDC_520_Quote__c where id = :quoteid];
 
        PricebookEntry maintPBE = [select Id,UnitPrice from PricebookEntry where Product2id= :maintProdId and Pricebook2.id = :q.opportunity__r.Pricebook2.id];
        
        Decimal maintAmt = q.Annual_Support_Fee__c;

        if (q.Annual_Support_Fee__c > 0)
        {
            oL = new OpportunityLineItem (
                opportunityId = opptyId,
                quantity = 1,
                unitPrice = q.Annual_Support_Fee__c,
                pricebookEntryId  = maintPBE.Id
                ); 
            insert oL;
        }
//end Maint
        
        makeQuotePrimary(); // since we just overwrote the opp 
    } 
    
    PageReference opportunityPR() { return new pagereference('/'+quote.Opportunity__c); }
    PageReference quotePR() { return new pagereference('/'+quote.id); }
    public list<SFDC_520_QuoteLine__c> todelete = new list<SFDC_520_QuoteLine__c>();
    
    public PageReference del() {
        
        string delname = ApexPages.CurrentPage().getParameters().get('delname');
        system.assert( delname != null );
        integer gone = -1;
        integer i = 0; 
        
        for ( i=0; i< quoteLineItems.size(); i++ ) { 
            if (quoteLineItems[i].Name == delname) { 
                gone = i;
            }   
        }
        if ( gone >= 0) { 
            todelete.add(  quoteLineItems.remove(gone) );   
        }
        return null;
    }

    public PageReference delPlusZero() {
        
        string delname = ApexPages.CurrentPage().getParameters().get('delname');
        system.assert( delname != null );
        integer gone = -1;
        integer i = 0; 
        
        for ( i=0; i< quoteLineItemsPlusZero.size(); i++ ) { 
            if (quoteLineItemsPlusZero[i].Name == delname) { 
                gone = i;
            }   
        }
        if ( gone >= 0) { 
            todelete.add(  quoteLineItemsPlusZero.remove(gone) );   
        }
        return null;
    }
        
    public PageReference save() { 
        try { 
            
            upsert quoteLineItems;
            if ( todelete.size() > 0 ) {            
                delete todelete;    
            }
            queryQuoteLines(quote.id);
        } 
        catch ( DmlException exc) { 
             ApexPages.addMessages(exc); 
             return null;
        }
        return quotePR(); 
    }

    public PageReference savePlusZero() { 
        try { 
            
            upsert quoteLineItemsPlusZero;
            if ( todelete.size() > 0 ) {            
                delete todelete;    
            }
            queryQuoteLinesPlusZero(quote.id);
        } 
        catch ( DmlException exc) { 
             ApexPages.addMessages(exc); 
             return null;
        }
        return quotePR(); 
    }
    
    /* TODO 
     * public PageReference add() { return null;}
     */
    public PageReference add() { 
        // insert a new line, after user selects a product?
        SFDC_520_QuoteLine__c q =  new SFDC_520_QuoteLine__c( Quote__c = quote.id,
         Unit_price__c = 0.0, qty_ordered__c = 0.0, sales_discount__c = 0.0 ) ;
        quoteLineItems.add ( q );
        return null; 
    }
         
    public PageReference reset() { 
        queryQuoteLines(quote.id);
        return null;
    }
    public PageReference resetPlusZero() { 
        queryQuoteLinesPlusZero(quote.id);
        return null;
    }

    /* 
     * test methods below here 
     */
    
    static testMethod void testQuoteApp() {
      // To begin, the sample application first creates the necessary records
      // to test OpportunityLineItems: Pricebook2, Product2, and PricebookEntry
      // First it creates a product with a standard price
      // RALPH: Modified to use TestUtilProductHelper to create product test data
      TestUtilProductHelper productHelper = new TestUtilProductHelper();
      
      // To test the first example from the sample application, the test
      // method creates an opportunity line item using the pricebook entry,
      // and then asserts that the Color field was correctly copied from the
      // product after record insertion.
      // RALPH: Modified to use TestUtils to create test data
      // RALPH: No assertions were present in this code when modified in contrast
      // to what the above comment states.
      Account testAccount = TestUtil.createAccount();
      Opportunity testOppty = TestUtil.generateOpportunity(testAccount.id);
      testOppty.pricebook2Id = productHelper.pricebook.id;
      insert testOppty;
      OpportunityLineItem testLineItem = TestUtilProductHelper.createOpportunityLineItem(
          testOppty.id
        , productHelper.testPricebookEntry.id
      );
    
      // To test the fifth example from the sample application, the test method
      // creates a primary quote and then asserts that its Primary field cannot
      // be deselected. The code uses the try...catch testing idiom to assert
      // that the error message is correct.
      // RALPH: Modified to use TestUtils to create test data
      // RALPH: No assertions were present in this code when modified in contrast to 
      // what the above comment states
      SFDC_520_Quote__c testQuote = TestUtil.generateQuote(testOppty.id);
      testQuote.primary__c = true;
      insert testQuote;
    }
    
    static testmethod void t1() {
        // RALPH: Modified to use test util to create test data
        Account testAccount = TestUtil.createAccount();
        Opportunity testOppty = TestUtil.createOpportunity(testAccount.id);
        SFDC_520_Quote__c testQuote = TestUtil.createQuote(testOppty.id);
        salesQuotes s = new salesQuotes( new ApexPages.StandardController(testQuote));
    }

    public static  testMethod  void test1() {
        // RALPH: modified to create data using test utils
        TestUtilProductHelper productHelper = new TestUtilProductHelper();
        Account testAccount = TestUtil.createAccount();
        Opportunity testOppty = TestUtil.generateOpportunity(testAccount.id);
        testOppty.pricebook2id = productHelper.pricebook.id;
        insert testOppty;
        OpportunityLineItem testLineItem = TestUtilProductHelper.createOpportunityLineItem(
              testOppty.id
            , productHelper.testPricebookEntry.id
        );
        SFDC_520_Quote__c testQuote = TestUtil.createQuote(testOppty.id); 
        
        // RALPH: add maintenance product to test pricebook as code expects this product to exist
        // in oppty pricebook
        Product2 maintenanceProduct = [select id from Product2 where name = :MAINTENANCE_PRODUCT_NAME];
        PricebookEntry maintenancePBE = new PricebookEntry(
              product2Id = maintenanceProduct.id
            , pricebook2Id = productHelper.pricebook.id
            , useStandardPrice = true
            , isActive = true
            , unitPrice = 1
        );
        insert maintenancePBE;
        
        PageReference pageRef = new PageReference('/apex/editQuoteLines');
        Test.setCurrentPage(pageRef);
        system.debug( 'quote id is '+testQuote.id);
        
        ApexPages.currentPage().getParameters().put('id', testQuote.id);
        
        salesQuotes s = new salesQuotes();
        ApexPages.currentPage().getParameters().put('reloadQuote','1');
        s.CopyOperation(); 
        
        s.save(  ) ;
        system.debug ( s.total );
        
        system.assert( s.opportunityPR() != null ); 
        
        system.debug ( s.quoteLineItems );
        s.quoteLineItems = Database.query( 
            ' Select s.Unit_Price__c, s.Unit_Net_Price__c, s.ServiceDate__c, s.Ext_Net_Price__c,'+
            ' s.Sales_Discount__c, s.Quote__c, s.Qty_Ordered__c, s.Product2__c, s.Name, '+
            ' s.Id, s.Description__c, Special_Discount_Price__c  From SFDC_520_QuoteLine__c s '+
            ' where Quote__c = \''+ s.quote.Id+'\' ');
        system.debug ( s.quoteLineItems );  
        s.recalc();  
          
        system.debug ( s.quoteLineItemsPlusZero );
        s.quoteLineItemsPlusZero = Database.query( 
            ' Select s.Unit_Price__c, s.Unit_Net_Price__c, s.ServiceDate__c, s.Ext_Net_Price__c,'+
            ' s.Sales_Discount__c, s.Quote__c, s.Qty_Ordered__c, s.Product2__c, s.Name, '+
            ' s.Id, s.Description__c, Special_Discount_Price__c  From SFDC_520_QuoteLine__c s '+
            ' where Quote__c = \''+ s.quote.Id+'\' ');
        system.debug ( s.quoteLineItemsPlusZero );  
        s.recalcPlusZero();  
             
        s.copyLineItemsFromQuoteToOppty();
        ApexPages.currentPage().getParameters().put('reloadQuote','1');
        s.CopyOperation(); 
        ApexPages.currentPage().getParameters().put('reloadOpp','1');
        s.CopyOperation(); 
        
        try { s.attachQuote(); } catch( System.NullPointerException npe) { }  
        
        try { s.createMaintLine(); } catch( System.NullPointerException npe) { }  

        try { s.createSTBlines(); } catch( System.Exception e) { }   

        ApexPages.currentPage().getParameters().put('delname', s.quoteLineItems[0].name );
        s.del();
        s.save(); 
        s.reset();
        
        ApexPages.currentPage().getParameters().put('delname', s.quoteLineItems[0].name );
        s.delPlusZero();
        s.savePlusZero(); 
        s.resetPlusZero();
        
        ApexPages.CurrentPage().getParameters().put('reloadOpp',null );
        ApexPages.CurrentPage().getParameters().put('reloadQuote',null );
        salesQuotes s3 = new salesQuotes();
        system.assertEquals( null, s3.CopyOperation() ); 
    }       
}
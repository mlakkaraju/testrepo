/*
Developer: Ralph Callaway <ralph@callawaycloudconsulting.com>
Description:
	Consolidated location for simple OBJECT trigger logic.  For more complex object a separate class should be created.
Updated:
By Charlie@callawaycloudconsulting.com to use ProjectMasterTrigger and move testing to seperate class
*/
public class ProjectCreateDeliverablesTriggerHandler implements TriggerHandler.HandlerInterface {

	/* Trigger Methods */

	// When a project is created query the line items and pull them down to the project as deliverables
	public static void handle() {
		// query line items for related opportunities
		Set<Id> opptyIds = new Set<Id>();
		Map<Id, List<OpportunityLineItem>> lineItemMap = new Map<Id, List<OpportunityLineItem>>();
		for(tenrox__c project : (List<tenrox__c>)trigger.new) {
			opptyIds.add(project.opportunity__c);
		}
		for(OpportunityLineItem lineItem : [
			select 
				  description
				, opportunityId
				, pricebookEntry.product2Id
				, quantity
				, totalPrice
			from OpportunityLineItem
			where opportunityId in :opptyIds  
		]) {
			if(!lineItemMap.containsKey(lineItem.opportunityId)) {
				lineItemMap.put(lineItem.opportunityId, new List<OpportunityLineItem>());
			}
			lineItemMap.get(lineItem.opportunityId).add(lineItem);
		}
		
		// create project deliverables
		List<Project_Deliverable__c> newDeliverables = new List<Project_Deliverable__c>();
		for(tenrox__c project : (List<tenrox__c>)trigger.new) {
			if(lineItemMap.containsKey(project.opportunity__c)) {
				for(OpportunityLineItem lineItem : lineItemMap.get(project.opportunity__c)) {
					newDeliverables.add(new Project_Deliverable__c(
						  amount__c = lineItem.totalPrice
						, customer_main_project__c = project.Customer_main_Project__c
						, line_description__c = lineItem.description
						, product__c = lineItem.pricebookEntry.product2Id
						, project__c = project.id
						, quantity__c = lineItem.quantity
					));
				}
			}
		}
		insert newDeliverables;
	}
}
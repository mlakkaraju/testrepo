public class SystemBlobUtil{
    /*This class sets the License Count and Feature Settings blobs on System. 
      Last Updated: Brittany Tankhim - 10/24/13 */

    /*Set license feature settings on System*/
    public static void setLicenseFeaturesSettingsBlob(List<System__c> listSystems, Map<Id, System__c> oldMap) {
        //Identify Systems to update
        for (System__c uSystem : listSystems) {
            System__c systemBeforeUpdate = oldMap.get(uSystem.Id);  //store record information before update
            
            /*License Generator (vgen/vgen2) require a different date format than Salesforce's. Get date and reformat.*/
            
            /*License Start Date*/
            String LICENSE_START_YEAR = string.valueof(uSystem.LicenseStartDate__c.year());
            String LICENSE_START_MONTH = string.valueof(uSystem.LicenseStartDate__c.month());
            String LICENSE_START_DAY = string.valueof(uSystem.LicenseStartDate__c.day());
            
            if (LICENSE_START_MONTH.length()==1) {
                LICENSE_START_MONTH = '0'+LICENSE_START_MONTH;
            }
            
            if (LICENSE_START_DAY.length()==1) {
                LICENSE_START_DAY = '0'+LICENSE_START_DAY;
            }
            
            String startDate = LICENSE_START_YEAR + LICENSE_START_MONTH + LICENSE_START_DAY;  //set new License Start Date string
            
            /*License End Date*/
            String LICENSE_END_YEAR = string.valueof(uSystem.LicenseEndDate__c.year());
            String LICENSE_END_MONTH = string.valueof(uSystem.LicenseEndDate__c.month());
            String LICENSE_END_DAY = string.valueof(uSystem.LicenseEndDate__c.day());
            
            if ( LICENSE_END_MONTH.length()==1) {
                LICENSE_END_MONTH = '0'+LICENSE_END_MONTH;
            }
            
            if ( LICENSE_END_DAY.length()==1) {
                LICENSE_END_DAY = '0'+LICENSE_END_DAY;
            }
            
            String endDate = LICENSE_END_YEAR + LICENSE_END_MONTH + LICENSE_END_DAY;  //set new License End Date string
            
            //Set License Features Settings blob by concatenating all features
           
            uSystem.License_Features_Settings_Blob__c = //'VOD_VIDEOMARK = ' + uSystem.VODVideomark__c + '%' +    // videomark taken out of initial string, added in below 
                                                        //'BROADCAST_VIDEOMARK = ' + uSystem.BroadcastVideomark__c + '%' +
                                                        'REMOTE_ENTITLEMENT_MANAGER = ' + uSystem.RemoteEntitlementManager__c + '%' +
                                                        'ACTIVE_CERTIFICATES_COUNT = ' + (Integer)uSystem.LicenseCountLegacy__c + '%' +
                                                        'MAX_CHANNEL_COUNT = ' + (Integer)uSystem.MaxBroadcastChannels__c + '%' +
                                                        'INTERNET_STREAM_COUNT = ' + (Integer)uSystem.MaxInternetStreams__c + '%' +
                                                        'LICENSE_START_DATE = ' + startDate + '%' +
                                                        'LICENSE_END_DATE = ' + endDate + '%' +
                                                        'LICENSE_RECYCLE_PERIOD =' +(Integer)uSystem.LicenseRecyclePeriod__c*24 + '%' +
                                                        'CLONE_DETECTION = ' + uSystem.CloneDetection__c + '%' +
                                                        'ON_SCREEN_DISPLAY = ' + uSystem.OnScreenDisplay__c + '%' +
                                                        'COPY_CONTROL = ' + uSystem.CopyControl__c + '%' +
                                                        'VMPLCONF_BASE1 = ' + uSystem.VMPLCONFBASE1__c + '%' +
                                                        'VMPLCONF_RANGE1 = ' + uSystem.VMPLCONFRANGE1__c + '%' +
                                                        'VMPL_TransactionID = ' + uSystem.VMPLTransactionID__c + '%' +
                                                        'VMPLCONF_HASH = ' + uSystem.VMPLCONFHASH__c + '%' +
                                                        'VOD_ENCRYPTION = ' + uSystem.IPTVVODEncryption__c + '%' +
                                                        'VCP_ENC_KEY = ' + uSystem.VCPENCKey__c + '%' +
                                                        'SITE_ID = ' + uSystem.Site_ID__c+ '%' +
                                                        'OTT_ENABLED = ' + uSystem.OTTEnabled__c + '%' +
                                                        'MRPR_ENABLED = ' + uSystem.MRPREnabled__c + '%' +
                                                        'SOCKEM_ENABLED = ' + uSystem.SOCKEM_Enabled__c + '%' ;
           



       // Videomark enhancement checks for 3.7     
            if( decimal.valueof(uSystem.hid_Version__c.substring(0,3)) >= 3.7) {
             uSystem.License_Features_Settings_Blob__c = 'VIDEOMARK = ' + uSystem.Videomark__c + '%' + uSystem.License_Features_Settings_Blob__c; 
        } else {
             uSystem.License_Features_Settings_Blob__c =
            'VOD_VIDEOMARK = ' + uSystem.Videomark__c+ '%' +
            'BROADCAST_VIDEOMARK = ' + uSystem.Videomark__c + '%' +
                               uSystem.License_Features_Settings_Blob__c; 
         }



        if(!Test.isRunningTest())
            {if (decimal.valueOf(uSystem.hid_Version__c.subString(0,3)) >=3.6)
            {

                uSystem.License_Features_Settings_Blob__c = uSystem.License_Features_Settings_Blob__c+ 'VIIDSTR =' + uSystem.VIID__c + '%';
            }}
                                            
            if (systemBeforeUpdate.License_Features_Settings_Blob__c != uSystem.License_Features_Settings_Blob__c) {
                uSystem.hid_License_Blobs_Changed__c = true;
            }
        }
    }
    
    /*Set license counts for System*/
    public static void setLicenseCountsBlob(List<System__c> listSystems, Map<Id, System__c> oldMap) {
        //Identify Systems to update
        for (System__c uSystem : listSystems) {
            System__c systemBeforeUpdate = oldMap.get(uSystem.Id); //store record information before update
            
            /*Bundled Group licenses*/
            
            //Set Group 1 with devices if not null    
            String Group_1_Devices = uSystem.Devices_in_Group_1_TRIGGER__c;
            System.debug('Group_1_Devices =' +Group_1_Devices);
            if (Group_1_Devices != null)
            {
                Group_1_Devices ='CTIDGROUP.CTIDCLASS.Group1= '+ Group_1_Devices +'%';
            }
            else
            {
                Group_1_Devices = '';
            }
            System.debug('Group_1_Devices after if else statement =' +Group_1_Devices);
            
            
            //Set Group 2 with devices if not null
            String Group_2_Devices = uSystem.Devices_in_Group_2_TRIGGER__c;
            System.debug('Group_2_Devices =' +Group_2_Devices); 
            if (Group_2_Devices != null)
            {
                Group_2_Devices = 'CTIDGROUP.CTIDCLASS.Group2= '+ Group_2_Devices + '%';
            } 
            else
            {
                Group_2_Devices = '';
            }
             System.debug('Group_2_Devices after if else statement =' +Group_2_Devices);
             
             
            //Always set Group 3 with Legacy and IPTV STB devices   
            String Group_3_Devices = uSystem.Devices_in_Group_3_TRIGGER__c;
            Double dLicenseCountLegacy = uSystem.LicenseCountLegacy__c;
            Double dLicenseCountIPTVSTB = uSystem.LicenseCountIPTVSTB__c;
            System.debug('Group_3_Devices =' +Group_3_Devices); 
                
            if (Group_3_Devices != null)
            {
                Group_3_Devices = 'CTIDGROUP.CTIDCLASS.Group3= '+ Group_3_Devices + '%';
                dLicenseCountLegacy = 0.0;
                dLicenseCountIPTVSTB = 0.0;
            } 
            else
            {
                Group_3_Devices = '';
            }
            System.debug('Group_3_Devices after if else statement =' +Group_3_Devices);
            
            //Set Client Licenses blob by concatenating all license counts
            uSystem.Client_Licenses_Blob__c = 
                    'CTIDCLASS.IPTV.legacy = ' +(Integer)dLicenseCountLegacy +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *dLicenseCountLegacy/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*dLicenseCountLegacy/100)+',Legacy Client Devices'+'%'+ 
                    'CTIDCLASS.IPTV.unknown = '+(Integer)uSystem.LicenseCountUnknown__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountUnknown__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountUnknown__c /100)+',Unknown Devices'+'%'+
                    'CTIDCLASS.IPTV.stb_iptv = '+ (Integer)dLicenseCountIPTVSTB  +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *dLicenseCountIPTVSTB/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*dLicenseCountIPTVSTB/100)+',ViewRight STB for IPTV'+'%'+ 
                    'CTIDCLASS.IPTV.stb_hybrid = '+(Integer)uSystem.LicenseCountHybridSTB__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountHybridSTB__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountHybridSTB__c /100)+',ViewRight STB for Hybrid'+'%'+ 
                    'CTIDCLASS.IPTV.stb_dvb_sc = '+(Integer)uSystem.LicenseCountDVBSTBwSC__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountDVBSTBwSC__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountDVBSTBwSC__c /100)+',ViewRight STB for DVB with Smartcard'+'%'+
                    'CTIDCLASS.IPTV.stb_dvb_nsc = '+(Integer)uSystem.LicenseCountDVBSTBwoSC__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountDVBSTBwoSC__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountDVBSTBwoSC__c/100)+',ViewRight STB for DVB Smartcard-less'+'%'+ 
                    'CTIDCLASS.IPTV.desktop_pc = '+(Integer)uSystem.LicenseCountDesktopPC__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountDesktopPC__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountDesktopPC__c/100 )+',ViewRight Desktop for PC'+'%'+ 
                    'CTIDCLASS.IPTV.desktop_mac = '+(Integer)uSystem.LicenseCountDesktopMac__c +','+(Integer)( uSystem.LowerBoundforWarningMsg__c *uSystem.LicenseCountDesktopMac__c/100)+','+(Integer)( uSystem.UpperBoundforIssueCert__c*uSystem.LicenseCountDesktopMac__c /100)+',ViewRight Desktop for MAC'+'%'+
                    'CTIDCLASS.InternetTV.web_pc = '+(Integer)uSystem.LicenseCountWebPC__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountWebPC__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountWebPC__c /100)+',ViewRight Web for PC'+'%'+ 
                    'CTIDCLASS.InternetTV.web_mac= '+(Integer)uSystem.LicenseCountWebMac__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountWebMac__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountWebMac__c /100)+',ViewRight Web for MAC'+'%'+ 
                    'CTIDCLASS.InternetTV.web_iphone= '+(Integer)uSystem.LicenseCountWebiPhone__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountWebiPhone__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountWebiPhone__c/100 )+',ViewRight Web for iPhone'+'%'+
                    'CTIDCLASS.InternetTV.web_android= '+(Integer)uSystem.LicenseCountWebAndroid__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountWebAndroid__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountWebAndroid__c/100 )+',ViewRight Web for Android'+'%'+ 
                    'CTIDCLASS.InternetTV.web_stb= '+(Integer)uSystem.LicenseCountWebSTB__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountWebSTB__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountWebSTB__c/100 )+',ViewRight Web for STB'+'%'+
                    'CTIDCLASS.InternetTV.web_gateway= '+(Integer)uSystem.License_Count_WebGateway__c +','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.License_Count_WebGateway__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.License_Count_WebGateway__c/100 )+',ViewRight Web for Gateway'+'%'+
                    'CTIDGROUP.Group1= '+(Integer)uSystem.LicenseCountGroup1__c+','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountGroup1__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountGroup1__c/100 )+',Group 1'+'%'+ Group_1_Devices +
                    'CTIDGROUP.Group2= '+(Integer)uSystem.LicenseCountGroup2__c+','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.LicenseCountGroup2__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.LicenseCountGroup2__c/100 )+',Group 2'+'%'+ Group_2_Devices +
                    'CTIDGROUP.Group3= '+(Integer)uSystem.hid_LicenseCountGroup3__c+','+(Integer)( uSystem.Lower_Bound_for_OTT__c *uSystem.hid_LicenseCountGroup3__c/100)+','+(Integer)( uSystem.Upper_Bound_for_OTT__c*uSystem.hid_LicenseCountGroup3__c/100 )+',Group 3'+'%'+ Group_3_Devices ;
         
            //If License Counts were changed, flag the License Blobs Changed boolean
            if (systemBeforeUpdate.Client_Licenses_Blob__c != uSystem.Client_Licenses_Blob__c) {
                uSystem.hid_License_Blobs_Changed__c = true;
            }
        }   
    }
    
}
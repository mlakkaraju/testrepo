//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Class to assist in requesting software downloads

public without sharing class SLA_DownloadRequestHelpers {
	public static final String STATUS_READY = 'Ready';
	public static final String STATUS_EXPIRED = 'Expired';

	public static Software_Request__c requestSoftware(Id accountId, Id versionId){
		
		VCAS_ViewRight_Version__c version = [SELECT Delivery_File_Name__c FROM VCAS_ViewRight_Version__c WHERE Id = :versionId LIMIT 1];

		String guid = accountId + String.valueOf(datetime.now().getTime());
		Software_Request__c requestObj = new Software_Request__c(
			Account__c = accountId,
			VCAS_Version__c = version.Id,
			Reference_Id__c = guid
		);
		

		SLA_DeliveryClient.SoftwareRequest req = new SLA_DeliveryClient.SoftwareRequest(guid, version.Delivery_File_Name__c);
		
		SLA_DeliveryClient.APIResponse resp;
		if(!Test.isRunningtest()){
		 	resp = SLA_DeliveryClient.sendRequest(req);
		}else{
			resp = new SLA_DeliveryClient.APIResponse();
			resp.status = '200';
			resp.message = 'http://test.com';
		}

		if(resp.status == '200'){
			requestObj.Download_URL__c = resp.message;
		}else{
			System.Debug(resp.message);
			throw new SoftwareDownloadException('Failed to get download URL! ' + resp.message);
		}
		requestObj.Requested_On_Date__c = DateTime.Now();
		insert requestObj;
		return requestObj;
	}

public class SoftwareDownloadException extends Exception {} 

}
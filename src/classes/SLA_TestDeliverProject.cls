//Author: Charles Jonas (charlie@callawaycloudconsulting.com)
//  Tests for sending Projects to Customers.
@isTest
private class SLA_TestDeliverProject {


    //test project with ready system but no users to deliver to
    @isTest static void test_deliver_no_users() {
        Tenrox__c proj = setupProject(false,false,false);

        PageReference pageRef = Page.SLA_DeliverProject;
        Test.setCurrentPage(pageRef);
        //ApexPages.currentPage().getParameters().put('id',proj.id);

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proj);
        SLA_DeliverProjectController ctrl = new SLA_DeliverProjectController(stdCtrl);

        ctrl.deliverSystem();

        List<Apexpages.Message> msgs = ApexPages.getMessages();

        boolean b = false;

        for(Apexpages.Message msg : msgs){
            if (msg.getDetail() == SLA_DeliverProjectController.MSG_NO_USERS)
            {
                b = true;
            }
        }

        system.assert(b);

    }

    //test a valid delivery
    @isTest static void test_deliver_success() {
        Tenrox__c proj = setupProject(true,true,true);

        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proj);

        PageReference pageRef = Page.SLA_DeliverProject;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',proj.id);

        SLA_DeliverProjectController ctrl = new SLA_DeliverProjectController(stdCtrl);

        ApexPages.currentPage().getParameters().put('contactId',proj.Primary_Customer_Contact__c);
        ctrl.createCustomerLogin();
        ApexPages.currentPage().getParameters().put('contactId',proj.Secondary_Customer_Contact__c);
        ctrl.createCustomerLogin();
        
        ctrl.emailPrimaryCustomer = true;
        ctrl.emailSecondaryCustomer = true;
        ctrl.deliverSystem();

        //make sure there are no page messages
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;

        for(Apexpages.Message msg : msgs){
            if (msg.getDetail() == SLA_DeliverProjectController.MSG_DELIVER_SUCCESS)
            {
                b = true;
            }
        }

        system.assert(b);

        //make sure the record is there
        proj = [SELECT Project_Status_CC__c FROM Tenrox__c WHERE Id = :proj.Id];
        system.assertEquals(SLA_ProjectHelpers.STATUS_EXECUTING,proj.Project_Status_CC__c);

    }

    //test a valid delivery
    @isTest static void test_deliver_success_completed() {
        Tenrox__c proj = setupProject(true,true,true);
        proj.Customer_License_Wizard_Step__c = SLA_Communities_Helpers.getUrlBase(Page.SLA_LicenseRequestWizard_Final.getUrl());
        proj.Project_Status_CC__c = SLA_ProjectHelpers.STATUS_COMPLETE;
        ApexPages.StandardController stdCtrl = new ApexPages.StandardController(proj);

        PageReference pageRef = Page.SLA_DeliverProject;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',proj.id);

        SLA_DeliverProjectController ctrl = new SLA_DeliverProjectController(stdCtrl);

        ApexPages.currentPage().getParameters().put('contactId',proj.Primary_Customer_Contact__c);
        ctrl.createCustomerLogin();
        ApexPages.currentPage().getParameters().put('contactId',proj.Secondary_Customer_Contact__c);
        ctrl.createCustomerLogin();
        
        ctrl.emailPrimaryCustomer = true;
        ctrl.emailSecondaryCustomer = true;
        ctrl.deliverSystem();

        //make sure there are no page messages
        List<Apexpages.Message> msgs = ApexPages.getMessages();
        boolean b = false;

        for(Apexpages.Message msg : msgs){
            if (msg.getDetail() == SLA_DeliverProjectController.MSG_DELIVER_SUCCESS)
            {
                b = true;
            }
        }

        system.assert(b);

        //make sure the record is there
        proj = [SELECT Project_Status_CC__c, Customer_License_Wizard_Step__c FROM Tenrox__c WHERE Id = :proj.Id];
        system.assertEquals(SLA_ProjectHelpers.STATUS_COMPLETE, proj.Project_Status_CC__c);
        system.assertEquals(Page.SLA_LicenseRequestWizard_Review.getUrl(), proj.Customer_License_Wizard_Step__c);

    }


    //Helper function to generate setup the project
    private static Tenrox__c setupProject(Boolean sys, 
        Boolean contact1, boolean contact2){
        Account acc = TestUtil.generateAccount();
        acc.Enable_Verimatrix_Self_Service_Licensing__c = true;
        insert acc;
        Entitlement entitl = TestUtil.createEntitlement(acc.Id);
        Tenrox__c proj = TestUtil.generateProject(acc.Id, entitl.Id);
        if(sys){
            System__c syst = TestUtil.createSystem(acc.Id, null);
            proj.system__c = syst.id;
        }

        if(contact1){
            Contact c = TestUtil.generateContact(acc.id);
            c.email = TestUtil.generateRandomEmail();
            insert c;
            proj.Primary_Customer_Contact__c = c.id;
        }

        if(contact2){
            Contact c = TestUtil.generateContact(acc.id);
            c.email = TestUtil.generateRandomEmail();
            insert c;
            proj.Secondary_Customer_Contact__c =  c.id;
        }

        insert proj;
        proj = selectProject(proj.id);

        return proj;
    }

    //helper function to retrieve all needed project info
    private static Tenrox__c selectProject(Id projectId){
       Tenrox__c proj = [SELECT 
                id, 
                Primary_Customer_Contact__c, 
                Secondary_Customer_Contact__c, 
                system__r.id, 
                system__r.VCAS_Version__c,
                Max_Number_of_Servers__c,
                account__c,
                account__r.Enable_Verimatrix_Self_Service_Licensing__c 
                FROM Tenrox__c 
                WHERE id =: projectId 
                Limit 1
                ];
                return proj;
    }


}
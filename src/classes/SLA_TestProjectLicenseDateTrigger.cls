@isTest
private class SLA_TestProjectLicenseDateTrigger {
	
	@isTest static void test_afpl() {
		System__c sys = TestUtil.createSystem();
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		proj.System__c = sys.Id;
		proj.License_Counts_Set_on_System__c = true;
		insert proj;

		Test.startTest();
		proj.Approved_for_Permanent_Licenses__c = true;
		update proj;
		Test.stopTest();

		sys = [SELECT LicenseEndDate__c, LicenseStartDate__c FROM System__c WHERE Id = :sys.Id];
		System.assertEquals(SLA_SystemLicenseCount_Helper.MAX_DATE, sys.LicenseEndDate__c);
		System.assertEquals(Date.today(), sys.LicenseStartDate__c);
		
	}

	@isTest static void test_dlv() {
		System__c sys = TestUtil.createSystem();
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		proj.System__c = sys.Id;
		proj.License_Counts_Set_on_System__c = true;
		insert proj;

		Test.startTest();
		proj.Days_License_Valid__c = 180;
		update proj;
		Test.stopTest();

		sys = [SELECT LicenseEndDate__c, LicenseStartDate__c FROM System__c WHERE Id = :sys.Id];
		System.assertNotEquals(null, sys.LicenseEndDate__c); //hard to calculate in test
		System.assertEquals(Date.today(), sys.LicenseStartDate__c);
		
	}

	@isTest static void test_afpl_lr() {
		System__c sys = TestUtil.createSystem();
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		proj.System__c = sys.Id;
		proj.License_Counts_Set_on_System__c = true;
		insert proj;
		License_Request__c lr = TestUtil.generateLicenseRequest(sys.Id, sys.VCAS_Version__c);
		lr.Project__c = proj.Id;
		insert lr;

		Test.startTest();
		proj.Approved_for_Permanent_Licenses__c = true;
		update proj;
		Test.stopTest();

		sys = [SELECT LicenseEndDate__c, LicenseStartDate__c FROM System__c WHERE Id = :sys.Id];
		System.assertEquals(SLA_SystemLicenseCount_Helper.MAX_DATE, sys.LicenseEndDate__c);
		System.assertEquals(Date.today(), sys.LicenseStartDate__c);

		License_Request__c lrNew = [SELECT Id FROM License_Request__c WHERE Id != :lr.Id LIMIT 1];
		System.assertNotEquals(null, lrNew);
	}


	@isTest static void test_prevent_recursion(){
		System__c sys = TestUtil.createSystem();
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		proj.System__c = sys.Id;
		proj.License_Counts_Set_on_System__c = true;
		insert proj;
		List<License_Request__c> lrs = new List<License_Request__c>();
		for(Integer i = 0; i < 60; i++){
			License_Request__c lr = TestUtil.generateLicenseRequest(sys.Id, sys.VCAS_Version__c);
			lr.Project__c = proj.Id;
			lrs.add(lr);
		}
		insert lrs;

		Test.startTest();
		proj.Approved_for_Permanent_Licenses__c = true;
		update proj;
		Test.stopTest();

	}

	@isTest static void test_afpl_set(){
		System__c sys = TestUtil.createSystem();
		Account a = [SELECT Id FROM Account WHERE Id = :sys.Account__c];
		a.Always_give_Permanent_Licenses__c = true;
		update a;
		Entitlement entitl = TestUtil.createEntitlement(sys.Account__c);
		Tenrox__c proj = TestUtil.generateProject(sys.Account__c, entitl.Id);
		
		Test.startTest();
		insert proj;
		Test.stopTest();
		proj = [SELECT Approved_for_Permanent_Licenses__c FROM Tenrox__c WHERE Id = :proj.Id];
		System.assert(proj.Approved_for_Permanent_Licenses__c);
	}
	
}
global with sharing class CustomBALINKPSS extends BALINK1.BALINK_ProductSelection{
    private Map<String, Schema.SObjectField> MapFields = Schema.SObjectType.Product2.fields.getMap();
   public string ids{get;set;}

        
/********************************************Constructor***************************************/
    public CustomBALINKPSS (ApexPages.StandardController controller)
    {
        showErrorMessage = false;
        try{
           if(currentOpp == null)
           {
                retID=controller.getId();
                this.currentOpp = (Opportunity)controller.getRecord();
                string currentOppSQL ; 
                if(!MC)
                    currentOppSQL ='select id ,name,pricebook2id from Opportunity where id =\''+String.escapeSingleQuotes(retId)+'\'';
                else
                    currentOppSQL='select id,name,pricebook2id,currencyisocode from Opportunity where id =\''+String.escapeSingleQuotes(retId)+'\'';
                 currentOpp =database.query(currentOppSQL);
                 if(currentOpp.Id==null)
                 {
                    e.setMessage('Error');
                    throw e;
                 }
           }
        }
        catch(Exception err)
        {
            isId=false;
            showErrorMessage = true;
            Apexpages.addMessages(err);
        }
        currentSortingField = 'Product2.ProductCode';
	}
	public string displayUnitPriceEditable{
		get{
			Profile p = [select PermissionsEditOppLineItemUnitPrice from Profile where id=:UserInfo.getprofileId()];
			if(p.PermissionsEditOppLineItemUnitPrice)
				return '';
			//if(OpportunityLineItem.UnitPrice.getDescribe().isUpdateable())
				//return '';
			return 'display: none';
		}
	}
	public boolean isUnitPriceEditable{
		get{
			Profile p = [select PermissionsEditOppLineItemUnitPrice from Profile where id=:UserInfo.getprofileId()];
			return p.PermissionsEditOppLineItemUnitPrice;
			//return OpportunityLineItem.UnitPrice.getDescribe().isUpdateable();
		}
	}
    global override PageReference init()
    {
        try
        {
            PageReference p = initAction();
            if(p!=null)
            {
                Pagereference p1 = Page.PSS_OppProd;///apex/BALINK_ProductSelection
                p.getParameters().put('retURL',p1.getUrl()+'?id='+currentopp.id+'&sURL=');
                p.getParameters().put('id' , currentopp.id);
                return p;
            }
            setpaging(new BALINK1.Common_Paging('PricebookEntry',100,sql,'Product2.ProductCode'));
            CreateGrid();
        }
        catch(Exception er)
        {
            showErrorMessage = true;
            Apexpages.addMessages(er);
        }
        return null;
    }   
    /*
    global override void CreateGrid()
    { 
        
        string f=' PricebookEntry.IsActive = true AND Pricebook2.isactive =true AND PricebookEntry.PriceBook2Id = \'' + currentOpp.PriceBook2Id +'\'';
        if(MC)
        {
            SObject c = currentOpp;
            //this.currentOpp.CurrencyIsoCode
            f+=' AND currencyisocode = \'' + c.get('CurrencyIsoCode') + '\' ';
        }   
        string moreFilter =getfilter().buildFilter(); 
              
        getfilter().setSObjectName('Product2');
        f+=getfilter().CreateFilterByKeyWord(getsearchKeyWord(),MapFields);
        getpaging().setCustomFilters( f);
        getpaging().GetSearchResult();
        
        setTxtTitle(' 1 of ' + string.valueOf(getpaging().getPageIndexLimit()));
    } */
    global override PageReference cancel()
    {
        PageReference p = new PageReference('/'+retID);
        return p;
    }
    global override PageReference selectProd()
    {
        return Page.PSS_OppProdGrid;
    }
    global override PageReference AddMoreProduct()
    {
        try
        {
            list <opportunityLineItem> op = new list <opportunityLineItem>();
            for(BALINK1.BALINK_OpportunityLineItemDS o : getListItem())
            {
                   op.add(o.getlineItem());
            }   
            upsert op;
            PageReference acctPage = Page.PSS_OppProd;
            return acctPage;
        }
        catch(Exception err)
        {
            showErrorMessage = true;
            Apexpages.addMessages(err);
            return null;
        }
    }
    public PageReference deleteSelection(){
		List<opportunityLineItem> listDelete = new List<opportunityLineItem>();
		if(ids==null)
			ids = '';
		list<Integer> idtoDel = new list<Integer>(); 
		set<Integer> idtoDelSet = new set<Integer>(); 
		for(string id : ids.split(';',-1)){
			if(id=='')
				continue;
			idtoDel.add(Integer.valueOf(id));
		} 
		idtoDelSet.addall(idtoDel);
		for(BALINK1.BALINK_OpportunityLineItemDS o : getListItem())
        {
               if(idtoDelSet.contains(o.getKey()))
               {
               		if(o.getlineItem().id!=null)
               			listDelete.add(o.getlineItem());
               }
        }
        idtoDel.sort();
        for(Integer i =idtoDel.size() ; i>0 ; i--)
        {
        	getListItem().remove(idtoDel.get(i-1)-1);
        }

       	//getListItem().removeAll(idtoDel);
        delete listDelete;
        setIndexList2();
        return null;
	} 

	private void setIndexList2()
	{
	 integer i =1;
	 for(BALINK1.BALINK_OpportunityLineItemDS o : getListItem())
          o.setKey(i++);
	}

    static testMethod void myTest() {
        Opportunity opp = [Select o.Name, o.Id,o.StageName,o.Pricebook2Id,o.CurrencyIsoCode From Opportunity o where o.Pricebook2Id !=null limit 1];
      
          ApexPages.StandardController con = new ApexPages.StandardController(opp);
        CustomBALINKPSS  ps = new CustomBALINKPSS (con);  
        ps.GetProductItemList();
        //ps.initqItems();
        ps.setisTest(true);  
        ps.setOpportunity(opp);
        ps.init();
        ps.AddMoreProduct();
       	ps.createGrid();
       	ps.deleteselection();
        ps.selectprod();
        ps.AddMoreProduct();
        string test = ps.displayUnitPriceEditable; 
        boolean test2 = ps.isUnitPriceEditable;
        ps.cancel();
    Product2 createP ;
    Pricebook2 createPB ;
    PricebookEntry createPBE ;
    try{
        createP = new Product2(Name='BALINKProd1',isActive=true);
          insert createP ;
          System.assert(createP.id!=null);
          createPB = [select id,isActive from Pricebook2 where isStandard=true limit 1];
          createPB.isActive=true;
          update createPB ;
          createPBE = new PriceBookEntry(UnitPrice=50,Product2Id=createP.Id,Pricebook2Id=createPB.id,isActive=true);
          insert createPBE ;
          System.assert(createPBE.id!=null);
          opp = new Opportunity(Name='test',StageName='Closed Won',CloseDate=Date.today());
          insert opp;
          System.assert(opp.id!=null);
    }
    catch(Exception err)
    { 
      createP = [Select p.ProductCode, p.Name, p.IsActive, p.Id, p.Family From Product2 p where isActive=true limit 1 ];
          System.assert(createP.id!=null);

          createPBE = [Select p.UnitPrice, p.ProductCode, p.Product2Id, p.Pricebook2Id, p.IsActive, p.Id From PricebookEntry p where Product2Id=:createP.Id limit 1];
      createPB = [select id,isActive from Pricebook2 where id=:createPBE.Pricebook2Id limit 1];
          createPB.isActive=true;
          update createPB ;

          System.assert(createPBE.id!=null);

          opp = [Select o.Name, o.Id,o.StageName From Opportunity o limit 1];
          System.assert(opp.id!=null);
    }
        
        con = new ApexPages.StandardController(opp);
   ps = new CustomBALINKPSS(con); 
     ps.setOpportunity(opp);
     ps.init();
     string a;
     string sql;
     string sql2;
     if(ps.MC)
     {
       sql='Select o.Type, o.SystemModstamp, o.StageName, o.Probability, o.Pricebook2Id, o.OwnerId, o.NextStep, o.Name, o.LeadSource, o.LastModifiedDate, o.LastModifiedById, o.LastActivityDate, o.IsWon, o.IsDeleted, o.IsClosed, o.Id, o.HasOpportunityLineItem, o.ForecastCategoryName, o.ForecastCategory, o.FiscalYear, o.FiscalQuarter, o.Fiscal, o.Description, o.CurrencyIsoCode, o.CreatedDate, o.CreatedById, o.CloseDate, o.CampaignId, o.Amount, o.AccountId From Opportunity o limit 1';
       string sql3 = 'select IsoCode from CurrencyType where IsCorporate = true limit 1';
       sObject s = database.query(sql3);
       a =(String)s.get('Isocode');
       sql2='select id,CurrencyIsoCode from PriceBookEntry where currencyISOCode=\''+a+'\' limit 1';
       //opp=new Opportunity(Probability=75,Name='test',stageName='Win',CloseDate=Date.today(),PriceBook2Id = createPB.id);
     }
     else
     {
       sql='Select o.Type,  o.TotalOpportunityQuantity, o.SystemModstamp, o.StageName, o.Probability, o.Pricebook2Id, o.OwnerId, o.NextStep, o.Name,  o.LeadSource, o.LastModifiedDate, o.LastModifiedById, o.LastActivityDate, o.IsWon, o.IsPrivate, o.IsDeleted, o.IsClosed, o.Id, o.HasOpportunityLineItem, o.ForecastCategoryName, o.ForecastCategory, o.FiscalYear, o.FiscalQuarter, o.Fiscal, o.ExpectedRevenue, o.Description,  o.CreatedDate, o.CreatedById, o.CloseDate, o.CampaignId,  o.Amount, o.AccountId From Opportunity o limit 1';
       sql2='select id from PriceBookEntry limit 1';
       //opp=new Opportunity(TotalOpportunityQuantity=1,Probability=75,Name='test',stageName='Win',CloseDate=Date.today(),PriceBook2Id = createPB.id);
     }
      
      //opp=new Opportunity(TotalOpportunityQuantity=1,Probability=75,Name='test',stageName='Win',CloseDate=Date.today(),PriceBook2Id = createPB.id);
      //insert opp;
      System.assert(opp.id!=null);
      ps.GetProductItemList();
        //ps.initqItems();
        ps.setisTest(true);  
        ps.setOpportunity(opp);
        ps.init();
        ps.AddMoreProduct();
       	//ps.createGrid();
       	ps.deleteselection();
        ps.selectprod();
        ps.AddMoreProduct();
        string test3 = ps.displayUnitPriceEditable; 
        boolean test4 = ps.isUnitPriceEditable;

      PriceBookEntry pbe = database.query(sql2);
      OpportunityLineItem oli ;
      try{
        oli = new OpportunityLineItem(opportunityId=opp.id,PriceBookEntryId=createPBE.id,unitPrice=25,quantity=1,description='');
        insert oli;
      }
      catch(Exception err)
      {
        oli=[Select o.PricebookEntryId, o.OpportunityId, o.Id From OpportunityLineItem o where opportunityId=:opp.id limit 1];
      }
      System.assert(oli.id!=null);
      ps.setOpportunity(opp);
      ps.setisTest(true);
     ps.init();
     opp=ps.getOpportunity();
  	ps.GetProductItemList();  
     ps.getselectedIds();
     ps.getlog();
     ps.getErrorMessage();
     //ps.getCurrencyRate();

     ps.getlocale();
     
     ps.setListItem(ps.getListItem());
     ps.setselectedIds(ps.getselectedIds());
     ps.setmsg(ps.getmsg());
     ps.setTxtTitle(ps.getTxtTitle());
     ps.setlog(ps.getlog());
     ps.setscrollTop(ps.getscrollTop());
     ps.setcurrentSortingField(ps.getcurrentSortingField());
     ps.setSearchKeyWord(ps.getSearchKeyWord());
     //ps.setPaging( ps.getPaging());
     //ps.setOpportunity(ps.getOpportunity());
     ps.getProductFields();
     ps.getProductFieldsType();
     ps.getProductFamilies();
     ps.getFamilyList();
     ps.setSidebar(ps.getSidebar());
     ps.setProFamily(ps.getProFamily());
     ps.getMc();
     ps.getfilterRow();
     
      ps.ChangeSize();
      ps.next();
      ps.previous();
      ps.search();
      
      
      ps.sortlist();
      ps.selectProd();
      ps.cancel();
      
      ps.save();
      ps.setselectedIds(null);
      ps.initProductGrid();
      //ps.setselectedIds('01t70000001PnIo');
     //ps.AddMoreProduct();
     ps.getOpportunity();
     System.currentPageReference().getParameters().put('selectedId','1');
     //ps.DeleteItem();
     
     ps.getListItem()[0].setKey(1);
     ps.getListItem()[0].getKey();
     ps.getListItem()[0].setLineItem(ps.getListItem()[0].getLineItem());
     ps.getListItem()[0].setpriceBookItem(ps.getListItem()[0].getpriceBookItem());

     }
}
/*
Developer: Ralph Callaway <ralph.w.callaway@gmail.com>
Description:
	Extension for createMaintPdf to allow the milestones
	to be sorted by begin date (start_date__c)
*/
public with sharing class createMaintPdf {

	public List<M_S_Milestone__c> milestones { get; set; }
	
	public createMaintPdf(ApexPages.StandardController controller) {
		milestones = [
			select
				  customer__c
				, po_number__c
				, start_date__c
				, maintenance_expiration__c
				, he_sw_amount__c
				, cl_sw_amount__c
				, annual_m_s__c
				, prorated_months__c
				, m_s_amount__c
				, red_hat_licenses__c
				, red_hat_license_cost__c
				, total_m_s__c
				, inactive__c
			from M_S_Milestone__c
			where account__c = :controller.getId()
			order by start_date__c asc nulls last
		];
	}

	@isTest
	private static void testCreateMaintPdf() {
		// create test data
		Account testAccount = TestUtil.createAccount();
		M_S_Milestone__c newerMilestone = TestUtil.generateMandSMilestone(testAccount.id);
		newerMilestone.start_date__c = system.today();
		M_S_Milestone__c olderMilestone = TestUtil.generateMandSMilestone(testAccount.id);
		olderMilestone.start_date__c = system.today().addDays(-1);
		insert new M_S_Milestone__c[] { newerMilestone, olderMilestone };
		
		// load the createMaintPdf controller
		ApexPages.StandardController controller = new ApexPages.StandardController(testAccount);
		createMaintPdf extension = new createMaintPdf(controller);
		
		// check that older milestone comes out first
		List<M_S_Milestone__c> output = extension.milestones;
		system.assertNotEquals(null, output);
		system.assertEquals(2, output.size());
		system.assertEquals(olderMilestone.id, output[0].id);
		system.assertEquals(newerMilestone.id, output[1].id);
	}
}
@isTest
private class SLA_TestEntitledAccessRetriever {
//test to make sure we don't fail if the account is empty
	@isTest static void test_empty_account() {
		Account acc = TestUtil.createAccount();
		SLA_EntitledAccessRetriever retriever = new SLA_EntitledAccessRetriever(acc.id);
		List<System__c> systems = retriever.getSystems();
		System.assertEquals(systems.size(), 0);
	}

	//test user constructor
	@isTest static void test_from_user() {
		Account acc = TestUtil.createAccount();
		Contact c = TestUtil.generateContact(acc.id);
		c.email = TestUtil.generateRandomEmail();
		insert c;
		User u = SLA_CommunityUserFactory.createUserFromContact(c.id,false);
		SLA_EntitledAccessRetriever retriever = new SLA_EntitledAccessRetriever(u);
		System.assertEquals(retriever.accountId, acc.id);
	}

	//test user construtor with invalid user
	@isTest static void test_from_user_no_contact() {
		try{
			Account acc = TestUtil.createAccount();
			User u = TestUtil.createUser();
			SLA_EntitledAccessRetriever retriever = new SLA_EntitledAccessRetriever(u);
			}catch(Exception e){
				System.assert(true);
				return;
			}
			System.assert(false);
		}

	//test to make sure systems from direct entitlements show up
	@isTest static void test_system_from_entitlement() {
		Account acc = TestUtil.createAccount();

		List<Entitlement> entitls = createEntitlements(acc.id, null, 5);
		Integer systemCount = 0;

		for(Integer i = 0; i < entitls.size(); i++){
			List<Tenrox__c> projs = createProjects(entitls[i], i);
			for(Integer j = 0; j < projs.size(); j++){
				systemCount++;
				createLicenseRequests(projs[j].System__c, j);
			}
		}
		Test.startTest();
		SLA_EntitledAccessRetriever retriever = new SLA_EntitledAccessRetriever(acc.id);
		List<System__c>  systems = retriever.getSystems();
		Test.stopTest();

		System.assertEquals(systems.size(), systemCount);
	}

	//test to make sure system linked from service contracts and entitlements show up,
	// and they are not duplicated
	@isTest static void test_system_from_both() {
		Account acc = TestUtil.createAccount();

		List<ServiceContract> scs = createServiceContracts(acc.id, 5);
		Integer systemCount = 0;
		for(Integer k = 0; k < scs.size(); k++){

			List<Entitlement> entitls = createEntitlements(acc.id, scs[k].id, k);

			for(Integer i = 0; i < entitls.size(); i++){
				List<Tenrox__c> projs = createProjects(entitls[i], i);
				for(Integer j = 0; j < projs.size(); j++){
					systemCount++;
					createLicenseRequests(projs[j].System__c, j);
				}
			}
		}
		Test.startTest();
		SLA_EntitledAccessRetriever retriever = new SLA_EntitledAccessRetriever(acc.id);
		List<System__c> systems = retriever.getSystems();
		Test.stopTest();

		System.assertEquals(systems.size(), systemCount);
	}


	//--- Test helpers ---

	private static List<ServiceContract> createServiceContracts(Id accountId, Integer count){
		List<ServiceContract> scs = new List<ServiceContract>();
		for(Integer i = 0; i < count; i++){
			ServiceContract sc = TestUtil.generateServiceContract(accountId);
			scs.add(sc);
		}
		insert scs;
		return scs;
	}

	private static List<Entitlement> createEntitlements(Id accountId, Id serviceContractId, Integer count){
		List<Entitlement> entitlements = new List<Entitlement>();
		for(Integer i = 0; i < count; i++){
			
			if(accountId == null){
				accountId = TestUtil.createAccount().id;
			}
			
			Entitlement entitl = TestUtil.generateEntitlement(accountId);
			if(serviceContractId != null){
				entitl.ServiceContractId = serviceContractId;
			}
			entitlements.add(entitl);
		}
		insert entitlements;
		return entitlements;
	}

	private static List<Tenrox__c> createProjects(Entitlement entitl, Integer count){
		List<Tenrox__c> projects = new List<Tenrox__c>();
		for(Integer i = 0; i < count; i++){
			System__c sys = TestUtil.createSystem(entitl.AccountId, null);

			Tenrox__c project = TestUtil.generateProject(entitl.AccountId, entitl.Id);
			project.System__c = sys.id;
			projects.add(project);
		}
		insert projects;
		return projects;
	}

	private static List<License_Request__c> createLicenseRequests(Id systemId, Integer count){
		List<License_Request__c> lrs = new List<License_Request__c>();
		
		//hmmm this could apperently cause test to fail due to a unquie constaint on the version #
		VCAS_ViewRight_Version__c version = TestUtil.createVCASVersion();
		for(Integer i = 0; i < count; i++){
			License_Request__c lr = TestUtil.generateLicenseRequest(systemId, version.id);
			lrs.add(lr);
		}
		insert lrs;
		return lrs;
	}
}
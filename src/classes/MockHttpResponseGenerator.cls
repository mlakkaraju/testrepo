//Author: Charlie Jonas (charlie@callawaycloudconsulting.com)
// Description:  Class is use for creating mock http callouts in test methods. 
// See http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_restful_http_testing_httpcalloutmock.htm
// for info on how to use class.

@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock {
	//require properites
    private Map<String,String> responseMap = new Map<String,String>();  

	//optional
	public String method { get; set;}

	//optional
	public String contentType { get; set;}
	//defaults to xml
	{contentType = 'text/xml';}

	//optional 
	public Integer statusCode {get; set;}
	//defaults to success
	{statusCode = 200;}

    //optional 
    public Boolean ignoreQuery {get; set;}
    //defaults to success
    {ignoreQuery = true;}


    public MockHttpResponseGenerator(){}

    public MockHttpResponseGenerator(String endpoint, String response){
        addResp(endpoint,response);
    }

    //add an additional endpoint response pairs
    public void addResponse(String endpoint, String response){
        addResp(endpoint,response);
    }

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        String endpoint = req.getEndpoint();
        if(ignoreQuery){
            endpoint = getBaseUrl(endpoint);
        }
        String response = responseMap.get(endpoint);
        System.assertNotEquals(response, null);

        if(method != null){
        	System.assertEquals(method, req.getMethod());
        }
        
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', contentType);
        res.setBody(response);
        res.setStatusCode(statusCode);
        return res;
    }

    private void addResp(String endpoint, String response){
        if(ignoreQuery){
            endpoint = getBaseUrl(endpoint);
        }
        responseMap.put(endpoint, response);
    }

    private String getBaseUrl(String endpoint){
        Url baseUrl = new URL(endpoint);
        return baseUrl.getHost() + baseUrl.getPath();
    }

}
public with sharing class ProductBundleController {

	/* Constants, Properties, Variables */

	// param constants
	public static final String BUNDLE_UNIT_PRICE_PARAM = 'bundleUnitPrice';
	public static final String PERCENTS_PARAM = 'percents';
	public static final String PRODUCT_IDS_PARAM = 'productIds';
	public static final String QUANTITIES_PARAM = 'quantities';
	
	// misc constants
	public static final String DELIMITER = ',';
	
	// label constatus
	private static final String PARAMETER_MISSING = 'Paramter missing';
	private static final String INVALID_PARAMETER = 'Invalid parameter';
	
	// properties
	public OpportunityLineItem refObj { get; set; } { refObj = new OpportunityLineItem(quantity = 1); }
	public List<OpportunityLineItem> bundleLineItems { get; set; }
	public Decimal bundlePrice { get { return refObj.unitPrice; } set { refObj.unitPrice = value; } }
	public Decimal bundleQuantity { get { return refObj.quantity; } set { refObj.quantity = value; } }
	public String opptyId { get { return oppty.id; } }
	public Boolean missingProducts { get; set; } { missingProducts = false; }
	
	// variables
	private ApexPages.StandardController controller;
	private Opportunity oppty;
	private List<Decimal> percents = new List<Decimal>();
	private List<Id> productIds = new List<Id>();
	private List<Integer> quantities = new List<Integer>();
	
	/* Constructor */

	public ProductBundleController(ApexPages.StandardController controller) {
		this.controller = controller;
		oppty = (Opportunity) controller.getRecord();
		if(oppty.pricebook2Id != null) {
			loadParams();
			buildBundle();
		}
	}
	
	/* Action Methods */
		
	public void updateBundle() {
		buildBundle();
	}
	
	public PageReference saveBundle() {
		buildBundle();
		insert bundleLineItems;
		return controller.cancel();
	}
	
	/* Support Methods */
	
	private void buildBundle() {
		if(bundleLineItems == null)
			initBundle();
		
		for(Integer i = 0; i < productIds.size(); i++) {
			OpportunityLineItem lineItem = bundleLineItems[i];
			lineItem.quantity = quantities[i] * bundleQuantity;
			lineItem.totalPrice = percents[i] / 100 * bundlePrice * bundleQuantity;
		}
	}
	
	private void initBundle() {
		Map<Id, Product2> prodMap = new Map<Id, Product2>(
			[select name from Product2 where id in :productIds]
		);
		Map<Id, PricebookEntry> pbMap = new Map<Id, PricebookEntry>();
		for(PricebookEntry pb : 
		 [select product2Id, product2.name 
		  from PricebookEntry 
		  where product2Id in :productIds 
		  and isActive = true
		  and pricebook2Id = :oppty.pricebook2Id])
		{
			pbMap.put(pb.product2id, pb);	
		}
		bundleLineItems = new List<OpportunityLineItem>();
		for(Id productId : productIds) {
			OpportunityLineItem lineItem = new OpportunityLineItem();
			PricebookEntry pricebookEntry = pbMap.get(productId);
			
			if(pricebookEntry == null) {
				VisualforceHelper.addErrorMessage(
					  prodMap.get(productId).name
					+ ' is not avaible in this pricebook or is inactive.'
					+ ' Please choose a pricebook that contains all bundle products.');
				missingProducts = true;
				pricebookEntry = new PricebookEntry(product2 = prodMap.get(productId));
			} else {
				lineItem.pricebookEntryId = pricebookEntry.id;	
			}
			
			lineItem.pricebookEntry = pricebookEntry;
			lineItem.opportunityId = oppty.id;
			bundleLineItems.add(lineItem);
		}
	}
	
	private void loadParams() {
		Map<String, String> params = ApexPages.currentPage().getParameters();
		
		for(String percent : params.get(PERCENTS_PARAM).split(DELIMITER)) {
			percents.add(Decimal.valueOf(percent));
		}
		
		for(String productId : params.get(PRODUCT_IDS_PARAM).split(DELIMITER)) {
			productIds.add(productId);
		}
		
		for(String quantity : params.get(QUANTITIES_PARAM).split(DELIMITER)) {
			quantities.add(Integer.valueOf(quantity));
		}

		bundlePrice = Decimal.valueOf(params.get(BUNDLE_UNIT_PRICE_PARAM));
	}
		
	/* Test Methods */
	
	@isTest
	private static void testProductBundleController() {
		// create test data
		Decimal bundleUnitPrice = 500;
		Decimal bundleQuantity = 2;
		Decimal quantity1 = 1;
		Decimal quantity2 = 2;
		Decimal percent1 = 70;
		Decimal percent2 = 30;
		TestUtilProductHelper productHelper = new TestUtilProductHelper();
		Opportunity testOpportunity = TestUtil.generateOpportunity(null);
		testOpportunity.pricebook2id = productHelper.pricebook.id;
		insert testOpportunity;
		
		// load product bundle page
		PageReference testPage = Page.ProductBundle;
		Map<String, String> params = testPage.getParameters();
		params.put(PRODUCT_IDS_PARAM, productHelper.testProduct.id + ',' + productHelper.testProduct2.id);
		params.put(QUANTITIES_PARAM, quantity1 + DELIMITER + quantity2);
		params.put(PERCENTS_PARAM, percent1 + DELIMITER + percent2);
		params.put(BUNDLE_UNIT_PRICE_PARAM, String.valueOf(bundleUnitPrice));
		Test.setCurrentPage(testPage);
		ApexPages.StandardController controller = new ApexPages.StandardController(testOpportunity);
		ProductBundleController extension = new ProductBundleController(controller);
		
		// set quantity to 2 and save bundle
		extension.bundleQuantity = bundleQuantity;
		extension.updateBundle();
		PageReference nextPage = extension.saveBundle();
		
		// check results
		system.assertNotEquals(null, nextPage);
		OpportunityLineItem lineItem1;
		OpportunityLineItem lineItem2;
		List<OpportunityLineItem> lineItems = 
			[select 
				  pricebookEntry.product2id
				, unitPrice
				, quantity
				, totalPrice 
			 from OpportunityLineItem
			 where opportunityId = :testOpportunity.id];
		system.assertEquals(2, lineItems.size());
		for(OpportunityLineItem lineItem : lineItems) {
			if(lineItem.pricebookEntry.product2Id == productHelper.testProduct.id)
				lineItem1 = lineItem;
			else if(lineItem.pricebookEntry.product2Id == productHelper.testProduct2.id)
				lineItem2 = lineItem;
		}
		system.assertNotEquals(null, lineItem1);
		system.assertNotEquals(null, lineItem2);
		system.assertEquals(bundleQuantity * quantity1, lineItem1.quantity);
		system.assertEquals(bundleQuantity * quantity2, lineItem2.quantity);
		system.assertEquals(bundleUnitPrice * percent1 / 100 * bundleQuantity, lineItem1.totalPrice);
		system.assertEquals(bundleUnitPrice * percent2 / 100 * bundleQuantity, lineItem2.totalPrice);
	}

}